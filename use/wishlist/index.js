const { Feature } = require("@acanto/laravel-scripts/scripts/use");

class FeatureWishlist extends Feature {
  routes() {
    return [];
  }

  components() {
    return [{ name: "Wishlist" }, { name: "ItemCard" }];
  }

  middlewares() {
    return [];
  }
}

module.exports = FeatureWishlist;
