import { bindWishlistBtn } from "@acanto/core-wishlist";
import "./Btn.scss";

/**
 * Component: WishlistBtn
 */
export function WishlistBtn() {
  bindWishlistBtn();
}
