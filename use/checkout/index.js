const { Feature } = require("@acanto/laravel-scripts/scripts/use");

class FeatureCheckout extends Feature {
  routes() {
    return [
      {
        name: "checkoutauth",
      },
      {
        name: "checkoutdetails",
      },
      {
        name: "checkoutpayment",
      },
      {
        name: "checkoutsummary",
      },
      {
        name: "checkoutcompleted",
      },
    ];
  }

  components() {
    return [{ name: "Checkout" }];
  }

  middlewares() {
    return [];
  }
}

module.exports = FeatureCheckout;
