import "@acanto/core-forms/radio";
import { $, $$, forEach, getDataAttr } from "@acanto/core-dom";
import collapsable from "@acanto/core-collapsable";
import { CheckoutPaymentMethod } from "./PaymentMethod";
import { CheckoutPaypal } from "./Paypal";
import { Checkout, ATTR_METHOD_CODE, ATTR_METHOD_BODY } from "./index";
import "./PaymentMethods.scss";

type ValueOf<T> = T[keyof T];

/**
 * Component: CheckoutPaymentMethods
 */
export function CheckoutPaymentMethods($form?: HTMLFormElement) {
  const $root = $(".CheckoutPaymentMethods:", $form);
  const $methods = $$(".CheckoutPaymentMethod:", $root);
  // @ts-expect-error
  const map: Checkout.Payment.MethodsMap = {};

  forEach($methods, initPaymentMethod);

  setInitialAccordionState();

  /**
   * Init each payment option
   *
   * It reads the data attribute on the DOM root element
   * to determine which payment controller class to instantiate
   */
  function initPaymentMethod($el: HTMLDivElement) {
    const code = getDataAttr($el, ATTR_METHOD_CODE);
    const $body = $(`[data-${ATTR_METHOD_BODY}]`, $el);
    let Controller = CheckoutPaymentMethod;

    switch (code) {
      case "banktransfer":
        // Controller = BankTransfer;
        break;
      case "cashondelivery":
        // Controller = CashOnDelivery;
        break;
      case "paypal":
        Controller = CheckoutPaypal;
        break;
      case "creditcard":
        // Controller = CheckoutPaypal;
        break;
      case "email":
        // Controller = CheckoutPaypal;
        break;
      default:
        if (__DEV__) {
          console.error(`Unsupported payment code: ${code}`, $el);
        }
        break;
    }

    map[code] = {
      c: new Controller($el, { $form, onChange: updateAccordion }),
      a: collapsable($body),
    } as ValueOf<Checkout.Payment.MethodsMap>;
  }

  /**
   * Set initial accordion state
   */
  function setInitialAccordionState() {
    const payment = getCurrent();

    updateAccordion(payment, true);
  }

  /**
   * Get currently selected payment
   */
  function getCurrent() {
    for (const key in map) {
      const item: ValueOf<Checkout.Payment.MethodsMap> = map[key];

      if (item.c.isSelected()) {
        return item.c;
      }
    }
    return null;
  }

  /**
   * Toggle payments in an accordion like behaviour
   */
  function updateAccordion(
    instance: Checkout.Payment.Controller,
    immediate?: boolean
  ) {
    for (const key in map) {
      const item: ValueOf<Checkout.Payment.MethodsMap> = map[key];

      if (instance && key === instance.type && instance.isSelected()) {
        if (item.a) item.a.expand(immediate);
        instance.select();
      } else {
        if (item.a) item.a.collapse(immediate);
        if (item.c) item.c.deselect();
      }
    }
  }
}
