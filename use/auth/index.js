const { Feature } = require("@acanto/laravel-scripts/scripts/use");

class FeatureAuth extends Feature {
  components() {
    return [{ name: "Auth" }];
  }

  routes() {
    return [
      {
        name: "login",
        alternativeNames: ["signin"],
      },
      {
        name: "passwordreset",
      },
      {
        name: "passwordrecovery",
      },
      {
        name: "register",
        alternativeNames: ["signup"],
      },
      {
        name: "profile",
        alternativeNames: ["account"],
      },
    ];
  }
}

module.exports = FeatureAuth;
