import { $ } from "@acanto/core-dom";
import { hasGtm } from "@acanto/core-analytics";
import FormsInputMaterial from "@acanto/core-forms/input/material";
import FormsSelectMaterial from "@acanto/core-forms/select/material";
import AuthFormRegister from "@acanto/core-auth/form-register";
import "@acanto/core-forms/checkbox";
import "./Register.scss";

/**
 * Component: AuthRegister
 */
export function AuthRegister($root?: HTMLElement) {
  $root = $root ? $(".AuthRegister:", $root) : $root;
  const $error = $(".error", $root);

  FormsInputMaterial();
  FormsSelectMaterial();

  AuthFormRegister($root, {
    succeded: (formData, instance, response) => {
      $error.innerHTML = response.data.msg || "";

      const {
        password,
        passwordcheck,
        _redirect,
        _timezone_offset,
        _token,
        ...userTrackData
      } = formData;

      if (hasGtm()) {
        dataLayer.push({
          ...userTrackData,
          userId: response.data?.user?.id,
          event: "form sended",
          form: "Sign Up",
          eventCallback: function () {
            location.href = response.data.redirect;
          },
          eventTimeout: 2000,
        });
      } else {
        location.href = response.data.redirect;
      }
    },
    failed: (formData, instance, response) => {
      $error.innerHTML = response.data;
      setTimeout(() => {
        $error.innerHTML = "";
      }, 3000);
    },
  });
}
