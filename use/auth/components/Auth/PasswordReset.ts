import AuthFormPasswordReset from "@acanto/core-auth/form-password-reset";
import FormsInputMaterial from "@acanto/core-forms/input/material";
import "@acanto/core-forms/checkbox";
import "./PasswordReset.scss";

/**
 * Component: AuthPasswordReset
 */
export function AuthPasswordReset($root?: HTMLElement) {
  FormsInputMaterial();
  AuthFormPasswordReset($root);
}
