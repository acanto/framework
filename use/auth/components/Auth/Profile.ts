import FormsInputMaterial from "@acanto/core-forms/input/material";
import FormsSelectMaterial from "@acanto/core-forms/select/material";
import AuthFormProfile from "@acanto/core-auth/form-profile";
import AuthFormPasswordChange from "@acanto/core-auth/form-password-change";
import "@acanto/core-forms/checkbox";
import "./Profile.scss";

/**
 * Component: AuthProfile
 */
export function AuthProfile($root?: HTMLElement) {
  FormsInputMaterial();
  FormsSelectMaterial();
  AuthFormProfile($root);
  AuthFormPasswordChange($root);
}
