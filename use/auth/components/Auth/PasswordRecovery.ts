import AuthFormPasswordRecovery from "@acanto/core-auth/form-password-recovery";
import FormsInputMaterial from "@acanto/core-forms/input/material";
import "@acanto/core-forms/checkbox";
import "./PasswordRecovery.scss";

/**
 * Component: AuthPasswordRecovery
 */
export function AuthPasswordRecovery($root?: HTMLElement) {
  FormsInputMaterial();
  AuthFormPasswordRecovery($root);
}
