import { $ } from "@acanto/core-dom";
import { hasGtm } from "@acanto/core-analytics";
import AuthFormLogin from "@acanto/core-auth/form-login";
import FormsInputMaterial from "@acanto/core-forms/input/material";
import "@acanto/core-forms/checkbox";
import "./Login.scss";

/**
 * Component: AuthLogin
 */
export function AuthLogin($root?: HTMLElement) {
  $root = $root ? $(".AuthLogin:", $root) : $root;
  const $error = $(".error", $root);

  FormsInputMaterial();

  AuthFormLogin($root, {
    succeded: (formData, instance, response) => {
      $error.innerHTML = response?.data?.msg || "";

      const {
        id,
        password,
        token,
        token_active,
        end_session,
        ...userTrackData
      } = response.data.user;

      if (hasGtm()) {
        dataLayer.push({
          ...userTrackData,
          userId: id,
          event: "form sended",
          form: "Login",
          eventCallback: function () {
            location.href = response.data.redirect;
          },
          eventTimeout: 2000,
        });
      } else {
        location.href = response.data.redirect;
      }
    },
    failed: (formData, instance, response) => {
      $error.innerHTML = response.data;
      setTimeout(() => {
        $error.innerHTML = "";
      }, 3000);
    },
  });
}
