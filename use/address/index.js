const { Feature } = require("@acanto/laravel-scripts/scripts/use");

class FeatureAddress extends Feature {
  routes() {
    return [];
  }

  components() {
    return [{ name: "Address" }];
  }

  middlewares() {
    return [];
  }
}

module.exports = FeatureAddress;
