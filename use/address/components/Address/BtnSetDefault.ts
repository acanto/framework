import { bindAddressBtnSetDefault } from "@acanto/core-address";
import "./BtnSetDefault.scss";

/**
 * Component: AddressBtnSetDefault
 */
export function AddressBtnSetDefault() {
  bindAddressBtnSetDefault();
}
