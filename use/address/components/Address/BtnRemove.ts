import { bindAddressBtnRemove } from "@acanto/core-address";
import "./BtnRemove.scss";

/**
 * Component: AddressBtnRemove
 */
export function AddressBtnRemove() {
  bindAddressBtnRemove();
}
