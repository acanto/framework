import { addressOn } from "@acanto/core-address";
import { AddressBtnRemove } from "components/Address/BtnRemove";
import { AddressBtnSetDefault } from "components/Address/BtnSetDefault";

/**
 * Component: AddressList
 */
export function AddressList() {
  AddressBtnRemove();
  AddressBtnSetDefault();

  addressOn("remove:ok", ({ $item }) => {
    $item.remove();
  });

  addressOn("setdefault:ok", () => {
    location.reload();
  });
}
