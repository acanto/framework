// import { $ } from "@acanto/core-dom";
import { AddressFormShipping } from "@acanto/core-address/form";
import FormsInputMaterial from "@acanto/core-forms/input/material";
import FormsSelectMaterial from "@acanto/core-forms/select/material";
import "./Shipping.scss";

/**
 * Component: AddressShipping
 */
export function AddressShipping($root?: HTMLElement) {
  AddressFormShipping($root);
  FormsInputMaterial();
  FormsSelectMaterial();
}
