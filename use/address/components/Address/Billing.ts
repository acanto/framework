// import { $ } from "@acanto/core-dom";
import { AddressFormBilling } from "@acanto/core-address/form";
import FormsInputMaterial from "@acanto/core-forms/input/material";
import FormsSelectMaterial from "@acanto/core-forms/select/material";
import "./Billing.scss";

/**
 * Component: AddressBilling
 */
export function AddressBilling($root?: HTMLElement) {
  AddressFormBilling($root);
  FormsInputMaterial();
  FormsSelectMaterial();
}
