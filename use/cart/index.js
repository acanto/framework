const { Feature } = require("@acanto/laravel-scripts/scripts/use");

class FeatureCheckout extends Feature {
  routes() {
    return [];
  }

  components() {
    return [{ name: "Cart" }, { name: "ItemCard" }];
  }

  middlewares() {
    return [];
  }
}

module.exports = FeatureCheckout;
