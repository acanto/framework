import { bindCartBtnAdd } from "@acanto/core-cart";
import "./BtnAdd.scss";

/**
 * Component: CartBtnAdd
 */
export function CartBtnAdd() {
  bindCartBtnAdd();
}
