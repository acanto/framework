<button class="CartBtnRemove:" data-cart-btn="remove">
  <span title="Remove from cart" data-cart-when="available">
    remove &#128722;
  </span>
  <span title="Processing..." data-cart-when="loading">
    <x-progress-circular />
  </span>
</button>
