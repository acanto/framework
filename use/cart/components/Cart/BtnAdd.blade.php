<button class="CartBtnAdd:" data-cart-btn="add">
  <span title="Add to cart">
    &#128722; add
  </span>
  <span title="Processing..." data-cart-when="loading">
    <x-progress-circular />
  </span>
</button>

