import { bindCartBtnRemove } from "@acanto/core-cart";
import "./BtnRemove.scss";

/**
 * Component: CartBtnRemove
 */
export function CartBtnRemove() {
  bindCartBtnRemove();
}
