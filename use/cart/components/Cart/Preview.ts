import { hydrateWishlist } from "@acanto/core-wishlist";
import { hydrateCart } from "@acanto/core-cart";
import "./Preview.scss";

/**
 * Component: CartPreview
 */
export function CartPreview() {
  hydrateWishlist(".CartPreview:");
  hydrateCart(".CartPreview:");
}
