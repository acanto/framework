import { initItems } from "@acanto/core-helpers/item";
// import { initWishlist } from "@acanto/core-wishlist";
import { initCart } from "@acanto/core-cart";
// import { WishlistBtn } from "components/Wishlist/Btn";
import { CartBtnAdd } from "components/Cart/BtnAdd";
import { CartBtnRemove } from "components/Cart/BtnRemove";
import FormsSelectMaterial from "@acanto/core-forms/select/material";
import "./index.scss";

/**
 * Component: ItemCard
 */
export function ItemCard() {
  initItems();
  // initWishlist();
  initCart();

  // WishlistBtn();
  CartBtnAdd();
  CartBtnRemove();

  FormsSelectMaterial();
}
