import { writeFileSync } from "node:fs";
import { join } from "node:path";
import { cwd } from "node:process";
import editJsonFile from "edit-json-file";
import { projects } from "../framework.config.js";

const generateVsCodeWorkspace = () => {
  const fullpath = join(cwd(), "acanto-framework.code-workspace");
  const file = editJsonFile(fullpath);
  const projectsFolders = [
    {
      name: "framework",
      path: ".",
    },
    ...projects.map((project) => ({
      name: project.name,
      path: `../${project.name}`,
    })),
  ];

  file.set("folders", projectsFolders);
  file.save();
};

const generateMuRepo = () => {
  const fullpath = join(cwd(), ".mu_repo");
  const projectsPaths = projects.map((project) => `../${project.name}`);
  const content = `repo=.
serial=True
current_group=acanto-framework-projects
group=acanto-framework-projects, ${projectsPaths.join(", ")}
`;
  writeFileSync(fullpath, content);
};

const run = () => {
  generateVsCodeWorkspace();
  generateMuRepo();
};

run();
