# Acanto Framework

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/1dfb471aa62649a29ffd71d1f386e7fe)](https://www.codacy.com/gl/acanto/framework/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=acanto/framework&utm_campaign=Badge_Grade)
[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/) [![pipelines](https://gitlab.com/acanto/framework/badges/main/pipeline.svg)](https://gitlab.com/acanto/framework/-/pipelines)

Monorepo for the whole acanto framework, offical documentation at [acanto.gitlab.io/framework](https://acanto.gitlab.io/framework)

## Contribute

Clone this repo locally and install dependencies running

```bash
npm i
```

Link all packages with:

```bash
npm run link
```

Now in your test project bootstrapped with `@acanto/create-laravel-app` you can run `npm run link` to use the globally symlinked packages from your machine.

### Publish packages

First commit and push your local work. Then, never manually bump package versions, just run form the terminal:

```bash
npm run publish
```

It uses `lerna` under the hood, the follow its interactive tool to semver each modified package.

NB: The only file you might need to manually bump is [`./workflow/laravel-template/template/composer.json`](/-/tree/main/workflow/laravel-template/template/composer.json) with the latest `acanto/laravel-frontend` version. This is probably required only for **major** semver version changes and it is likely better to keep a non-strict version here as its only purpose it is to bootstrap the project with the latest available major release.

### Inner dependencies

Packages in monorepos can depend on each other internally, use [lerna](https://github.com/lerna/lerna) for this.
If unsure on how to define the right version use the [`semver calculator online tool`](https://semver.npmjs.com/).

### Manage dependent projects

This repository contain a `.code-workspace` file for [VS code](https://code.visualstudio.com/docs/editor/workspaces) and a `.mu_repo` file for [mu-repo](https://fabioz.github.io/mu-repo/) to conveninetly upgrade projects dependent on this framework.

### Dev notes

- About managing polyglot monorepo (with both npm packages and composer packages) see this [test monorepo](https://github.com/newism/lerna) and its [consumer](https://github.com/newism/lerna-sub)
- About monorepo tools see [npm 7 workspaces](https://docs.npmjs.com/cli/v7/using-npm/workspaces), [awesome-monorepo](https://github.com/korfuri/awesome-monorepo), [lage](https://microsoft.github.io/lage/) and [11 Great Tools for a Monorepo in 2021](https://blog.bitsrc.io/11-tools-to-build-a-monorepo-in-2021-7ce904821cc2)
- About multi git repo tools see [meta](https://github.com/mateodelnorte/meta)
- About typescript see [typedoc](http://typedoc.org/) and [ts-toolbelt](https://millsp.github.io/ts-toolbelt/index.html)
- About webpack hot module replacement see [A Deep Dive into Hot Module Replacement with Webpack (Part Two - Handling Updates)](https://blog.nativescript.org/deep-dive-into-hot-module-replacement-with-webpack-part-two-handling-updates/)
- In VScode find and replace the following regex to cleanup `gitHead` lerna stuf fin `package.json`s:

```text
,
  "gitHead": .*$
```

- Switch between php versions on linux `sudo update-alternatives --set php /usr/bin/php` (then press tab for autocomlete options).

### TODO

- **cache**: svuotamento della cache. la `structure` sembra non svuotarsi, vedi ify, su olivieri anche. Entrambi sono su wps su serverplan. Magari è un problema di permessi o di memoria.
- **preview**: via URL param applicato solo al base controller di ogni route.
- **cache**: workflow/laravel-frontend/src/Forms/Fillform.php#182
