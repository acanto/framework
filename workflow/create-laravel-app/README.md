# Create Laravel App

> For all the documentation refer to the [Acanto Framework Docs](https://acanto.gitlab.io/framework/)

## Quickstart

```console
npx @acanto/create-laravel-app myproject
cd myproject
npm start
```
