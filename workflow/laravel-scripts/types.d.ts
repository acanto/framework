declare module NodeJS {
  interface Global {
    __FREEPORT: number;

    __IPS: {
      local: string;
      cms: string;
      fillform: string;
      auth: string;
      all: Set<string>;
    };
  }
}
