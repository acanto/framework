const path = require("path");
const cliPkg = require("../package.json");
const pkgPath = path.join(process.cwd(), "package.json");
const prettier = require("../scripts/prettier/config");
// the project's package.json content
const pkg = require(pkgPath);

module.exports = {
  npmScope: (pkg.config && pkg.config.npmScope) || "@acanto",
  coreLibrary: (pkg.config && pkg.config.coreLibrary) || "@acanto/core",
  componentsLibrary:
    (pkg.config && pkg.config.componentsLibrary) || "@acanto/components",
  useLibrary: (pkg.config && pkg.config.useLibrary) || "@acanto/use",
  isProduction: process.env.NODE_ENV === "production",
  isHttps() {
    return /^https:\/\//.test(process.env.APP_URL);
  },
  devSourceMaps() {
    return process.env.DEV_SOURCEMAPS === "true";
  },
  devWebpackDebug() {
    return process.env.DEV_WEBPACK_DEBUG === "true";
  },
  devWebpackCache() {
    return process.env.DEV_WEBPACK_CACHE;
  },
  env() {
    return process.env.APP_ENV;
  },
  isCI() {
    const ci = require("ci-info");
    // return true; // to quickly debug CI only behaviours
    return ci.isCI;
  },
  cliPkg,
  pkg,
  filenames: {
    assetsHeadPartial: "assets-head.blade.php",
    assetsBodyPartial: "assets-body.blade.php",
    faviconsPartial: "favicons.blade.php",
    svgIconsPartial: "svgicons.blade.php",
    routesBarba: "routesBarba.js",
    middlewares: "Middlewares.php",
  },
  // https://browserl.ist/
  browserslist: ["last 2 versions", "> 1%"],
  husky: {
    hooks: {
      "pre-commit": "lint-staged",
    },
  },
  prettier: prettier.PRETTIER_CONFIG,
  lintStaged: {
    [prettier.PRETTIER_PATH]: "prettier --write",
  },
};
