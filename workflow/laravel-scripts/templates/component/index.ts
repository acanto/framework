import "./index.scss";
// import { $ } from "@acanto/core-dom";

/**
 * Component: <%= component.className %>
 */
export function <%= component.className %>() {
  console.log("<%= component.className %> mounted.");
}
