# <%= title %>

> Refer to the documentation of the [Acanto Framework](https://acanto.gitlab.io/framework)

## Installation

```bash
git clone <%= repo.ssh %>
cd <%= repo.name %>
npm i
composer i
npm start
```

## Urls
<% for (var i = 0; i < envs.length; i++) { var env = envs[i]; %>
- `<%= env.APP_ENV %>`: [app](<%= env.APP_URL %>), [cms-api](<%= env.CMS_API_URL %>)<% if (env.AUTH_API_URL && env.AUTH_API_URL !== "false") { %>, [auth-api](<%= env.AUTH_API_URL %>)<% } %>;<% } %>

*To copy paste in gitlab's project description*:

```text
Frontend: <% for (var i = 0; i < envs.length; i++) { var env = envs[i]; %>[<%= env.APP_ENV %>](<%= env.APP_URL %>)<% if (i < envs.length - 1) { %>, <% } %><% } %>
```
