module.exports = function preinstall() {
  const fs = require("fs");
  const paths = require("../../paths");

  if (fs.existsSync(paths.join(paths.root, "package-lock.json"))) {
    const execSync = require("child_process").execSync;
    execSync("npx npm-force-resolutions");
  }
};
