const fs = require("fs");
const path = require("path");
const paths = require("../../paths");
const transform = require("./parser-route");

const templateName = process.argv.slice(3, 4)[0];
const templatePath = path.join(
  paths.frontend.src.routes,
  templateName,
  `index.blade.php`
);
const outputPath = path.join(
  paths.frontend.src.routes,
  templateName,
  `index--auto.scss`
);

fs.readFile(templatePath, "utf8", function (err, data) {
  if (err) throw err;

  const output = transform(data);

  fs.writeFile(outputPath, output, function (err) {
    if (err) {
      return console.log(err);
    }
    console.log(`Generated routes/${templateName}/index--auto.scss`);
  });
});
