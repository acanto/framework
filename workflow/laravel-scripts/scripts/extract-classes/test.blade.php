{% partial "components/Header" useSchema=true %}

<section class="intro:">
  {% partial "media/video" 
    src='assets/images/'|theme~'/pages/home/franchising-negozi-abbigliamento-gruppo-teddy.mp4'
    alt='homepage.alt4' | _
    poster='assets/images/'|theme~'/pages/home/video_poster_opt.jpg'
    width=1310
    height=432
    attrs="autoplay loop muted"
    classRoot="intro:video"
  %}
  <div class="intro:overlay"></div>
  <div class="intro:wrapSlider">
    <div class="intro:slider glide">
      <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides">
          {% for slide in slider %}
            <li class="glide__slide intro:slide">
              <div class="container-xl">
                <div class="intro:slide__title">
                  {{ slide.title | _ }}
                </div>
                <a href="{{ slide.url | page }}" class="intro:slide__cta btn btn--outlined">
                  {{ slide.cta | _ }}
                </a>
              </div>
            </li>
          {% endfor %}
        </ul>
      </div>
      <div class="intro:slider__bullets container-xl">
        <div class="glide__bullets" data-glide-el="controls[nav]">
          {% for slide in slider %}
            <button class="glide__bullet" data-glide-dir="={{ loop.index - 1 }}">
            </button>
          {% endfor %}
        </div>
      </div>
    </div>
  </div>
</section>
<section class="about: about:--cool">
  <div class="container-xl">
    {% partial "media/img" 
      local_src='/pages/home/gruppo-teddy-negozi-abbigliamento-franchising.jpg'
      alt='homepage.alt1' | _
      spinner=false
      classRoot='sceneBg'
    %}
    <div class="row">
      <div class="about:images">
        <div class="about:images__scene" data-onscroll="scene">
          {% partial "media/img" 
            local_src='/pages/home/aprire-negozio-franchising-gruppo-teddy.png'
            alt='homepage.alt3' | _
            spinner=false
            classRoot='sceneImg img-shoe'
          %}
          {% partial "media/img" 
            local_src='/pages/home/gruppo-teddy-negozi-abbigliamento-franchising-conto-vendita.png'
            alt='homepage.alt5' | _
            spinner=false
            classRoot='sceneImg img-rose'
          %}
          {% partial "media/img" 
            local_src='/pages/home/negozi-abbigliamento-franchising-gruppo-teddy.png'
            alt='homepage.alt2' | _
            spinner=false
            classRoot='sceneImg img-c'
          %}
          {% partial "media/img" 
            local_src='/pages/home/negozio-franchising-gruppo-teddy.png'
            alt='homepage.alt9' | _
            spinner=false
            classRoot='sceneImg img-plant'
          %}
        </div>
      </div>
      <div class="about:text">
        <h1 class="about:text__title">
          {{ 'home.company.h1' | _ }}
        </h1>
        <div class="row about:text__desc" itemprop="description">
          <div class="about:text__descCol">
            {{ 'home.company.text1' | _ | raw }}
          </div>
          <div class="about:text__descCol">
            {{ 'home.company.text2' | _ | raw }}
          </div>
        </div>
        <div class="row about:text__cta">
          <div class="about:text__ctaCol">
            <a data-brand="teddy" class="btn btn--video">
              {{ 'cta.guardavideo' | _ }} {% partial "play" %}
            </a>
          </div>
          <div class="about:text__ctaCol">
            <a href="#formula" class="btn btn--formula" data-smooth>
              {{ 'home.cta.formula' | _ }}
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="brands:" style="background-image: url({{ 'assets/images/'|theme~'/pages/home/gruppo-teddy-negozi-abbigliamento-franchising.jpg' }})">
  <div class="brands:bg" data-brand="{{ brands[0].slug }}"></div>
  <div class="brands:fg">
    <div class="container-xl">
      <div class="row">
        <h2 class="brands:title">
          {{ 'home.brand.h2' | _ }}
        </h2>
      </div>
    </div>
    <div class="container-xl">
      <div class="row brands:area brands:area--mod">
        <div class="brands:area__info">
          <div class="brands:area__infoSlider brands:area__infoSlider--ciao glide">
            <div class="glide__track" data-glide-el="track">
              <ul class="glide__slides">
                {% for brand in brands %}
                <li class="glide__slide brand:">
                  <p class="brand:name" itemprop="brand">{{ brand.name | _ }}</p>
                  <div class="brand:logo">
                    {% set logo = brand.logo_home ? brand.logo_home : brand.logo %}
                    {% partial "media/img" local_src=logo.src alt=brand.name | _ %}
                  </div>
                  <h3 class="brand:desc">
                    {{ brand.desc | _ }}
                  </h3>
                  <a href="{{ brand.slug | page }}" class="brand:cta btn btn--outlined">
                    {{ 'cta.scopridettagli' | _ }}
                  </a>
                </li>
                {% endfor %}
              </ul>
            </div>
          </div>
        </div>
        <div class="brands:area__cards">
          <div class="brands:area__cardsSlider glide">
            <div class="brands:sticks" data-onscroll="sticks">
              {% for i in range(0, 5) %}
                <span class="stick"></span>
              {% endfor %}
            </div>
            <div class="glide__maskLeft">
              <div class="glide__maskRight">
                <div class="glide__track" data-glide-el="track">
                  <ul class="glide__slides">
                    {% for brand in brands %}
                    <li class="glide__slide">
                      <div class="inner">
                        {% partial "media/img" 
                          local_src=brand.img_home.src
                          alt=brand.img_home.alt | _
                          fullwidth=true
                          bgColor=brand.color_bg
                        %}
                        <div class="overlay" style="background-color: {{ brand.color_overlay }}"></div>
                      </div>
                    </li>
                    {% endfor %}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-xl">
      <div class="row brands:bottom">
        <div class="brands:bottom__controls" data-glide-el="controls">
          <span class="glide__arrow prev" data-glide-dir="<">
            {% partial "icon" glyph="arrow_prev" %}
          </span>
          <span class="glide__arrow next" data-glide-dir=">">
            {% partial "icon" glyph="arrow_next" %}
          </span>
        </div>
        <div class="brands:bottom__video">
          <a data-brand="{{ brands[0].slug }}" class="brands:bottom__videoCta btn btn--video">
            {{ 'cta.guardavideo' | _ }} {% partial "play" %}
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="formula" class="formulas:">
  <div class="container-xl">
    <div class="row">
      <h2 class="formulas:title">
        {{ 'home.formula.h2' | _ }}
      </h2>
    </div>
    <div class="row formulas:cards glide">
      <div class="glide__track" data-glide-el="track">
        <ol class="glide__slides">
          {% for formula in formulas %}
          <li class="glide__slide formulas:cards__slide" data-idx="{{ loop.index - 1 }}">
            <span class="formulas:cards__slideNumber">{{ loop.index }}</span>
            <p class="formulas:cards__slideTitle">{{ formula.title | _ }}</p>
          </li>
          {% endfor %}
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="formulas:texts">
        <div class="formulas:texts__slider glide">
          <div class="glide__track" data-glide-el="track">
            <ol class="glide__slides">
              {% for formula in formulas %}
              <li class="glide__slide">
                <p>{{ formula.text | _ | raw }}</p>
              </li>
              {% endfor %}
            </ol>
          </div>
        </div>
        <div class="formulas:cta">
          <a href="{{ 'formula' | page }}" class="btn btn--outlined">
            {{ 'home.formula.cta' | _ }}
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

{% partial "components/Footer" schema='true' %}

{% partial "json" key="home" data=js_data %}
