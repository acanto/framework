const fs = require("fs");
const path = require("path");
const paths = require("../../paths");
const { pascalCase } = require("change-case");
const transform = require("./parser-component");

const templateName = pascalCase(process.argv.slice(3, 4)[0]);
const templatePath = path.join(
  paths.frontend.src.components,
  templateName,
  `index.blade.php`
);
const outputPath = path.join(
  paths.frontend.src.components,
  templateName,
  `index--auto.scss`
);

fs.readFile(templatePath, "utf8", function (err, data) {
  if (err) throw err;

  const output = transform(data);

  fs.writeFile(outputPath, output, function (err) {
    if (err) {
      return console.log(err);
    }
    console.log(`Generated components/${templateName}/index--auto.scss`);
  });
});
