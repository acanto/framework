const fs = require("fs");
const path = require("path");
const transform = require("./parser");

const templateName = "test";
const templatePath = path.join(__dirname, `./${templateName}.blade.php`);
const outputPath = path.join(__dirname, `./${templateName}.scss`);

fs.readFile(templatePath, "utf8", function (err, data) {
  if (err) throw err;

  console.log("Start parsing template: " + templateName);

  const output = transform(data);

  fs.writeFile(outputPath, output, function (err) {
    if (err) {
      return console.log(err);
    }
    console.log("Generated scss file!");
  });
});
