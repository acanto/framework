const PRETTIER_PATH =
  "(src|config)/**/!(assets)/**/*.{js,jsx,ts,tsx,json,css,scss,md,yml,yaml,php}";

const PRETTIER_CONFIG = {
  overrides: [
    {
      files: "*.php",
      options: {
        singleQuote: true,
      },
    },
  ],
};

module.exports = {
  PRETTIER_PATH,
  PRETTIER_CONFIG,
};
