module.exports = function prettier() {
  const config = require("./config");
  const spawn = require("@acanto/workflow-utils/crossSpawn");

  spawn("prettier", ["--write", config.PRETTIER_PATH], {
    stdio: "inherit",
  });
};
