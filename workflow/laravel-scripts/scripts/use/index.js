const glob = require("glob");
const prompts = require("prompts");
const fs = require("fs");
const fsExtra = require("@acanto/workflow-utils/fs");
const log = require("@acanto/workflow-utils/log");
const chalk = require("@acanto/workflow-utils/chalk");
const execSync = require("child_process").execSync;
const path = require("path");
const paths = require("../../paths");

/**
 * @typedef {object} FeatureMetadata
 * @property {string} path The path to the node_module for this feature
 * @property {string} name Grabbed and cleaned from the package.name
 * @property {string} version Grabbed from the package.version
 * @property {boolean} local Wethere we are using a symlinked version of the package
 * @property {string} title
 * @property {string} description
 * @property {string} fullName
 * @property {string} initialiser Path to the index.js to require, this file will exports as default with the FeatureClass to initialise
 */

/**
 * @typedef {object} FeatureComponent
 * @property {string} name The component name
 */

/**
 * @typedef {object} FeatureRoute
 * @property {string} name The route name/id
 * @property {string[]} [alternativeNames] Alternative route names to check for existence if the default one is missing
 */

/**
 * @typedef {object} FeatureMiddleware
 * @property {string} name The middleware filename
 */

/**
 * Feature base class that `use-` packages must extend
 *
 * @abstract
 */
class FeatureBase {
  /**
   * @type {FeatureMetadata}
   */
  info = null;

  /**
   * @param {FeatureMetadata} info
   */
  constructor(info) {
    this.info = info;
    const { title, version } = info;

    console.log(
      `Using ${chalk.bold(chalk.cyanBright(title))} v${chalk.bold(version)}...`
    );

    this.run();

    // console.groupEnd();
  }

  async run() {
    const components = this.components();
    const routes = this.routes();
    const middlewares = this.middlewares();

    await this._manageComponents(components);
    await this._manageRoutes(routes);
    await this._manageMiddlewares(middlewares);

    console.log(`${chalk.bold(chalk.cyanBright(this.info.title))} is set up.`);
  }

  /**
   * To override in subclasses
   *
   * @abstract
   * @returns {FeatureComponent[]}
   */
  components() {
    return [];
  }

  /**
   * To override in subclasses
   *
   * @abstract
   * @returns {FeatureRoute[]}
   */
  routes() {
    return [];
  }

  /**
   * To override in subclasses
   *
   * @abstract
   * @returns {FeatureRoute[]}
   */
  middlewares() {
    return [];
  }

  _startLog(subject) {
    console.log(`
Manage ${chalk.cyanBright(this.info.title)}'s ${chalk.bold(
      chalk.blue(subject)
    )}...`);
  }

  _endLog(subject) {
    // console.groupEnd();
  }

  /**
   * Manage routes
   *
   * @param {FeatureRoute[]} routes
   */
  async _manageRoutes(routes) {
    if (!routes.length) {
      return;
    }

    this._startLog("routes");

    const map = {
      existing: [],
      missing: [],
    };

    // check the feature routes and divide them by those existing in the project
    // and those who do not exist
    routes.forEach(({ name, alternativeNames = [] }) => {
      let exists = false;
      const to = path.join(paths.frontend.src.routes, name);

      if (fs.existsSync(to)) {
        exists = true;
      } else {
        alternativeNames.forEach((alternativeName) => {
          const to = path.join(paths.frontend.src.routes, alternativeName);
          if (fs.existsSync(to)) {
            exists = true;
          }
        });
      }

      if (exists) {
        map.existing.push(name);
      } else {
        map.missing.push(name);
      }
    });

    // report the routes that exist already
    if (map.existing.length) {
      console.log(
        chalk.white(`
Routes ${chalk.bold(map.existing.join(", "))} already exist in your project.`)
      );
    }

    // manage the creation of the missing routes
    if (map.missing.length) {
      const response = await prompts({
        type: "multiselect",
        name: "toCreate",
        instructions: false,
        min: 0,
        hint: "Select/deselect pressing the space bar",
        message: "The following routes are missing, select those to create",
        choices: map.missing.map((name) => ({
          title: name,
          value: name,
          selected: true,
        })),
      });

      // automatically create the selected missing routes
      if (response.toCreate.length) {
        // the use feature can have some routes default scaffolding in its
        // `/routes` subfolder...
        const withScaffolding = glob
          .sync("*", { cwd: path.join(this.info.path, "routes") })
          .filter((name) => response.toCreate.includes(name));

        // ... or it cannot have it, in that case we just use `npx acanto route`
        // to automatically create them
        const withoutScaffolding = response.toCreate.filter(
          (name) => !withScaffolding.includes(name)
        );

        withScaffolding.forEach((name) => {
          const from = path.join(this.info.path, "routes", name);
          const to = path.join(paths.frontend.src.routes, name);

          fsExtra.copy(from, to);
        });

        if (withoutScaffolding.length) {
          execSync(`npx acanto route ${withoutScaffolding.join(",")}`);
        }

        console.log(
          chalk.white(`
Created missing routes ${chalk.bold(response.toCreate.join(", "))}
`)
        );
      }

      // warn if some mandatory routes are still missing as the user has not
      // selected them via the prompts CLI interface
      if (response.toCreate.length < map.missing.length) {
        const stillMissing = map.missing.filter(
          (name) => !response.toCreate.includes(name)
        );

        console.log(
          chalk.redBright(`
The following routes are still missing: ${chalk.bold(stillMissing.join(", "))}
`)
        );
      }
    }

    this._endLog();
  }

  /**
   * Manage components
   *
   * @param {FeatureComponent[]} components
   */
  async _manageComponents(components) {
    if (!components.length) {
      return;
    }

    this._startLog("components");

    const map = {
      existing: [],
      missing: [],
    };

    // check the feature components and divide them by those existing in the project
    // and those who do not exist
    components.forEach(({ name }) => {
      const to = path.join(paths.frontend.src.components, name);

      if (fs.existsSync(to)) {
        map.existing.push(name);
      } else {
        map.missing.push(name);
      }
    });

    // report that some components already exist and won't be copied
    if (map.existing.length) {
      console.log(`
${chalk.white(
  `Components ${chalk.bold(
    map.existing.join(", ")
  )} already exist in your project.`
)}
${chalk.dim(`The homonym ${chalk.bold(
  this.info.name
)}'s feature components will not be copied over now.
If you like you might rename these components in your project and re-run ${chalk.bold(
  "npx acanto use"
)}`)}`);
    }

    // create instead the other components
    if (map.missing.length) {
      const response = await prompts({
        type: "multiselect",
        name: "toCreate",
        instructions: false,
        min: 0,
        hint: "Select/deselect pressing the space bar",
        message: "Select the components to bootstrap in your project",
        choices: map.missing.map((name) => ({
          title: name,
          value: name,
          selected: true,
        })),
      });

      // automatically create the selected missing components
      if (response.toCreate.length) {
        map.missing.forEach((name) => {
          const from = path.join(this.info.path, "components", name);
          const to = path.join(paths.frontend.src.components, name);

          fsExtra.copy(from, to);
        });

        console.log(
          chalk.white(
            `Created components ${chalk.bold(response.toCreate.join(", "))}`
          )
        );
      }
    }

    this._endLog();
  }

  /**
   * Manage middlewares
   *
   * @param {FeatureMiddleware[]} middlewares
   */
  async _manageMiddlewares(middlewares) {
    if (!middlewares.length) {
      return;
    }

    this._startLog("middlewares");

    const map = {
      existing: [],
      missing: [],
    };

    // check the feature middlewares and divide them by those existing in the project
    // and those who do not exist
    middlewares.forEach(({ name }) => {
      const to = path.join(paths.frontend.src.middlewares, `${name}.php`);

      if (fs.existsSync(to)) {
        map.existing.push(name);
      } else {
        map.missing.push(name);
      }
    });

    // report that some middlewares already exist and won't be copied
    if (map.existing.length) {
      console.log(`
${chalk.white(
  `Middlewares ${chalk.bold(
    map.existing.join(", ")
  )} already exist in your project.`
)}
${chalk.dim(`The homonym ${chalk.bold(
  this.info.name
)}'s feature middleware will not be copied over now.
If you like you might rename these middlewares in your project and re-run ${chalk.bold(
  "npx acanto use"
)}`)}`);
    }

    // create instead the other components
    if (map.missing.length) {
      const response = await prompts({
        type: "multiselect",
        name: "toCreate",
        instructions: false,
        min: 0,
        hint: "Select/deselect pressing the space bar",
        message: "Select the middlewares to copy in your project",
        choices: map.missing.map((name) => ({
          title: name,
          value: name,
          selected: true,
        })),
      });

      // automatically create the selected missing middlewares
      if (response.toCreate.length) {
        map.missing.forEach((name) => {
          const from = path.join(this.info.path, "middlewares", `${name}.php`);
          const to = path.join(paths.frontend.src.middlewares, `${name}.php`);

          fsExtra.copy(from, to);
        });

        console.log(
          chalk.white(
            `Created middlewares ${chalk.bold(response.toCreate.join(", "))}`
          )
        );
      }
    }

    this._endLog();
  }
}

/**
 * Get available features by globbing the node_modules that match the `use-`
 * naming convention
 *
 * @returns {FeatureMetadata[]}
 */
function getAvailableFeatures() {
  const { sentenceCase } = require("change-case");
  const { npmScope } = require("../../config");
  const globPath = `${npmScope}/use-*`;

  return glob.sync(globPath, { cwd: paths.nodeModules }).map((pkgName) => {
    const path = paths.join(paths.nodeModules, pkgName);
    const { version, description } = require(paths.join(path, "/package.json"));
    const local = fs.lstatSync(path).isSymbolicLink();
    const name = pkgName.replace(npmScope, "").replace("/use-", "");
    const title = sentenceCase(name);
    const fullName = `${chalk.bold(title)} v${version} ${
      !local ? chalk.yellow(`(local)`) : ""
    }${chalk.dim(description ? `: ${description}` : "")}`;
    const initialiser = paths.join(path, "/index.js");

    return {
      path,
      name,
      version,
      local,
      title,
      description,
      fullName,
      initialiser,
    };
  });
}

/**
 * Run the `use` command
 *
 * @returns
 */
async function run() {
  const features = getAvailableFeatures();

  const response = await prompts({
    type: "multiselect",
    name: "useFeatures",
    instructions: false,
    min: 1,
    hint: "Select one or many pressing the space bar",
    message: "Select which features you want to bootstrap",
    choices: features.map((feature) => ({
      title: feature.fullName,
      value: feature.initialiser,
    })),
  });

  response.useFeatures.forEach((initialiser) => {
    const Feature = require(initialiser);
    new Feature(features.filter((meta) => meta.initialiser === initialiser)[0]);
  });
}

module.exports = {
  Feature: FeatureBase,
  run,
};
