const log = require("@acanto/workflow-utils/log");
const chalk = require("@acanto/workflow-utils/chalk");
const Crawler = require("crawler");
const crawler = new Crawler();
const visitedUrls = [];

/**
 * SHould visit URL
 *
 * @param {string} baseUrl
 * @param {string} url
 * @returns
 */
function shouldVisitUrl(baseUrl, url) {
  if (url && url.indexOf(baseUrl) !== -1) {
    return true;
  }
  return false;
}

/**
 * Normalise link href attribute value
 *
 * @param {string} baseUrl
 * @param {string} [href='']
 * @returns {string | false}
 */
function normaliseHref(baseUrl, href = "") {
  const { normaliseUrl } = require("../../utils");
  href = href.trim();

  if (
    href.startsWith("#") ||
    href.startsWith("mailto") ||
    href.startsWith("tel")
  ) {
    return false;
  }
  if (href.startsWith("http")) {
    return href;
  }
  return normaliseUrl(baseUrl + "/" + href);
}

/**
 * Visit all links
 *
 * @param {string} baseUrl
 * @param {string} url
 */
function visitLinks(baseUrl, url, end) {
  crawler.queue({
    uri: url,
    rateLimit: 100,
    preRequest: function (options, done) {
      if (baseUrl.indexOf(".acanto.net") !== -1) {
        options.auth = {
          user: "acanto",
          pass: "acanto",
          sendImmediately: true,
        };
      }
      done();
    },
    callback: function (err, res, done) {
      if (err) throw err;
      let $ = res.$;
      try {
        const urls = $("a");

        Object.keys(urls).forEach((item) => {
          if (urls[item].type === "tag") {
            const url = normaliseHref(baseUrl, urls[item].attribs.href);
            if (shouldVisitUrl(baseUrl, url) && !visitedUrls.includes(url)) {
              visitedUrls.push(url);

              visitLinks(baseUrl, url);
            }
          }
        });
      } catch (e) {
        console.error(`Encountered an error crawling ${url}. Aborting crawl.`);
        done();
      }
      done();
    },
  });
}

/**
 * Get base url for `visit` script, if defined, it grabs the base url from the
 * given environment variable
 *
 * @returns {string}
 */
function getBaseUrl() {
  // reload current env to be sure...
  require("dotenv").config();
  const ci = require("ci-info");
  const { getDataForEnv } = require("../../utils");
  let appUrl = process.env.APP_URL;

  if (!ci.isCI) {
    const env = process.argv.slice(2)[1];

    if (env) {
      const envData = getDataForEnv(env);
      appUrl = envData.APP_URL || "";
    }
  }

  return appUrl;
}

/**
 * Visitor report stats
 *
 * @param {string} baseUrl
 */
function visitReport(baseUrl) {
  const urls = [];
  const summary = `Visited ${chalk.bold(visitedUrls.length)} urls`;

  log(chalk.greenBright(`${summary}:`));

  for (let i = 0; i < visitedUrls.length; i++) {
    const url = visitedUrls[i].replace(baseUrl, "");
    urls.push(url);
    log(chalk.green(url));
  }
  log(chalk.greenBright(`${summary}.`));
}

/**
 * Visit script
 */
module.exports = function visit(callback) {
  const baseUrl = getBaseUrl();

  log(chalk.yellow(`Start visiting all urls from ${chalk.bold(baseUrl)}`));

  visitLinks(baseUrl, baseUrl);

  crawler.on("drain", () => {
    visitReport(baseUrl);
    if (callback) callback();
  });
};
