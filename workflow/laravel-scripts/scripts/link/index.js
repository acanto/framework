const paths = require("../../paths");
const { npmScope } = require("../../config");
const fs = require("@acanto/workflow-utils/fs");
const log = require("@acanto/workflow-utils/log");
const glob = require("glob");
const globPath = `${npmScope}/*`;

/**
 * Ignore old @acanto pacakges that we do not use anymore and whose `bin`
 * conflicts with the new ones
 */
const blacklist = [
  "@acanto/frontend",
  "@acanto/workflow",
  "@acanto/october-scripts",
];

/**
 * Try npm link for each npm candidate package
 */
function tryNpmLink() {
  // const candidatesLocallyInstalled = glob.sync(globPath, { cwd: paths.nodeModules });
  const execSync = require("child_process").execSync;
  const npmGlobalPath = execSync("npm root -g").toString().trim();
  const candidatesGloballyLinked = glob.sync(globPath, { cwd: npmGlobalPath });
  const linked = [];

  candidatesGloballyLinked.forEach((name) => {
    const pkgName = name.trim();
    if (!blacklist.includes(pkgName)) {
      const origin = paths.join(npmGlobalPath, pkgName);
      const destination = paths.join(paths.nodeModules, pkgName);
      try {
        if (fs.existsSync(destination)) {
          fs.rmdirSync(destination, { recursive: true });
        }

        fs.symlinkSync(origin, destination, "dir");
        // execSync(`npm link ${pkgName}`, { cwd: paths.root, stdio: "inherit" });
        linked.push(pkgName);
      } catch (e) {
        console.warn(`Could not npm link package ${pkgName}`);
      }
    }
  });

  if (linked.length) {
    log(`The following globally linked packages have been succesfully linked in your project 'node_modules/${globPath}':

${linked.join(", ")}
`);
  } else {
    log(`No packages were linked :/.`);
  }
}

function tryComposerLink() {
  const del = require("del");
  const src = paths.join(paths.root, "../framework/workflow/laravel-frontend");
  const dest = paths.join(paths.vendor, "/acanto/laravel-frontend");
  const fs = require("fs");

  if (fs.existsSync(src)) {
    if (fs.existsSync(dest)) {
      del.sync(dest);
    }
    fs.symlinkSync(src, dest);
    log(`Symlinked composer package acanto/laravel-frontend`);
  } else {
    log(`Could not link composer package acanto/laravel-frontend`);
  }
}

tryNpmLink();
tryComposerLink();
