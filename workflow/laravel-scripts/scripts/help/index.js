const commandLineUsage = require("command-line-usage");

// prettier-ignore
const logo = `
      @@@@@@@@      
   @@@@@@@@@@@@@@   
 @@@@@@@@@@@@@@@@@@ 
@@@@@@@      @@@@@@@
@@@@@@   @@@  @@@@@@
@@@@@@   @@@  @@@@@@
@@@@@@@         @@@@
 @@@@@@@@@@@@@@@@@@ 
   @@@@@@@@@@@@@@   
      @@@@@@@@      
`;

const sections = [
  {
    header: "Acanto's Laravel scripts",
    content: {
      options: {
        columns: [
          { name: "one", width: 24, noWrap: true },
          { name: "two", maxWidth: 60 },
        ],
      },
      data: [
        {
          one: logo,
          two: `Scripts to manage Laravel Frontend applications.\r\n\r\n
            Usage is simply {cyanBright.bold npx acanto <command>} provided that you have set aliases in your package.json`,
        },
      ],
    },
  },
  {
    header: "Command List",
    content: [
      {
        name: "start",
        summary:
          "Run full local development server with live reload and assets compilation",
      },
      { name: "build", summary: "Build the production code" },
      { name: "clear", summary: "Clear Laravel local caches" },
      {
        name: "clean",
        summary: "Clean storage and page cache",
      },
      {
        name: "wipe",
        summary:
          "Wipe out everything: Laravel caches, storage and all compiled files",
      },
      {
        name: "route",
        summary:
          "Generate a php, blade, scss and js for a specific route. Accept a single string {bold route my-route-name} or a comma separated list of names {bold route home,404,500,contacts}. Pass the option {bold --barba} if the route uses barba.js.",
      },
      {
        name: "component",
        summary:
          "Generate a php, blade, scss and js for a specific component. Accept a single string {bold component my-component-name} or a comma separated list of names {bold component header,footer,card}",
      },
      {
        name: "core",
        summary:
          "Copy core elements to current project from the @acanto/core libraries.",
      },
      {
        name: "prettify",
        summary:
          "Prettify js, scss, md and php files (it is done automatically before every commit)",
      },
      {
        name: "test",
        summary:
          "Build the production code and start a local server running it",
      },
      {
        name: "svgicons",
        summary:
          "Just builds the svg icons (this is always run on {bold build})",
      },
      {
        name: "visit",
        summary:
          "Visit all website URLs, it uses the current env, or a specific one can be given as argument {bold visit production}",
      },
      {
        name: "deploy",
        summary: "This is meant to be used {bold only by the CI}",
      },
      { name: "help", summary: "Display this help information." },
    ],
  },
];

const usage = commandLineUsage(sections);

module.exports = function help() {
  console.log(usage);
};
