const path = require("path");
const paths = require("../paths");
const { getPublicUrls } = require("../utils");
const { isProduction } = require("../config");
const { getOutputName } = require("./helpers");

module.exports = {
  path: path.resolve(paths.frontend.dest.assets),
  publicPath: getPublicUrls().assets,
  // @see https://webpack.js.org/guides/build-performance/#output-without-path-info
  pathinfo: isProduction ? false : true,
  filename: getOutputName("entry", "js"),
  chunkFilename: getOutputName("chunk", "js"),
  crossOriginLoading: "anonymous", // maybe not needed but it doesn't hurt
};
