/**
 * Get server host
 *
 * @returns {string}
 */
function getServerHost() {
  const host = "localhost"; // "myproject.test";
  return host.replace(/\/+$/g, "");
}

/**
 * Get server port
 *
 * @returns {number}
 */
function getServerPort() {
  return global.__FREEPORT;
}

/**
 * Get js/css output filename
 *
 * @param {"entry" | "chunk"} type
 * @param {"js" | "css"} ext
 */
function getOutputName(type, ext) {
  const { isProduction } = require("../config");
  const { frontend } = require("../paths");

  if (isProduction) {
    ext = `[contenthash].${ext}`;
  }

  // asyn chunks usually don't have a name, if they do we just use it during dev
  // as it increases bundle size, @see https://bit.ly/3cGkYif
  if (type === "chunk" && isProduction) {
    return `${frontend.dest.folders.chunks}/[id].${ext}`;
  }

  return `${frontend.dest.folders.entries}/[name].${ext}`;
}

/**
 * Get resource type setting for webpack's CopyPlugin
 *
 * @param {"images" | "fonts" | "media"} type
 * @param {boolean} [hashable]
 */
function getCopySetting(type, hashable) {
  const paths = require("../paths");
  const { isProduction } = require("../config");

  const context = paths.frontend.src[type];
  const from = "**/*.*";
  let to;
  if (isProduction) {
    to = `${paths.frontend.dest.folders[type]}/[path][name]`;
  } else {
    to = `${paths.frontend.dest[type]}/[path][name]`;
  }
  if (hashable && isProduction) {
    to += ".[contenthash]";
  }
  to += "[ext]";

  return {
    from,
    to,
    context,
    noErrorOnMissing: true,
  };
}

module.exports = {
  getServerHost,
  getServerPort,
  getOutputName,
  getCopySetting,
};
