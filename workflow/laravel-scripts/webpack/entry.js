/**
 * Create dynamic entries based on folder structure
 *
 * @param {Object} entries
 */
function createDynamicEntries(entries) {
  const path = require("path");
  const glob = require("glob");
  const paths = require("../paths");
  const srcPath = paths.frontend.src.routes;
  const files = glob.sync(paths.join(srcPath, "**/index.{js,ts}"));

  files.forEach((filePath) => {
    const folderName = path.dirname(path.relative(srcPath, filePath));
    const name = `${folderName}`;
    entries[name] = [
      path.resolve(filePath),
      path.resolve(filePath).replace(".js", ".scss").replace(".ts", ".scss"),
    ];
  });

  return entries;
}

module.exports = () => {
  // dynamically build routes entries
  let entries = {};
  entries = createDynamicEntries(entries);

  return entries;
};
