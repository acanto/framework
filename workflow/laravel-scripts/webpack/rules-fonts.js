module.exports = () => {
  const paths = require("../paths");

  return {
    test: /\.(woff2?|ttf|otf|eot)$/,
    type: "asset",
    generator: {
      filename: `${paths.frontend.dest.folders.fonts}/[name].[contenthash][ext]`,
    },
  };
};
