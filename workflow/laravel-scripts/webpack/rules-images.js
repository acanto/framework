module.exports = () => {
  const paths = require("../paths");

  return {
    test: /\.(gif|png|jpe?g|svg)$/i,
    type: "asset",
    generator: {
      filename: `${paths.frontend.dest.folders.images}/[name].[contenthash][ext]`,
    },
  };
};
