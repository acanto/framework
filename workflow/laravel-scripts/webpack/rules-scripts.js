module.exports = () => {
  const { isProduction } = require("../config");

  const plugins = [
    [require.resolve("@babel/plugin-transform-runtime")],
    [
      require.resolve("@babel/plugin-proposal-class-properties"),
      { loose: true },
    ],
    [require.resolve("@babel/plugin-proposal-optional-chaining")],
  ];

  // this is opt-in through env variable, enable it if there are build issues
  // caused by some external library (like ts-particles.js), it will increase
  // bundle size as tree shaking will not operate correctly.
  if (process.env.BUILD_ADD_BABEL_COMMONJS) {
    plugins.push([require.resolve("@babel/plugin-transform-modules-commonjs")]);
  }

  return {
    test: /\.(js|mjs|jsx|ts|tsx)$/,
    // pass also node_modules through babel on production otherwise libraries
    // like `gsap` break on Edge and IE
    // exclude: isProduction ? /\xxxxxxxx/ : /node_modules/,
    exclude: isProduction ? /\xxxxxxxx/ : /node_modules(?!\/(@acanto)).*/,
    // exclude: isProduction ? /\xxxxxxxx/ : /node_modules(?!\/(swiper|gsap)).*/,
    use: {
      loader: require.resolve("babel-loader"),
      options: {
        presets: [
          [
            require.resolve("@babel/preset-env"),
            {
              targets: "defaults",
              useBuiltIns: "entry",
              corejs: 3, // "3.6",
              modules: "auto",
              loose: true,
            },
          ],
          require.resolve("@babel/preset-react"),
          require.resolve("@babel/preset-typescript"),
        ],
        plugins,
        babelrc: false,
        configFile: false,
        cacheDirectory: true,
        cacheCompression: isProduction,
        compact: isProduction,
      },
      // with `swc`  the buld seem actually slower by 10-15%
      // loader: require.resolve("swc-loader"),
      // options: {
      //   jsc: {
      //     parser: {
      //       syntax: "typescript"
      //     }
      //   }
      // }
    },
  };
};
