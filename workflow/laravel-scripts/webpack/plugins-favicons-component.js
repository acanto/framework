const HtmlWebpackPlugin = require("html-webpack-plugin");

class FaviconsComponent {
  apply(compiler) {
    compiler.hooks.compilation.tap("FaviconsComponent", (compilation) => {
      HtmlWebpackPlugin.getHooks(compilation).beforeEmit.tapAsync(
        "FaviconsComponent", // <-- Set a meaningful name here for stacktraces
        (data, cb) => {
          const { getHeaderAutogeneration } = require("../utils");
          const comment = `{{-- ${getHeaderAutogeneration()} --}}\n\n`;
          // Manipulate the content
          data.html = comment + data.html;
          // Tell webpack to move on
          cb(null, data);
        }
      );
    });
  }
}

module.exports = FaviconsComponent;
