const webpack = require("webpack");
const CaseSensitivePathsPlugin = require("case-sensitive-paths-webpack-plugin");

// const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const { isProduction } = require("../config");
const { getProjectJsGlobals } = require("../utils");
const { getOutputName, getCopySetting } = require("./helpers");
const paths = require("../paths");

let plugins = [
  new CaseSensitivePathsPlugin(),
  new webpack.DefinePlugin(
    getProjectJsGlobals().reduce((output, def) => {
      output[def.name] = def.value;
      return output;
    }, {})
  ),
  new CopyPlugin({
    patterns: [
      getCopySetting("fonts", true),
      getCopySetting("images", false),
      getCopySetting("media", false),
    ],
  }),
].concat(require("./plugins-favicons")());

if (isProduction) {
  const MiniCssExtractPlugin = require("mini-css-extract-plugin");
  const PostCSSAssetsPlugin = require("postcss-assets-webpack-plugin");

  plugins = plugins.concat([
    new MiniCssExtractPlugin({
      filename: getOutputName("entry", "css"),
      chunkFilename: getOutputName("chunk", "css"),
      ignoreOrder: true,
    }),
    new PostCSSAssetsPlugin({
      test: /\.css$/,
      log: false,
      plugins: [require("postcss-sort-media-queries")()],
    }),
  ]);
} else {
  plugins.push(new webpack.HotModuleReplacementPlugin());
}

if (isProduction) {
  const { WebpackManifestPlugin } = require("webpack-manifest-plugin");
  const WorkboxPlugin = require("workbox-webpack-plugin");

  plugins = plugins.concat([
    new WebpackManifestPlugin({
      // the public url is prepended in the `assets.blade.php` automated partial
      publicPath: paths.frontend.dest.folders.assets + "/",
      // filter out license files
      filter: ({ name }) => !name.endsWith(".LICENSE.txt"),
    }),
    new webpack.BannerPlugin({
      banner: require("../utils/banner")(),
      raw: true,
      entryOnly: true,
      exclude: /^vendors/,
    }),
    // @see https://developers.google.com/web/tools/workbox/guides/codelabs/webpack
    // @see https://developers.google.com/web/tools/workbox/reference-docs/latest/module-workbox-webpack-plugin.InjectManifest#InjectManifest
    new WorkboxPlugin.GenerateSW({
      swDest: paths.join(paths.frontend.dest.assets, "service-worker.js"),
      clientsClaim: true,
      skipWaiting: true,
      // exclude php files (like the favicons partial which goes through webpack
      // HTML plugin and therefore ends up in the webpack chunks handling)
      exclude: [/\.php$/],
    }),
  ]);
}

if (process.env.DEV_ANALYZE === "true") {
  const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;
  plugins = plugins.concat([
    new BundleAnalyzerPlugin({
      reportFilename: paths.join(
        paths.frontend.dest.folders.assets,
        "stats.json"
      ),
    }),
  ]);
}

module.exports = () => plugins;
