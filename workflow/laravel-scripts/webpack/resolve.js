const path = require("path");
const paths = require("../paths");

/**
 * Should webpack resolve symlinks?
 *
 * Defalt is `true`:
 * @see https://webpack.js.org/configuration/resolve/#resolvesymlinks:
 */
function shouldResolveSymlinks() {
  const { isUsingLocalLinkedNodeModule } = require("../utils");

  if (isUsingLocalLinkedNodeModule(paths.self.nodeModule)) {
    return true;
  }

  const {
    coreLibraryExists,
    getCoreLibraryPath,
  } = require("../utils/coreLibrary");

  if (coreLibraryExists()) {
    if (isUsingLocalLinkedNodeModule(getCoreLibraryPath())) {
      return true;
    }
  }

  return false;
}

/**
 * Get resolve modules
 *
 * @see https://webpack.js.org/configuration/resolve/#resolvemodules
 */
function getResolveModules() {
  const modules = ["node_modules"];
  const { getCoreLibraryNodeModulesPath } = require("../utils/coreLibrary");
  const coreLibraryNodeModules = getCoreLibraryNodeModulesPath();
  if (coreLibraryNodeModules) {
    modules.push(coreLibraryNodeModules);
  }
  return modules;
}

module.exports = {
  modules: getResolveModules(),
  symlinks: shouldResolveSymlinks(),
  extensions: [".ts", ".tsx", ".js", ".jsx"],
  alias: {
    "~": path.resolve(__dirname, paths.frontend.src.base),
    assets: path.resolve(__dirname, paths.frontend.src.assets),
    components: path.resolve(__dirname, paths.frontend.src.components),
    config: path.resolve(__dirname, paths.frontend.src.config),
    layouts: path.resolve(__dirname, paths.frontend.src.layouts),
    routes: path.resolve(__dirname, paths.frontend.src.routes),
    utils: path.resolve(__dirname, paths.frontend.src.utils),
    vendor: path.resolve(__dirname, paths.frontend.src.vendor),
  },
};
