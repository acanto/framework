/**
 * @file
 *
 * Follow these issues:
 * - [attrs option for MiniCssExtractPlugin](https://git.io/JU10P)
 */
module.exports = () => {
  const MiniCssExtractPlugin = require("mini-css-extract-plugin");
  // FIXME: with webpack 5 or new postcss does not work?
  const IconfontWebpackPlugin = require("iconfont-webpack-plugin");
  const {
    getSassAdditionalData,
    getSassIncludePaths,
    getSassSharedResources,
  } = require("../utils/sass");
  const { isProduction, devSourceMaps } = require("../config");
  const sourceMap = devSourceMaps();

  const minicssLoader = () => ({
    loader: MiniCssExtractPlugin.loader,
    options: {
      // style-loader might be avoided and we might use just Mini... but
      // there is an issue with hmr, @see https://git.io/JU1RW
      // hmr: !isProduction,
      // reloadAll: !isProduction,
      // @see https://github.com/webpack-contrib/mini-css-extract-plugin#esmodule
      // @see https://github.com/webpack/webpack/issues/6741
      esModule: isProduction ? true : false,
    },
  });

  // We have an issue with development environment where js execution that
  // depends on CSS effects happens before the CSS applies, hence we might need
  // in parts of our js to put code in a setTimeout during development
  // @see https://stackoverflow.com/a/49228202/1938970
  // @see https://github.com/webpack-contrib/style-loader/issues/121
  // cross ref this internal issue with FIXME: @styleloader
  const styleLoader = () => ({
    // FIXME: using tweaked custom style-loader, see https://git.io/JU1qO
    loader: require.resolve("./style-loader"),
    options: {
      esModule: isProduction ? true : false,
      // FIXME: see https://git.io/JU1qO
      // attributes: { id: '[path][name].[ext]'}
    },
  });

  const cssLoader = () => ({
    loader: require.resolve("css-loader"),
    options: {
      esModule: isProduction ? true : false,
      sourceMap,
      modules: {
        mode: "icss",
      },
      importLoaders: 1, // @see https://git.io/fjEmR
    },
  });

  const postcssLoader = () => ({
    loader: require.resolve("postcss-loader"),
    options: {
      // sourceMap,
      postcssOptions: (loader) => ({
        plugins: [
          // FIXME: with webpack 5 or new postcss does not work?
          new IconfontWebpackPlugin({
            resolve: loader.resolve,
          }),
          require("autoprefixer")(),
        ],
      }),
    },
  });

  const sassLoader = () => ({
    loader: require.resolve("sass-loader"),
    options: {
      sourceMap,
      implementation: require("sass"),
      additionalData: getSassAdditionalData(),
      // webpackImporter: false,
      sassOptions: {
        includePaths: getSassIncludePaths(),
        // fiber: require("fibers"),
        quietDeps: true,
      },
    },
  });

  const sassResourcesLoader = () => ({
    loader: require.resolve("sass-resources-loader"),
    options: {
      resources: getSassSharedResources(),
      hoistUseStatements: true,
    },
  });

  return {
    // sideEffects: true,
    test: /\.(css|sass|scss)$/,
    use: [
      isProduction ? minicssLoader() : styleLoader(),
      cssLoader(),
      postcssLoader(),
      sassLoader(),
      sassResourcesLoader(),
    ],
  };
};
