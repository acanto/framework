module.exports = () => {
  const paths = require("../paths");

  return {
    test: /\.(mp4|wav|mp3|ogg)$/i,
    type: "asset",
    generator: {
      filename: `${paths.frontend.dest.folders.media}/[name].[contenthash][ext]`,
    },
  };
};
