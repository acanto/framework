function getFaviconsOpts() {
  const config = require("../config");
  const { getProjectTitle, getOptions } = require("../utils");

  return getOptions("config.favicons", {
    appName: getProjectTitle(), // Your application's name. `string`
    appDescription: "", // config.pkg.description, // Your application's description. `string`
    version: config.pkg.version, // Your application's version string. `string`
    url: process.env.APP_URL,
    appShortName: null, // Your application's short_name. `string`. Optional. If not set, appName will be used
    developerName: null, // Your (or your developer's) name. `string`
    developerURL: null, // Your (or your developer's) URL. `string`
    display: "standalone", // Preferred display mode: "fullscreen", "standalone", "minimal-ui" or "browser". `string`
    background: "#fff", // Background colour for flattened icons. `string`
    theme_color: "#fff", // Theme color user for example in Android's task switcher. `string`
    // appleStatusBarStyle: "black-translucent", // Style for Apple status bar: "black-translucent", "default", "black". `string`
    dir: "auto", // Primary text direction for name, short_name, and description
    lang: "it", // Primary language for name and short_name
    orientation: "any", // Default orientation: "any", "natural", "portrait" or "landscape". `string`
    scope: "/", // set of URLs that the browser considers within your app
    start_url: "/?homescreen=1", // Start URL when launching the application from a device. `string`
    pixel_art: false, // Keeps pixels "sharp" when scaling up, for pixel art.  Only supported in offline mode.
    loadManifestWithCredentials:
      config.env() === "dev" || config.env() === "staging" ? true : false, // Browsers don't send cookies when fetching a manifest, enable this to fix that. `boolean`
  });
}

module.exports = () => {
  const path = require("path");
  const HtmlWebpackPlugin = require("html-webpack-plugin");
  const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
  const FaviconsComponentPlugin = require("./plugins-favicons-component");
  const config = require("../config");
  const paths = require("../paths");
  const partialFilename = config.filenames.faviconsPartial;
  const HTML_PARTIAL_ID = "favicons-component-html";

  return [
    new HtmlWebpackPlugin({
      inject: false,
      // we use this option to link the FaviconsWebpackPlugin `inject` to this
      // specific HtmlWebpackPlugin instance
      title: HTML_PARTIAL_ID,
      // we filter only the output coming from `favicons-webpack-plugin`, consult
      // the typings https://github.com/jantimon/html-webpack-plugin/blob/main/typings.d.ts#L260
      // to understand the filtering applied here and the example here
      // https://github.com/jantimon/html-webpack-plugin/tree/main/examples/custom-insertion-position
      // templateContent:
      //   `<%= htmlWebpackPlugin.tags.headTags.filter(
      //       (tag) => tag.meta.plugin === "favicons-webpack-plugin"
      //     ).join('')
      //   %>`,
      templateContent: ({ htmlWebpackPlugin }) =>
        `${htmlWebpackPlugin.tags.headTags.filter(
          (tag) => tag.meta.plugin === "favicons-webpack-plugin"
        )}`,
      filename: path.join(paths.frontend.dest.automated, partialFilename),
    }),
    new FaviconsComponentPlugin(),
    new FaviconsWebpackPlugin({
      inject: (htmlPlugin) => htmlPlugin.options.title === HTML_PARTIAL_ID,
      logo: paths.join(paths.frontend.src.favicons, "favicon.png"),
      mode: "webapp",
      devMode: "webapp",
      favicons: getFaviconsOpts(),
      prefix: paths.frontend.dest.folders.favicons + "/",
      // path: getPublicUrls().favicons,
    }),
  ];
};
