const { isHttps } = require("../config");
const { removeTrailingSlashes, getPublicUrls } = require("../utils");
const { getServerHost, getServerPort } = require("./helpers");

module.exports = () => ({
  devMiddleware: {
    publicPath: getPublicUrls().assets,
    // needed for copy-webpack-plugin and images/media files
    // writeToDisk: true,
    writeToDisk: (filePath) => {
      // match everyhting but js/css
      return /^(.(?!\.js|\.css$))+$/.test(filePath);
    },
  },
  client: {
    webSocketURL: {
      port: getServerPort(),
    },
  },
  headers: {
    "Access-Control-Allow-Origin": "*",
  },
  liveReload: false,
  host: getServerHost(),
  port: getServerPort(),
  https: isHttps(),
  allowedHosts: "all",
  proxy: {
    "*": {
      target: removeTrailingSlashes(process.env.APP_URL), // target host
      changeOrigin: true, // needed for virtual hosted sites
    },
  },
  // setupExitSignals: true,
});
