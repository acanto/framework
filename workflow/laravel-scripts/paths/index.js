const path = require("path");
const frontend = require("./frontend");
const laravel = require("./laravel");
const self = require("./self");

// paths of the projects's using this package
const cwd = process.cwd(); // the directory of the project using this package
const root = cwd; // alias the for the current project directory
const nodeModules = path.join(cwd, "node_modules");
const vendor = path.join(cwd, "vendor");
const pkg = path.join(cwd, "package.json"); // the project's package.json path
const env = path.join(root, ".env");

module.exports = {
  // generic
  root,
  nodeModules,
  vendor,
  pkg,
  env,
  // scoped
  frontend,
  laravel,
  self,
  // utilities
  join: path.join, // alias this method to use it without requiring it over and over
};
