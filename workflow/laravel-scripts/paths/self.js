const path = require("path");
const { cliPkg } = require("../config");

const root = path.join(__dirname, "..");

// paths of this package
module.exports = {
  root,
  nodeModule: path.join(process.cwd(), "node_modules", cliPkg.name),
  tmp: path.join(root, ".tmp"),
  scripts: path.join(root, "scripts"),
  templates: path.join(root, "templates"),
};
