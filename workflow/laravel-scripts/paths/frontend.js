const { join } = require("path");
const { pkg } = require("../config");

const __ = pkg.config ? pkg.config.paths || {} : {}; // custom configuration

const srcFolder = __.srcFolder || "src";
const destFolder = __.destFolder || "resources";
const publicFolder = __.publicFolder || "public";
const destUrl = __.destUrl || `/`;
const destFolders = {
  assets: "assets",
  chunks: "chunks",
  entries: "entries",
  images: "images",
  media: "media",
  fonts: "fonts",
  favicons: "favicons",
};

// ensure trailing slashes
const destRelativeUrls = {
  base: `/${destUrl}/`,
  assets: `/${destUrl}/${destFolders.assets}/`,
  chunks: `/${destUrl}/${destFolders.assets}/${destFolders.chunks}/`,
  entries: `/${destUrl}/${destFolders.assets}/${destFolders.entries}/`,
  images: `/${destUrl}/${destFolders.assets}/${destFolders.images}/`,
  media: `/${destUrl}/${destFolders.assets}/${destFolders.media}/`,
  favicons: `/${destUrl}/${destFolders.assets}/${destFolders.favicons}/`,
  fonts: `/${destUrl}/${destFolders.assets}/${destFolders.fonts}/`,
  serviceWorker: `/${destUrl}/${destFolders.assets}/service-worker.js`,
};
const destPublic = join(process.cwd(), publicFolder);
const destPath = join(process.cwd(), destFolder);
const srcPath = join(process.cwd(), srcFolder);

/**
 * All default paths are defined here
 */
module.exports = {
  src: {
    folder: srcFolder,
    base: srcPath,
    assets: join(srcPath, "assets"),
    favicons: join(srcPath, "assets"),
    images: join(srcPath, "assets/images"),
    media: join(srcPath, "assets/media"),
    fonts: join(srcPath, "assets/fonts"),
    svgicons: join(srcPath, "assets/svgicons"),
    utils: join(srcPath, "utils"),
    config: join(srcPath, "config"),
    vendor: join(srcPath, "vendor"),
    // laravel:
    components: join(srcPath, "components"),
    core: join(srcPath, "core"),
    fragments: join(srcPath, "fragments"),
    layouts: join(srcPath, "layouts"),
    middlewares: join(srcPath, "middlewares"),
    routes: join(srcPath, "routes"),
    services: join(srcPath, "services"),
    translations: join(srcPath, "assets/translations.csv"),
  },
  dest: {
    folders: destFolders,
    relativeUrls: destRelativeUrls,
    base: destPath,
    public: destPublic,
    assets: join(destPublic, destFolders.assets),
    favicons: join(destPublic, destFolders.assets, destFolders.favicons),
    images: join(destPublic, destFolders.assets, destFolders.images),
    media: join(destPublic, destFolders.assets, destFolders.media),
    fonts: join(destPublic, destFolders.assets, destFolders.fonts),
    chunks: join(destPublic, destFolders.assets, destFolders.chunks),
    entries: join(destPublic, destFolders.assets, destFolders.entries),
    utils: join(destPath, "components"),
    config: join(process.cwd(), "config"),
    // laravel:
    automated: join(destPath, "components"),
    components: join(destPath, "components"),
    core: join(destPath, "components"),
    fragments: join(destPath, "fragments"),
    layouts: join(destPath, "layouts"),
    middlewares: join(destPath, "middlewares"),
    routes: join(destPath, "routes"),
    services: join(destPath, "services"),
    translations: join(destPath),
  },
};
