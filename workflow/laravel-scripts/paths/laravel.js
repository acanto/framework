const { join } = require("path");
const { pkg } = require("../config");

const __ = pkg.config ? pkg.config.paths || {} : {}; // custom configuration

const root = process.cwd();

const tplPath = join(require("./self").templates, "laravel-frontend");
const libPath = join(root, "vendor", __.libFolder || "acanto/laravel-frontend");
const appPath = root;

const appFolders = {
  bootstrap: __.appBootstrapFolder || "bootstrap",
  config: __.appConfigFolder || "config",
  public: __.appPublicFolder || "public",
  resources: __.appResourcesFolder || "resources",
  routes: __.appRoutesFolder || "routes",
  storage: __.appRoutesFolder || "storage",
};

/**
 * All default paths are defined here
 */
module.exports = {
  tpl: {
    base: tplPath,
    bootstrap: join(tplPath, "bootstrap"),
    config: join(tplPath, "config"),
    public: join(tplPath, "public"),
  },
  lib: {
    base: libPath,
    bootstrap: join(libPath, "bootstrap"),
    config: join(libPath, "config"),
    public: join(libPath, "public"),
    routes: join(libPath, "routes"),
    src: join(libPath, "src"),
  },
  app: {
    folders: appFolders,
    base: appPath,
    bootstrap: join(appPath, appFolders.bootstrap),
    config: join(appPath, appFolders.config),
    public: join(appPath, appFolders.public),
    resources: join(appPath, appFolders.resources),
    routes: join(appPath, appFolders.routes),
    storage: join(appPath, appFolders.storage),
  },
};
