function getFileClass(filepath) {
  const fs = require("fs");
  // const path = require('path');
  const engine = require("php-parser");
  const parser = new engine({
    parser: {
      php7: true,
    },
  });
  const phpFile = fs.readFileSync(filepath);

  try {
    const program = parser.parseCode(phpFile);

    for (let i = 0; i < program.children.length; i++) {
      const child = program.children[i];
      if (child.kind === "namespace") {
        for (let j = 0; j < child.children.length; j++) {
          const subChild = child.children[j];
          if (subChild.kind === "class") {
            return subChild;
          }
        }
      } else if (child.kind === "class") {
        return child;
      }
    }
  } catch (e) {}

  return;
}

/**
 * Get PHP class property value
 *
 * @param {string} propertyName
 * @param {string | object} source Either an already parse PHP class or a filepath to a php file
 * @returns {?string}
 */
function getClassPropertyValue(propertyName, source) {
  const phpClass = typeof source === "string" ? getFileClass(source) : source;

  if (phpClass) {
    for (let i = 0; i < phpClass.body.length; i++) {
      const element = phpClass.body[i];
      if (element.kind === "propertystatement") {
        for (let j = 0; j < element.properties.length; j++) {
          const property = element.properties[j];
          if (property.name.name === propertyName) {
            return property.value.value;
          }
        }
      }
    }
  }

  return;
}

/**
 * Converts a file path to a php namespace syntax
 *
 * @param {string} [prefix='']
 * @param {string} [filePath='']
 * @returns {string}
 */
function filePathToNamespace(prefix = "", filePath = "") {
  const fullPath = prefix + "/" + filePath.replace(".php", "");

  return `\\${fullPath.replace(/\//g, "\\")}`;
}

module.exports = {
  getFileClass,
  getClassPropertyValue,
  filePathToNamespace,
};
