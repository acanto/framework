/**
 * Get SASS shared resources
 *
 * Paths of sass file that might be made always available with
 * `sass-resource-loader`
 *
 * The order of the array is crucial here to enable SASS overriding defaults
 * behaviour.
 */
function getSassSharedResources() {
  const fs = require("fs");
  const path = require("path");
  const paths = require("../paths");
  const { coreLibrary, componentsLibrary } = require("../config");
  const importLibrary = (lib, suffix, filename) =>
    paths.join(
      paths.root,
      "/node_modules/",
      `${lib === "core" ? coreLibrary : componentsLibrary}-${suffix}/`,
      `_${filename}.scss`
    );
  const importProject = (filename) =>
    path.join(paths.frontend.src.config, `/${filename}.scss`);

  const possiblePaths = [
    // functions:
    importLibrary("core", "scss", "functions"),
    importProject("functions"),
    // variables:
    importProject("variables"),
    importLibrary("core", "scss", "variables"),
    importLibrary("core", "typography", "variables"),
    importLibrary("core", "grid", "variables"),
    importLibrary("core", "buttons", "variables"),
    importLibrary("core", "forms", "variables"),
    importLibrary("core", "media", "variables"),
    importLibrary("core", "progress", "variables"),
    importLibrary("core", "glide", "variables"),
    importLibrary("components", "header", "variables"),
    importLibrary("components", "hamburger", "variables"),
    importLibrary("core", "dialog", "variables"),
    // mixins:
    importLibrary("core", "scss", "mixins"),
    importLibrary("core", "scss/vendor", "rfs"),
    importLibrary("core", "scss/vendor", "sass-rem"),
    importLibrary("core", "grid", "mixins"),
    importLibrary("core", "responsive", "mixins"),
    importLibrary("core", "typography", "mixins"),
    importLibrary("core", "forms", "mixins"),
    importLibrary("core", "fillform", "mixins"),
    importLibrary("core", "media", "mixins"),
    importLibrary("core", "buttons", "mixins"),
    importLibrary("core", "dialog", "mixins"),
    importLibrary("core", "scss/mixins", "fonts"),
    importLibrary("core", "scss/mixins", "overlay"),
    importLibrary("core", "scss/mixins", "ratio"),
    importLibrary("core", "scss/mixins", "spacing"),
    importLibrary("core", "scss/mixins", "svg"),
    importLibrary("core", "glide", "mixins"),
    importLibrary("core", "video", "mixins"),
    importLibrary("components", "header", "mixins"),
    importLibrary("components", "hamburger", "mixins"),
    importProject("mixins"),
    // placeholders:
    importLibrary("core", "scss", "placeholders"),
    importProject("placeholders"),
  ];

  return possiblePaths.filter((sassFilePath) => {
    return fs.existsSync(path.resolve(sassFilePath));
  });
}

/**
 * Get SASS include paths
 *
 * Adds the core library node_modules folder if the core library exists, this
 * is needed when using the core library installed through `npm link` which
 * does not install its node_modules in the project root (in a flat deps
 * structure) but keeps them under the node_modules inside the symlinked
 * library's folder (e.g. in `./node_modules/@acanto/core-.../node_modules)
 *
 * @returns {string[]}
 */
function getSassIncludePaths() {
  const path = require("path");
  const paths = require("../paths");
  const { getCoreLibraryNodeModulesPath } = require("./coreLibrary");
  const includePaths = [path.resolve(path.join(paths.root, "node_modules/"))];
  const coreLibraryNodeModules = getCoreLibraryNodeModulesPath("-scss");

  // FIXME: this is not working, but not sure if still needed
  if (coreLibraryNodeModules) {
    includePaths.push(path.resolve(coreLibraryNodeModules));
  }

  return includePaths;
}

/**
 * Get SASS additional data based on env variables and paths/urls
 *
 * @returns {string}
 */
function getSassAdditionalData() {
  const path = require("path");
  const { frontend } = require("../paths");
  const { getPublicUrls } = require("./index");
  const publicUrls = getPublicUrls();
  const output = [
    // FIXME: hacky solution found at
    // https://github.com/nuxt-community/style-resources-module/issues/143#issuecomment-847413359
    `@use "sass:math";`,
    `$DEV: ${process.env.NODE_ENV !== "production"};`,
    `$ENV: "${process.env.APP_ENV}";`,
    `$SRC_ASSETS: "${path.relative(frontend.src.utils, frontend.src.assets)}";`,
    `$URL_ASSETS: "${publicUrls.assets}";`,
    `$SRC_FONTS: "${path.relative(frontend.src.utils, frontend.src.fonts)}";`, // "../assets/fonts";`,
    `$URL_FONTS: "${publicUrls.fonts}";`,
    `$SRC_IMAGES: "${path.relative(frontend.src.utils, frontend.src.images)}";`,
    `$URL_IMAGES: "${publicUrls.images}";`,
    `$SRC_SVGICONS: "${frontend.src.svgicons}";`,
  ];

  return output.join(" ");
}

module.exports = {
  getSassSharedResources,
  getSassIncludePaths,
  getSassAdditionalData,
};
