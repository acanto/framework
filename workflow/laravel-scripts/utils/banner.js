module.exports = () => {
  const { pkg } = require("../config");
  const { getProjectTitle, getNow } = require("./index");
  const thisYear = new Date().getFullYear();
  const years = thisYear > pkg.config.startYear ? "-" + thisYear : "";

  return (
    `` +
    `/*! ` +
    `Copyright (c) ${pkg.config.startYear}${years} ${pkg.author} | ` +
    `${getProjectTitle()} v${pkg.version} (${
      process.env.APP_URL
    }, last change on: ${getNow()}) ` +
    `*/`
  );
};
