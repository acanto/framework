/**
 * @see https://github.com/funkedigital/gulp-touch-fd/
 */
module.exports = function (options) {
  const fs = require("fs");
  const map = require("map-stream");
  return map(function (file, cb) {
    if (file.isNull()) {
      return cb(null, file);
    }

    // Update file modification and access time
    return fs.utimes(file.path, new Date(), new Date(), () => {
      cb(null, file);
    });
  });
};
