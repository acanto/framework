function enrichName(givenName) {
  const {
    paramCase,
    sentenceCase,
    pascalCase,
    camelCase,
  } = require("change-case");

  return {
    slug: paramCase(givenName),
    name: sentenceCase(givenName),
    className: pascalCase(givenName),
    instanceName: camelCase(givenName),
  };
}

module.exports = {
  enrichName,
};
