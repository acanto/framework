// @ts-check

/**
 * Create htaccess file if not existing on user's machine
 *
 * @param {"" | "example" | "dev" | "staging" | "production"} name
 */
function maybeCreateFile(name, callback) {
  const filename = name ? `.htaccess.${name}` : ".htaccess";
  const { taskIfDevAndMissingFile } = require("../utils");
  const paths = require("../paths");

  return taskIfDevAndMissingFile(
    paths.join(paths.frontend.dest.public, filename),
    callback,
    () => {
      const gulp = require("gulp");

      return gulp
        .src(paths.join(paths.self.templates, filename), { dot: true })
        .pipe(gulp.dest(paths.frontend.dest.public));
    }
  );
}

/**
 * Create current .htaccess file
 */
function htaccessCurrent(callback) {
  return maybeCreateFile("", callback);
}

/**
 * Create htaccess "dev"
 */
function htaccessDev(callback) {
  return maybeCreateFile("dev", callback);
}

/**
 * Create htaccess "staging"
 */
function htaccessStaging(callback) {
  return maybeCreateFile("staging", callback);
}

/**
 * Create htaccess "production"
 */
function htaccessProduction(callback) {
  return maybeCreateFile("production", callback);
}

/**
 * Htaccess reusable function for hashed assets
 *
 * @param {string} dest
 * @param {string[]} extensions
 * @param {number} hours
 * @returns
 */
function _htaccessHashed(dest, extensions, hours) {
  const gulp = require("gulp");
  const rename = require("gulp-rename");
  const template = require("gulp-template");
  const paths = require("../paths");

  return gulp
    .src(paths.join(paths.self.templates, "htaccess-assets-hashed"))
    .pipe(
      // @ts-ignore
      template({
        extensions: extensions.join("|"),
        days: hours * 60 * 60,
      })
    )
    .pipe(rename(".htaccess"))
    .pipe(gulp.dest(dest));
}

function htaccessEntries() {
  const paths = require("../paths");
  return _htaccessHashed(
    paths.frontend.dest.entries,
    ["js", "css", "txt"],
    24 * 365
  );
}

function htaccessChunks() {
  const paths = require("../paths");
  return _htaccessHashed(
    paths.frontend.dest.chunks,
    ["js", "css", "txt"],
    24 * 365
  );
}

function htaccessImages() {
  const paths = require("../paths");
  return _htaccessHashed(
    paths.frontend.dest.images,
    ["gif", "png", "jpg", "jpeg", "svg", "webp"],
    24 * 365
  );
}

function htaccessFonts() {
  const paths = require("../paths");
  return _htaccessHashed(
    paths.frontend.dest.fonts,
    ["woff", "woff2", "ttf", "otf", "eot"],
    24 * 365
  );
}

function htaccessFavicons() {
  const paths = require("../paths");
  return _htaccessHashed(
    paths.frontend.dest.favicons,
    ["png", "json", "xml", "webapp", "ico"],
    4
  );
}

function getHtaccessAllowedIps(filecontent) {
  const regex = /Allow from(.+)/gim;
  const ips = [];
  let matches;

  while ((matches = regex.exec(filecontent)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (matches.index === regex.lastIndex) {
      regex.lastIndex++;
    }
    if (matches[1]) {
      ips.push(matches[1]);
    }
  }
  return ips.map((value) => value.trim());
}

/**
 * Add the current runner IP to the htaccess allow section in dev/staging
 * protected environments
 *
 * Order allow,deny
 * Allow from 185.31.102.112
 * Allow from 83.91.216.141
 */
function htaccessModify(callback) {
  const { isCI } = require("../config");
  const { getInternalIps } = require("../utils");
  const allowedIps = getInternalIps();

  if (!isCI() || !allowedIps.length) {
    return callback();
  }
  const fs = require("fs");
  const { join, frontend } = require("../paths");
  const { env } = require("../config");
  const src = join(frontend.dest.public, ".htaccess");
  let content = fs.readFileSync(src, { encoding: "utf-8" });
  const existingIps = getHtaccessAllowedIps(content);
  const ipsToAdd = existingIps.length
    ? allowedIps.filter((ip) => !existingIps.includes(ip))
    : allowedIps;

  // if we already have all the ips whitelisted do nothing
  if (!ipsToAdd.length) {
    return callback();
  }

  // if we do not we add the rule
  let toInsert = `Order allow,deny${ipsToAdd
    .map((ip) => `\nAllow from ${ip}`)
    .join("")}\n`;

  // if there is already the rule tweak it
  if (/Order allow,deny/gim.test(content)) {
    content = content.replace(/Order allow,deny[.|\s|\n|\r]/gim, toInsert);
    // otherwise let's be sure there is an Auth basic protection in the htaccess
    // and in case add the lines at the end of the block
  } else if (/^AuthType Basic/gim.test(content)) {
    content = content.replace(
      /(AuthType Basic)((.|\s|\n|\r)*?)(^|[^\n])\n{2}(?!\n)/,
      `$1$2$4\n${toInsert}`
    );
  } else {
    // otherwise we just add the lines at the begininng of the htaccess file,
    // it is a bit unsafe perhaps... on production we will never have such
    // problem, in theory, htaccess will never block access
    if (env() !== "production") {
      const lines = content.split("\n");
      lines.splice(2, 0, toInsert);
      content = lines.join("\n");
    }
  }

  // add satisfy if missing
  if (!/Satisfy any/gim.test(content)) {
    const matches = content.match(/Allow.+$/gm);

    const lastMatch =
      matches && matches.length ? matches[matches.length - 1] : null;
    if (lastMatch) {
      content = content.replace(lastMatch, `${lastMatch}\nSatisfy any\n`);
    }
  }

  fs.writeFileSync(src, content);

  callback();
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
module.exports = function htaccess(callback) {
  const gulp = require("gulp");
  const { isProduction } = require("../config");

  return isProduction
    ? gulp.parallel(
        htaccessEntries,
        htaccessChunks,
        htaccessImages,
        htaccessFonts,
        htaccessFavicons,
        htaccessModify
      )(callback)
    : gulp.parallel(
        // htaccessModify, // to debug...
        htaccessCurrent,
        htaccessDev,
        htaccessStaging,
        htaccessProduction
      )(callback);
};
