// @ts-check

/**
 * Manage laravel public folder
 */
function tplLaravelPublic() {
  const gulp = require("gulp");
  const { join, laravel } = require("../paths");

  return gulp
    .src(join(laravel.tpl.public, "**/*.php"), { base: laravel.tpl.public })
    .pipe(gulp.dest(laravel.app.public));
}

/**
 * Manage laravel bootstrap folder
 */
function tplLaravelBootstrap() {
  const gulp = require("gulp");
  const { join, laravel } = require("../paths");

  return gulp
    .src(join(laravel.tpl.bootstrap, "*.php"), { base: laravel.tpl.bootstrap })
    .pipe(gulp.dest(laravel.app.bootstrap));
}

/**
 * Manage laravel bootstrap cache folder
 */
function tplLaravelBootstrapCache() {
  const gulp = require("gulp");
  const rename = require("gulp-rename");
  const { join, laravel, self } = require("../paths");

  return gulp
    .src(join(self.templates, "gitignore-folder"))
    .pipe(rename(".gitignore"))
    .pipe(gulp.dest(join(laravel.app.bootstrap, "cache")));
}

/**
 * Manage laravel storage cache folder
 *
 * @see https://github.com/laravel/laravel
 */
function tplLaravelStorage() {
  const gulp = require("gulp");
  const rename = require("gulp-rename");
  const { join, laravel, self } = require("../paths");

  return gulp
    .src(join(self.templates, "gitignore-folder"))
    .pipe(rename(".gitignore"))
    .pipe(gulp.dest(join(laravel.app.storage, "app")))
    .pipe(gulp.dest(join(laravel.app.storage, "app/public")))
    .pipe(gulp.dest(join(laravel.app.storage, "debugbar")))
    .pipe(gulp.dest(join(laravel.app.storage, "framework")))
    .pipe(gulp.dest(join(laravel.app.storage, "framework/cache")))
    .pipe(gulp.dest(join(laravel.app.storage, "framework/sessions")))
    .pipe(gulp.dest(join(laravel.app.storage, "framework/testing")))
    .pipe(gulp.dest(join(laravel.app.storage, "framework/views")))
    .pipe(gulp.dest(join(laravel.app.storage, "logs")));
}

/**
 * Manage laravel artisan file
 */
function tplLaravelArtisan() {
  const gulp = require("gulp");
  const { join, laravel } = require("../paths");

  return gulp
    .src(join(laravel.tpl.base, "artisan"))
    .pipe(gulp.dest(laravel.app.base));
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
module.exports = function tplLaravel(callback) {
  const gulp = require("gulp");

  return gulp.series(
    tplLaravelPublic,
    tplLaravelBootstrap,
    tplLaravelBootstrapCache,
    tplLaravelStorage,
    tplLaravelArtisan
  )(callback);
};
