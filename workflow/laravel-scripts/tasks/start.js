// @ts-check

const gulp = require("gulp");
const { assets } = require("./assets");
const check = require("./check");
const { clean } = require("./clean");
const configure = require("./configure");
const env = require("./env");
const { i18nTranslations } = require("./i18n");
const htaccess = require("./htaccess");
const gitignore = require("./gitignore");
const pkg = require("./pkg");
const readme = require("./readme");
const setup = require("./setup");
const svgicons = require("./svgicons");
const watch = require("./watch");
const { tplBuild, tplWatch } = require("./tpl");

function start(callback) {
  return gulp.series(
    clean,
    setup,
    check,
    env,
    gulp.parallel(
      configure,
      readme,
      pkg,
      gitignore,
      htaccess,
      tplBuild,
      i18nTranslations,
      svgicons
    ),
    gulp.parallel(tplWatch, assets, watch)
  )(callback);
}

module.exports = start;
