// @ts-check

/**
 * Styles and scripts are instead watched by webpack in the `assets.js` task
 */
module.exports = function watch(callback) {
  const svgicons = require("./svgicons");
  const { i18n } = require("./i18n");
  const { clear } = require("./clean");
  const paths = require("../paths");
  const { watcher } = require("../utils");
  const { join, frontend } = paths;
  const { src } = frontend;

  const watchers = [
    watcher(join(paths.env), true)
      .on("change", () => clear(() => true))
      .on("unlink", () =>
        console.log(`You must have an '.env' file in your root project folder`)
      ),
    watcher(join(src.svgicons, "*.svg")).on("change", () => svgicons()),
    watcher(src.translations).on("change", () => i18n()),
  ];

  const handleExit = async () => {
    // console.log("handle exit");
    await Promise.all(watchers.map((watcher) => watcher.close()));
    // console.log(`closed ${watchers.length} watchers.`);

    callback();
  };
  // process.on("exit", () => { console.log("on exit"); callback(); });
  process.on("SIGINT", handleExit);
  process.on("SIGTERM", handleExit);
  process.on("SIGQUIT", handleExit);
};
