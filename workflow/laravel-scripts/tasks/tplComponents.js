// @ts-check

/**
 * Manage components' templates files
 */
module.exports = function tplComponents() {
  const gulp = require("gulp");
  const rename = require("gulp-rename");
  const { pascalCase } = require("change-case");
  const { join, frontend } = require("../paths");
  const { src, dest } = frontend;

  return gulp
    .src(join(src.components, "**/*.php"), { base: src.components })
    .pipe(
      rename((path) => {
        const isBlade = path.basename.endsWith(".blade");
        const isIndex = path.basename.startsWith("index");
        const filename = path.basename.replace(/\.blade$/, "");

        if (isIndex) {
          path.basename = pascalCase(path.dirname);
        } else {
          path.basename = pascalCase(path.dirname + "-" + filename);
        }
        if (isBlade) {
          path.basename += ".blade";
        }

        path.dirname = "";
      })
    )
    .pipe(gulp.dest(dest.components));
};
