// @ts-check

/**
 * Create dummy files that are usually required to run the website locally,
 * wihout overwriting existing ones.
 */
function checkAutomatedPartials() {
  const gulp = require("gulp");
  const rename = require("gulp-rename");
  const header = require("gulp-header");
  const paths = require("../paths");
  const checker = require("@acanto/workflow-utils/checker");
  const { getHeaderAutogeneration } = require("../utils");
  const { filenames } = require("../config");
  const { dest } = paths.frontend;
  const opts = { overwrite: false };

  checker.filesExist(
    [
      { name: filenames.faviconsPartial, dest: dest.automated },
      { name: filenames.assetsHeadPartial, dest: dest.automated },
      { name: filenames.assetsBodyPartial, dest: dest.automated },
      { name: filenames.svgIconsPartial, dest: dest.automated },
    ],
    {
      willAutogenerate: true,
    }
  );

  // with `overwrite: false` we can safely create the files even if they already
  // exist, in that case we have just logged a warn anyway
  return gulp
    .src(paths.join(paths.self.templates, "dummy.blade.php"))
    .pipe(header(`{{-- ${getHeaderAutogeneration()} --}}\n\n`))
    .pipe(rename(filenames.faviconsPartial))
    .pipe(gulp.dest(dest.automated, opts))
    .pipe(rename(filenames.assetsHeadPartial))
    .pipe(gulp.dest(dest.automated, opts))
    .pipe(rename(filenames.assetsBodyPartial))
    .pipe(gulp.dest(dest.automated, opts))
    .pipe(rename(filenames.svgIconsPartial))
    .pipe(gulp.dest(dest.automated, opts));
}

/**
 * Create the required `./config/laravel-frontend.php` file if missing
 */
function checkConfig(callback) {
  const fs = require("@acanto/workflow-utils/fs");
  // const { createFileIfMissing } = require("../utils")
  const paths = require("../paths");

  [
    {
      fallback: paths.laravel.tpl.config,
      dest: paths.frontend.dest.config,
      // }, {
      //   fallback: paths.laravel.tpl.config,
      //   dest: paths.frontend.dest.config
    },
  ].forEach(({ fallback, dest }) => {
    // createFileIfMissing(dest, fallback)
    fs.copySync(fallback, dest, { overwrite: false });
  });

  callback();
}

/**
 * Check routes consistency
 */
async function checkRoutes(callback) {
  const {
    shouldCheckRoutes,
    getCmsRoutes,
    checkRoutesConsistency,
  } = require("../utils/route");

  if (shouldCheckRoutes()) {
    const data = await getCmsRoutes();
    checkRoutesConsistency(data);
  }

  callback();
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
module.exports = function check(callback) {
  const gulp = require("gulp");

  return gulp.series(
    checkAutomatedPartials,
    checkConfig,
    checkRoutes
  )(callback);
};
