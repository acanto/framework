// @ts-check

/**
 * Manage utils' templates files
 */
module.exports = function tplUtils() {
  const gulp = require("gulp");
  const rename = require("gulp-rename");
  const { join, frontend } = require("../paths");
  const { src, dest } = frontend;

  return gulp
    .src(join(src.utils, "**/*.blade.php"), { base: src.utils })
    .pipe(
      rename(function (path) {
        // console.log("path.dirname", path.dirname)
        // flatten index file names by using the folder name
        if (path.basename.startsWith("index")) {
          path.basename = path.dirname;
          path.dirname = "";
        }
      })
    )
    .pipe(gulp.dest(dest.utils));
};
