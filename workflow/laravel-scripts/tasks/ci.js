// @ts-check

/**
 * @typedef {object} deployParams
 * @property {"dev" | "staging" | "production"} env - The environment to deploy (should match the branch associated to the deploy script in the ci.yml file)
 * @property {"ftp" | "ssh"} [mode="ftp"] - The deploy mode, either via ftp or ssh
 * @property {string} folder - The folder on the server where to deploy, starting from root
 * @property {string} host - When in `--mode ftp` is the ftp host (as url e.g. `dev.mycompany.net` or IP address). When in `--mode ssh` is the server address (e.g. `ubuntu@myproject.mycompany.net`)
 * @property {string} username - Ftp username
 * @property {string} password - Ftp password or `sshpass -p {password}`
 * @property {string} sshkeyvar - SSH key variable name, it must be a CI variable name like `MY_CI_VAR` set on git from https://gitlab.com/myproject/-/settings/ci_cd
 * @property {string} port - Port to connect (used in SSH deploys sometimes)
 * @property {"http"} [auth] - This is needed when deploying on an http protected domain from an external runner, e.g. from gitlab.com
 */

/**
 * Call deploy endpoint
 *
 * Use http to avoid issues on the runner such as this error encountered when
 * requesting `https` URLs:
 * `Failed to connect to myproject.acanto.net port 443: Connection refused`
 * We also make sure to add http authentication for `acanto.net` protected
 * endpoints
 *
 * @param {string} path
 * @param {string} action
 * @param {boolean} async
 */
function callDeployEndpoint(path, action = "", async = false) {
  const log = require("@acanto/workflow-utils/log");
  const { normaliseUrl } = require("../utils");
  const url = normaliseUrl(process.env.APP_URL + "/" + path);

  // alternative with curl:
  const { execSync } = require("child_process");
  log(`Calling ${action} script at url: ${url}`);
  if (async) {
    execSync(`curl ${url}`);
    log(`Done calling ${action} script`);

    // exec(`curl ${url}`);
    // // we do not want to wait for the async `exec` to complete here, but still
    // // we want to wait a bit to be sure that the command is spawned.
    // setTimeout(
    //   () => log(`Script ${action} will run in the background...`),
    //   200
    // );
  } else {
    execSync(`curl ${url}`);
    log(`Done calling ${action} script`);
  }
}

/**
 * Check that all deployment options are correctly set, in some occations we can
 * even guess a fallback value, in which case we log it, in others we just throw
 * a descriptive error and exit.
 *
 * @returns {deployParams}
 */
function checkOptionsDefintions() {
  const log = require("@acanto/workflow-utils/log");
  const chalk = require("@acanto/workflow-utils/chalk");
  const { getCliOptionsMap } = require("../utils/cli");

  let { env, mode, folder, port, host, username, password, sshkeyvar, auth } =
    getCliOptionsMap();

  if (!env) {
    env = guessEnvironment();

    if (env) {
      log.info(
        chalk.yellowBright.bold(
          `Missing '--env' option, assuming value '${env}' based on current branch.`
        )
      );
    } else {
      throw Error("Missing '--env myenv' option. Exiting...");
    }
  } else {
    // TODO: check that the env defined matches an `.env.{envName}` file?
  }

  if (!mode) {
    // throw Error("Missing '--mode ftp|ssh' option. Exiting...");
    mode = "ftp";

    log.info(
      chalk.yellowBright.bold(
        `Missing '--mode' option, assuming default value '${mode}'.`
      )
    );
  }

  if (mode === "ftp") {
    if (!host || !username || !password) {
      throw Error(
        "Missing one option between '--host', '--username' and '--password'. Exiting..."
      );
    }
  } else if (mode === "ssh") {
    if (!host || (!sshkeyvar && !password)) {
      throw Error(
        "Missing one option between '--host', '--sshkeyvar' and '--password'. Exiting..."
      );
    }
  }

  if (!folder) {
    folder = process.env.APP_NAME;

    log.info(
      chalk.yellowBright.bold(
        `Missing '--folder' option, guessed fallback is '${folder}' based on APP_NAME.`
      )
    );
  }

  return { env, mode, folder, port, host, username, password, sshkeyvar, auth };
}

/**
 * Guess deploy environment based on standard branching model
 *
 * @return {deployParams["env"] | false}
 */
function guessEnvironment() {
  const branch = process.env.CI_COMMIT_BRANCH;
  if (!branch) {
    return false;
  }

  const branchesMap = {
    main: "dev",
    master: "dev",
    stage: "staging",
    staging: "staging",
    prod: "production",
    production: "production",
  };

  return branchesMap[branch] || false;
}

/**
 * Check that we actually are in a CI environment, exit otherwise
 */
function ciCheck(callback) {
  const ci = require("ci-info");
  const log = require("@acanto/workflow-utils/log");

  if (ci.isCI) {
    log.info(`Running on CI server ${ci.name}`);
    checkOptionsDefintions();
    callback();
  } else {
    throw Error("This script is not running on a CI server. Exiting...");
    // process.exit();
  }
}

/**
 * Ci env and htaccess manager
 */
function ciEnvHtaccess(callback) {
  const fs = require("fs");
  const paths = require("../paths");
  const options = checkOptionsDefintions();
  const pathEnv = paths.join(paths.root, `.env.${options.env}`);
  const pathHtaccess = paths.join(
    paths.frontend.dest.public,
    `.htaccess.${options.env}`
  );

  // use the right .env.{env} file and copy it as .env
  if (fs.existsSync(pathEnv)) {
    fs.copyFileSync(pathEnv, paths.join(paths.root, ".env"));
  }

  // use the right .htaccess.{env} file and copy it as .htaccess
  if (fs.existsSync(pathHtaccess)) {
    fs.copyFileSync(
      pathHtaccess,
      paths.join(paths.frontend.dest.public, ".htaccess")
    );
  }

  // reload current env to be sure...
  require("dotenv").config();

  callback();
}

/**
 * Run composer to install vendor files
 */
function ciComposer(callback) {
  const { execSync } = require("child_process");

  execSync(
    "composer install --no-interaction --no-progress --no-dev --prefer-dist --quiet"
  );

  // execSync("composer dump-autoload"); // done by laravel already

  callback();
}

/**
 * Zip the vendor folder
 */
function ciComposerZip(callback) {
  // require modules
  const fs = require("fs");
  const archiver = require("archiver");
  const log = require("@acanto/workflow-utils/log");
  const del = require("del");
  const paths = require("../paths");

  // create a file to stream archive data to.
  const output = fs.createWriteStream(paths.join(paths.root, "vendor.zip"));
  const archive = archiver("zip", { zlib: { level: 9 } });

  output.on("close", function () {
    // now delete the vendor folder so that it does not get synced to the server
    del(paths.join(paths.root, "vendor"));

    // succesfull message
    log("Created vendor.zip and deleted vendor folder");
    callback();
  });

  output.on("end", function () {
    log("vendor.zip: data has been drained");
  });

  archive.on("warning", function (err) {
    if (err.code === "ENOENT") {
    } else {
      throw err;
    }
  });

  archive.on("error", function (err) {
    throw err;
  });

  archive.directory(paths.join(paths.root, "vendor/"), false);

  archive.pipe(output);
  archive.finalize();
}

/**
 * Clean files that should not end up on the server
 */
function ciClean() {
  const del = require("del");
  const paths = require("../paths");

  return del(
    [
      paths.join(paths.root, ".env.*"),
      paths.join(paths.frontend.dest.public, ".htaccess.*"),
      paths.join(paths.root, "package.json"),
      paths.join(paths.root, "package-lock.json"),
      paths.join(paths.root, "composer.lock"),
      paths.join(paths.root, "tsconfig.json"),
      paths.join(paths.root, "*.md"),
      paths.join(paths.root, ".vscode"),
      paths.join(paths.root, ".husky"),
      paths.join(paths.frontend.src.base),
    ],
    { force: true, dot: true }
  );
}

/**
 * Touch files in order to change their timestamps and force lftp to update them
 * despite having the same byte length.
 */
function ciTouch() {
  const paths = require("../paths");
  const gulp = require("gulp");
  const touch = require("../utils/gulp-touch");

  return (
    gulp
      .src(paths.join(paths.frontend.dest.base, "/**/*"), {
        base: paths.frontend.dest.base,
        dot: true, // htaccess too
      })
      .pipe(gulp.dest(paths.frontend.dest.base))
      // @ts-ignore
      .pipe(touch())
  );
}

/**
 * Sync files, either via ftp or ssh
 *
 * all commands are synchronous here, just run them one by one
 */
function ciSync(callback) {
  const { mode } = checkOptionsDefintions();

  if (mode === "ftp") {
    ciSyncFtp(callback);
  } else if (mode === "ssh") {
    ciSyncSsh(callback);
  } else {
    throw new Error(
      `deploy script supports 'ftp' and 'ssh'. Mode '${mode}' is not recognised`
    );
  }
}

/**
 * Sync files via ftp
 *
 * all commands are synchronous here, just run them one by one
 */
function ciSyncFtp(callback) {
  const { execSync } = require("child_process");
  const log = require("@acanto/workflow-utils/log");
  const { removeTrailingSlashes } = require("../utils");
  let { username, password, host, folder } = checkOptionsDefintions();
  const cmdPrefx = `lftp -c "set ftp:ssl-allow no; open -u`;
  const cmdDefaults = `mirror --reverse --parallel=50`;
  const cmdCommon = `${cmdPrefx} ${username},${password} ${host}; ${cmdDefaults}`;
  folder = removeTrailingSlashes(folder);

  // selective incremental sync (without delete, for long cached files)
  log("Syncing storage folder...");
  execSync(`${cmdCommon} ./storage ${folder}/storage" || true`);

  // selective excluding sync (with delete)
  log("Syncing public folder...");
  execSync(`${cmdCommon} --delete ./public ${folder}/public" || true`);
  log("Syncing resources folder...");
  execSync(`${cmdCommon} --delete ./resources ${folder}/resources" || true`);
  log("Syncing all the rest...");
  execSync(
    `${cmdCommon} --delete --exclude-glob=.git* --exclude=^.git/ --exclude=^.npm/ --exclude=^node_modules/ --exclude=^vendor/ --exclude=^public/ --exclude=^resources/ --exclude=^storage/ ./ ${folder}/" || true`
  );

  callback();
}

/**
 * Sync files via ssh
 *
 * all commands are synchronous here, just run them one by one
 */
function ciSyncSsh(callback) {
  const { execSync } = require("child_process");
  const log = require("@acanto/workflow-utils/log");
  const { removeTrailingSlashes } = require("../utils");
  let { sshkeyvar, port, host, folder, password } = checkOptionsDefintions();
  let cmdPrefx = `rsync --recursive --verbose`;
  let address = "";

  // ensure host ends with column without doubling it
  host = host.trim().replace(/:+$/, "") + ":";
  // turn the folder into the full server address
  address = host + removeTrailingSlashes(folder);
  // listen to port if necessary
  if (port) {
    address = `-e 'ssh -p ${port}' ${address}`;
  }

  log("Configuring ssh access...");

  // standard SSH auth via SSH key
  if (sshkeyvar) {
    execSync(`mkdir -p ~/.ssh`);
    execSync(`cat "$${sshkeyvar}" > ~/.ssh/id_dsa`);
    execSync(`chmod 600 ~/.ssh/id_dsa`);
    execSync(`echo "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config`);
  }
  // less standard authentication via `sshpass`
  else if (password) {
    cmdPrefx = `sshpass -p "${password}" ${cmdPrefx}`;
  }

  // selective incremental sync (without delete, for long cached files)
  log("Syncing storage folder...");
  execSync(
    `${cmdPrefx} --update --exclude 'framework/cache/data' ./storage ${address}/`
  );

  // selective excluding sync (with delete)
  log("Syncing public folder...");
  execSync(
    `${cmdPrefx} --delete-after --exclude 'page-cache' ./public ${address}/`
  );
  log("Syncing resources folder...");
  execSync(`${cmdPrefx} --delete-after ./resources ${address}/`);
  log("Syncing all the rest...");
  execSync(
    `${cmdPrefx} --delete-after --exclude '.git*' --exclude '.npm' --exclude 'node_modules' --exclude 'vendor' --exclude 'public' --exclude 'resources' --exclude 'storage' ./ ${address}/`
  );

  if (sshkeyvar) {
    execSync(`rm ~/.ssh/id_dsa`);
  }

  callback();
}

/**
 * Copy deployer script in public folder so that it will be uploaded
 */
function ciCopyScripts() {
  const gulp = require("gulp");
  const paths = require("../paths");

  return gulp
    .src([paths.join(paths.self.scripts, "deploy/deployer.php")])
    .pipe(gulp.dest(paths.frontend.dest.public));
}

/**
 * Call deployer script temporarily put on server
 */
function ciDeployer(callback) {
  callDeployEndpoint("deployer.php", "deployer");
  callback();
}

/**
 * Call laravel-frontend exposed hook
 */
function ciHooks(callback) {
  callDeployEndpoint("_/hooks/deploy/end/", "deploy end hook", true);
  callback();
}

/**
 * Visit all website url to re-create caches
 */
function ciVisit(callback) {
  const log = require("@acanto/workflow-utils/log");
  const mode = process.env.CI_VISIT_MODE;

  if (mode && mode.trim() === "false") {
    log("CI visit will not run.");
    callback();
  } else if (mode && mode.trim() === "node") {
    log("CI visit will run as a node task.");
    const visit = require("../scripts/visit");
    visit(callback);
  } else {
    log("CI visit will run as a php task.");
    callback();
  }
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
function ciPre(callback) {
  const gulp = require("gulp");

  return gulp.series(ciCheck, ciEnvHtaccess)(callback);
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
function ciPost(callback) {
  const gulp = require("gulp");

  return gulp.series(
    ciComposer,
    ciComposerZip,
    ciCopyScripts,
    ciClean,
    ciTouch,
    ciSync,
    ciDeployer,
    ciHooks,
    ciVisit
  )(callback);
}

exports.ciPre = ciPre;
exports.ciPost = ciPost;
