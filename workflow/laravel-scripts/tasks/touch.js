// @ts-check

/**
 * Touch files in order to change their timestamps and force lftp to update them
 * despite having the same byte length. Esclude node_modules, git, source, images
 * and fonts' files from the process.
 */
module.exports = function touch() {
  const paths = require("../paths");
  const gulp = require("gulp");
  const touch = require("../utils/gulp-touch");

  return (
    gulp
      .src(
        [
          paths.join(paths.frontend.dest.base, "/**/*"),
          `!${paths.join(paths.frontend.src.base, "/**/*")}`,
          `!${paths.join(paths.frontend.dest.images, "/**/*")}`,
          `!${paths.join(paths.frontend.dest.fonts, "/**/*")}`,
        ],
        { base: paths.frontend.dest.base }
      )
      .pipe(gulp.dest(paths.frontend.dest.base))
      // @ts-ignore
      .pipe(touch())
  );
};
