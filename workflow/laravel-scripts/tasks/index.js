// @ts-check

// we need to load the env immediately
require("dotenv").config();

const build = require("./build");
const { clear, wipe } = require("./clean");
const core = require("./core");
const deploy = require("./deploy");
const generateComponent = require("./generate-component");
const generateRoute = require("./generate-route");
const { i18n } = require("./i18n");
const init = require("./init");
const start = require("./start");
const svgicons = require("./svgicons");
const test = require("./test");

exports.build = build;
exports.clear = clear;
exports.core = core;
exports.deploy = deploy;
exports.generateComponent = generateComponent;
exports.generateRoute = generateRoute;
exports.i18n = i18n;
exports.init = init;
exports.start = start;
exports.svgicons = svgicons;
exports.test = test;
exports.wipe = wipe;
