// @ts-check

const gulp = require("gulp");
const { assets } = require("./assets");
const check = require("./check");
const { clean } = require("./clean");
const env = require("./env");
const { i18nTranslations } = require("./i18n");
const htaccess = require("./htaccess");
const gitignore = require("./gitignore");
const pkg = require("./pkg");
const setup = require("./setup");
const svgicons = require("./svgicons");
const { tplBuild } = require("./tpl");

function build(callback) {
  return gulp.series(
    clean,
    setup,
    check,
    env,
    gulp.parallel(
      pkg,
      gitignore,
      htaccess,
      tplBuild,
      i18nTranslations,
      svgicons
    ),
    assets
  )(callback);
}

module.exports = build;
