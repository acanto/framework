// @ts-check

global.__IPS = {
  local: "",
  cms: "",
  fillform: "",
  auth: "",
  all: new Set([]),
};

/**
 * Set server port
 */
function setupServerPort(callback) {
  const { isProduction } = require("../config");
  const getPort = require("get-port");
  const log = require("@acanto/workflow-utils/log");
  const chalk = require("@acanto/workflow-utils/chalk");

  if (isProduction) {
    callback();
    return;
  }

  return new Promise((resolve, reject) => {
    getPort({ port: [8080, 3000, 3002, 3004, 3006, 3008] })
      .then((port) => {
        global.__FREEPORT = port;

        log.info(
          chalk.yellow("Local server will use free port " + chalk.bold(port))
        );
        resolve();
      })
      .catch(reject);
  });
}

/**
 * Setup the local machine IP, only useful inside the CI deploy
 */
function setupMachineIP(callback) {
  const { isCI } = require("../config");
  if (!isCI()) {
    return callback();
  }

  const publicIp = require("public-ip");
  const log = require("@acanto/workflow-utils/log");
  const chalk = require("@acanto/workflow-utils/chalk");

  return new Promise((resolve, reject) => {
    publicIp
      .v4()
      .then((ip) => {
        global.__IPS["local"] = ip;
        global.__IPS["all"].add(ip);

        log.info(chalk.yellow("Local machine public IP " + chalk.bold(ip)));
        resolve();
      })
      .catch(() => {
        log.info(chalk.yellow("Could not determine local machine public IP."));
        reject();
      });
  });
}

/**
 * Setup the external userd APIS IPs, only useful inside the CI deploy
 */
function setupApisIPs(callback) {
  const { isCI } = require("../config");
  if (!isCI()) {
    return callback();
  }

  const log = require("@acanto/workflow-utils/log");
  const chalk = require("@acanto/workflow-utils/chalk");
  const { URL } = require("url");
  const { Resolver } = require("dns").promises;
  const resolver = new Resolver();

  const apisPromises = [
    { type: "cms", url: process.env.CMS_API_URL },
    // { type: "auth", url: process.env.AUTH_API_URL },
    { type: "fillform", url: process.env.FILLFORM_API_URL },
  ]
    .filter((data) => !!data.url)
    .map((data) => {
      const url = new URL(data.url);
      return new Promise((resolve, reject) => {
        resolver
          .resolve4(url.hostname)
          .then(
            (ips) => {
              global.__IPS[data.type] = ips.join(",");
              ips.forEach((ip) => global.__IPS["all"].add(ip));

              log.info(
                chalk.yellow(
                  `API ${data.type} public IP ${chalk.bold(ips.join(","))}`
                )
              );

              resolve();
            },
            () => {
              reject();
            }
          )
          .catch(reject);
      });
    });

  return Promise.all(apisPromises);
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
module.exports = function setup(callback) {
  const gulp = require("gulp");
  return gulp.parallel(setupServerPort, setupMachineIP, setupApisIPs)(callback);
};
