// @ts-check

/**
 * Clean the Laravel caches
 */
function clear(callback) {
  const { exec } = require("child_process");
  const log = require("@acanto/workflow-utils/log");

  exec(
    "php artisan config:cache && php artisan route:clear && php artisan view:clear && php artisan cacher:clear data && php artisan page-cache:clear",
    (error, stdout, stderr) => {
      if (stdout) {
        stdout.split("\n").forEach((line) => {
          log(`[clear] ${line}`);
        });
      }
      callback();
    }
  );
}

/**
 * Clean storage and public
 */
function cleanStorageAndPublic(callback) {
  const del = require("del");
  const { root, frontend, join } = require("../paths");

  return del([
    join(root, "bootstrap/cache/**/*"),
    // join(root, "storage/**/*"),
    join(root, "storage/app/*"),
    join(root, "storage/app/public/**/*"),
    join(root, "storage/debugbar/**/*"),
    join(root, "storage/framework/cache/**/*"),
    // join(root, "storage/framework/sessions/**/*"),
    join(root, "storage/framework/testing/**/*"),
    join(root, "storage/framework/views/**/*"),
    join(root, "storage/logs/**/*"),
    join(frontend.dest.assets),
    join(frontend.dest.public, "page-cache"),
  ]);
}

/**
 * Clean all generated files
 */
function clean(callback) {
  const gulp = require("gulp");
  const { tplClean } = require("./tpl");
  const { assetsClean } = require("./assets");

  return gulp.parallel(tplClean, assetsClean, cleanStorageAndPublic)(callback);
}

/**
 * Wipe out everything both `clean` and `clear`
 */
function wipe(callback) {
  const gulp = require("gulp");
  return gulp.series(clear, clean)(callback);
}

module.exports = {
  clean,
  clear,
  wipe,
};
