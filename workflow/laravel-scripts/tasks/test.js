// @ts-check
const gulp = require("gulp");
const build = require("./build");

function test(callback) {
  return gulp.series(build, (cb) => {
    const log = require("@acanto/workflow-utils/log");
    const chalk = require("@acanto/workflow-utils/chalk");
    log(
      chalk.greenBright(`Production code is built, you can view it at ${process.env.APP_URL}.
        You might first temporarily set in your ".env" file APP_ENV=production.
      `)
    );
    cb();
  })(callback);
}

module.exports = test;
