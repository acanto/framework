/**
 * Inspects the src/middlewares folder and determine if there are any custom
 * middlewares to automaticlaly register
 */
function getMiddlewaresToRegister() {
  const path = require("path");
  const glob = require("glob");
  const { join, frontend } = require("../paths");
  const { src } = frontend;
  const {
    getFileClass,
    getClassPropertyValue,
    filePathToNamespace,
  } = require("../utils/php");
  const files = glob.sync(join(src.middlewares, "**/*.php"));

  const middlewares = {
    global: [],
    web: [],
    named: [],
  };

  files.forEach((filePath) => {
    const phpClass = getFileClass(filePath);

    if (phpClass) {
      const relativePath = path.relative(src.middlewares, filePath);
      const name = getClassPropertyValue("name", phpClass);
      const classPath =
        filePathToNamespace("resources/middlewares", relativePath) + "::class";
      const middleware = { name, classPath };

      if (!name || name === "global") {
        middlewares.global.push(middleware);
      } else if (name === "web") {
        middlewares.web.push(middleware);
      } else {
        middlewares.named.push(middleware);
      }
    }
  });

  return middlewares;
}

/**
 * Manage middlewares files (might not be there)
 */
function tplMiddlewaresCopy() {
  const gulp = require("gulp");
  const { join, frontend } = require("../paths");
  const { src, dest } = frontend;

  return gulp
    .src(join(src.middlewares, "**/*.php"), { base: src.middlewares })
    .pipe(gulp.dest(dest.middlewares));
}

function tplMiddlewaresRegister() {
  const gulp = require("gulp");
  const footer = require("gulp-footer");
  const template = require("gulp-template");
  const paths = require("../paths");
  const config = require("../config");
  const middlewares = getMiddlewaresToRegister();
  const { getHeaderAutogeneration } = require("../utils");

  return gulp
    .src(paths.join(paths.self.templates, config.filenames.middlewares))
    .pipe(
      // @ts-ignore
      template(middlewares)
    )
    .pipe(footer(`\n// ${getHeaderAutogeneration()}\n`))
    .pipe(gulp.dest(paths.frontend.dest.middlewares));
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
module.exports = function tplMiddlewares(callback) {
  const gulp = require("gulp");

  return gulp.series(tplMiddlewaresCopy, tplMiddlewaresRegister)(callback);
};
