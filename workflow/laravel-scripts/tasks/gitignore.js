// @ts-check

/**
 * Create gitignore base file
 */
function gitignoreCreate(callback) {
  const paths = require("../paths");
  const { taskIfDevAndMissingFile } = require("../utils");

  return taskIfDevAndMissingFile(
    paths.join(paths.root, ".gitignore"),
    callback,
    () => {
      const gulp = require("gulp");
      const rename = require("gulp-rename");

      return gulp
        .src(paths.join(paths.self.templates, "gitignore"))
        .pipe(rename(".gitignore"))
        .pipe(gulp.dest(paths.root));
    }
  );
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
module.exports = function gitignore(callback) {
  const gulp = require("gulp");
  return gulp.parallel(gitignoreCreate)(callback);
};
