// @ts-check

const gulp = require("gulp");
const { clean } = require("./clean");
const configure = require("./configure");
const env = require("./env");
const { i18nTranslations } = require("./i18n");
const htaccess = require("./htaccess");
const gitignore = require("./gitignore");
const pkg = require("./pkg");
const readme = require("./readme");
const { tplBuild } = require("./tpl");

function init(callback) {
  const ci = require("ci-info");
  if (ci.isCI) {
    callback();
  } else {
    return gulp.series(
      clean,
      env,
      gulp.parallel(
        configure,
        readme,
        pkg,
        gitignore,
        htaccess,
        tplBuild,
        i18nTranslations
      )
    )(callback);
  }
}

module.exports = init;
