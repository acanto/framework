// @ts-check

/**
 * Generate component
 */
module.exports = function generateComponent(callback) {
  const log = require("@acanto/workflow-utils/log");
  const chalk = require("@acanto/workflow-utils/chalk");
  const paths = require("../paths");
  const { enrichName } = require("../utils/enricher");
  const { generate } = require("../utils/generator");
  const { getCliOption } = require("../utils/cli");

  const names = getCliOption().split(",");

  log(
    chalk.greenBright(
      names.length > 1
        ? `The following components have been generated:`
        : `The following component has been generated:`
    )
  );

  for (let i = 0; i < names.length; i++) {
    const data = { component: enrichName(names[i]) };
    log(chalk.green("• " + data.component.className));

    generate({
      name: data.component.className,
      data,
      srcFolder: "component",
      destFolder: paths.frontend.src.components,
    });
  }

  callback();
};
