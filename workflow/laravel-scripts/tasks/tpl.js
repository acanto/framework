// @ts-check

/**
 * Clean compiled template files location
 */
function tplClean() {
  const del = require("del");
  const paths = require("../paths");
  return del(paths.join(paths.frontend.dest.base, "**/*"));
}

/**
 * Build only template files ( blade templates and php)
 */
function tplBuild(callback) {
  const gulp = require("gulp");
  const core = require("./core");
  const tplComponents = require("./tplComponents");
  const tplFragments = require("./tplFragments");
  const tplLaravel = require("./tplLaravel");
  const tplLayouts = require("./tplLayouts");
  const tplMiddlewares = require("./tplMiddlewares");
  const tplRoutes = require("./tplRoutes");
  const tplServices = require("./tplServices");
  const tplUtils = require("./tplUtils");

  return gulp.parallel([
    core,
    tplComponents,
    tplFragments,
    tplLaravel,
    tplLayouts,
    tplMiddlewares,
    tplRoutes,
    tplServices,
    tplUtils,
  ])(callback);
}

/**
 * Watch only template files ( blade templates and php)
 */
function tplWatch(callback) {
  // const gulp = require("gulp");
  const paths = require("../paths");
  const { watcher } = require("../utils");
  const tplComponents = require("./tplComponents");
  const tplFragments = require("./tplFragments");
  // const tplLaravel = require("./tplLaravel");
  const tplLayouts = require("./tplLayouts");
  const tplMiddlewares = require("./tplMiddlewares");
  const tplRoutes = require("./tplRoutes");
  const tplServices = require("./tplServices");
  const tplUtils = require("./tplUtils");
  const { join, frontend } = paths;
  const { src } = frontend;

  const watchers = [
    watcher(join(src.components, "/**/*.php")).on("all", () => tplComponents()),
    watcher(join(src.fragments, "/**/*.php")).on("all", () => tplFragments()),
    watcher(join(src.layouts, "/**/*.php")).on("all", () => tplLayouts()),
    watcher(join(src.middlewares, "/**/*.php")).on("all", () =>
      tplMiddlewares()
    ),
    watcher(join(src.routes, "/**/*.{php,json}")).on("all", () => tplRoutes()),
    watcher(join(src.services, "/**/*.php")).on("all", () => tplServices()),
    watcher(join(src.utils, "/**/*.php")).on("all", () => tplUtils()),
  ];

  const handleExit = async () => {
    // console.log("handle exit");
    await Promise.all(watchers.map((watcher) => watcher.close()));
    // console.log(`closed ${watchers.length} watchers.`);

    callback();
  };
  // process.on("exit", () => { console.log("on exit"); callback(); });
  process.on("SIGINT", handleExit);
  process.on("SIGTERM", handleExit);
  process.on("SIGQUIT", handleExit);
}

/**
 * Exposse build and watch task separetely
 */
module.exports = {
  tplClean,
  tplBuild,
  tplWatch,
};
