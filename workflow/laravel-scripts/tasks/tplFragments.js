// @ts-check

/**
 * Manage fragments' templates files
 */
module.exports = function tplFragments() {
  const gulp = require("gulp");
  const rename = require("gulp-rename");
  const { join, frontend } = require("../paths");
  const { src, dest } = frontend;

  return gulp
    .src(join(src.fragments, "**/*.php"), { base: src.fragments })
    .pipe(
      rename(function (path) {
        // console.log("path.dirname", path.dirname)
        // flatten index file names by using the folder name
        if (path.basename.startsWith("index")) {
          path.basename = path.dirname;
          path.dirname = "";
        }
      })
    )
    .pipe(gulp.dest(dest.fragments));
};
