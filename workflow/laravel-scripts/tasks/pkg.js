// @ts-check

/**
 * Set `"scripts"` and `"config.startYear"` defaults on `package.json` if
 * missing
 */
function pkgSetup(pkg) {
  const existingScripts = pkg.get("scripts");
  const existingStartYear = pkg.get("config.startYear");
  const startYear = existingStartYear || new Date().getFullYear();

  pkg.set("scripts", {
    start: "npx acanto start",
    build: "npx acanto build",
    wipe: "npx acanto wipe",
    clean: "npx acanto clean",
    clear: "npx acanto clear",
    route: "npx acanto route",
    component: "npx acanto component",
    help: "npx acanto help",
    visit: "npx acanto visit",
    test: "npx acanto test",
    link: "npx acanto link",
    unlink: "npx acanto unlink",
    "composer:refresh": "rm composer.lock && rm -r ./vendor/ && composer i",
    postinstall: "npx acanto init && npx acanto core",
    // preinstall: "npx acanto preinstall",
    ...existingScripts,
  });
  pkg.set("config.startYear", startYear);
  pkg.save();
}

/**
 * Set browserlist configuration on `package.json`
 */
function pkgBrowserslistConfig(pkg, config) {
  const existingBrowserslist = pkg.get("browserslist");
  const browserslist = isCustomised(existingBrowserslist, config.browserList)
    ? existingBrowserslist
    : config.browserList;

  pkg.set("browserslist", browserslist);
  pkg.save();
}

/**
 * Set prettier configuration on `package.json`
 */
function pkgPrettierConfig(pkg, config) {
  const existingLintStaged = pkg.get("lint-staged");
  const existingPrettier = pkg.get("prettier");
  const prettier = isCustomised(existingPrettier, config.prettier)
    ? existingPrettier
    : config.prettier;
  const lintStaged = isCustomised(existingLintStaged, config.lintStaged)
    ? existingLintStaged
    : config.lintStaged;

  pkg.unset("overrides"); // remove the former prettier overrides key
  pkg.set("prettier", prettier);
  pkg.set("lint-staged", lintStaged);
  pkg.save();
}

/**
 * Copy husky v6 configuration folder
 */
function huskyConfig() {
  const fs = require("@acanto/workflow-utils/fs");
  const paths = require("../paths");
  const src = paths.join(paths.self.templates, "/.husky");
  const dest = paths.join(paths.root, "/.husky");

  fs.ensureDirSync(dest);

  return fs.copy(src, dest, {
    preserveTimestamps: true,
    overwrite: false,
  });
}

/**
 * Check if the existing configuration is different than the default,
 * so that we don't override it if it has been customised.
 */
function isCustomised(existing, _default) {
  if (!existing) {
    return false;
  }
  if (JSON.stringify(existing) === JSON.stringify(_default)) {
    return false;
  }
  return true;
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
module.exports = function pkg(callback) {
  const editJsonFile = require("edit-json-file");
  const paths = require("../paths");
  const pkg = editJsonFile(paths.pkg, {
    stringify_eol: true,
  });
  const config = require("../config");

  pkgSetup(pkg);
  pkgBrowserslistConfig(pkg, config);
  pkgPrettierConfig(pkg, config);
  huskyConfig().finally(callback);
};
