/**
 * Manage services files (might not be there)
 */
function tplServicesCopy() {
  const gulp = require("gulp");
  const { join, frontend } = require("../paths");
  const { src, dest } = frontend;

  return gulp
    .src(join(src.services, "**/*.php"), { base: src.services })
    .pipe(gulp.dest(dest.services));
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
module.exports = function tplServices(callback) {
  const gulp = require("gulp");

  return gulp.series(tplServicesCopy)(callback);
};
