// @ts-check

function getEnvsData() {
  const fs = require("fs");
  const path = require("path");
  const { root } = require("../paths");
  const { getDataForEnv } = require("../utils");
  const output = [];

  const envNames = fs
    .readdirSync(root)
    .filter((fn) => fn.startsWith(".env"))
    .map((envPath) => {
      let envName = path.basename(envPath).replace(".env", "");
      envName = envName.replace(/^\./, ""); // remove first dot if any
      return envName;
    })
    .filter((env) => env && env !== "example");

  envNames.forEach((name) => {
    output.push(getDataForEnv(name));
  });

  return output.filter((env) => env && env.APP_URL);
}

/**
 * Create readme file
 */
module.exports = function readme(callback) {
  const paths = require("../paths");
  const { getProjectTitle, getProjectRepoInfo } = require("../utils");
  const { paramCase, sentenceCase } = require("change-case");
  const gulp = require("gulp");
  const template = require("gulp-template");
  const rename = require("gulp-rename");
  const title = getProjectTitle();
  const repo = getProjectRepoInfo();
  const envs = getEnvsData();

  return gulp
    .src(paths.join(paths.self.templates, "README.md"))
    .pipe(
      template({
        slug: paramCase(title),
        title: sentenceCase(title),
        repo,
        envs,
      })
    )
    .pipe(rename("README.md"))
    .pipe(gulp.dest(paths.root));
};
