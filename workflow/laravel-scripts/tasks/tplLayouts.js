// @ts-check

/**
 * Manage layouts' templates files
 */
module.exports = function tplLayouts() {
  const gulp = require("gulp");
  const rename = require("gulp-rename");
  const { join, frontend } = require("../paths");
  const { src, dest } = frontend;

  return gulp
    .src(join(src.layouts, "**/*.php"), { base: src.layouts })
    .pipe(
      rename((path) => {
        // flatten index file names by using the folder name
        if (path.basename.startsWith("index")) {
          path.basename = path.dirname + ".blade";
          path.dirname = "";
        }
      })
    )
    .pipe(gulp.dest(dest.layouts));
};
