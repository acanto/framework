// @ts-check

/**
 * Generate route
 */
module.exports = async function generateRoute(callback) {
  const log = require("@acanto/workflow-utils/log");
  const chalk = require("@acanto/workflow-utils/chalk");
  const { getCliOption } = require("../utils/cli");
  const {
    shouldCheckRoutes,
    getCmsRoutes,
    checkRoutesConsistency,
    generateRouteSource,
  } = require("../utils/route");

  const names = getCliOption().split(",");

  log(
    chalk.greenBright(
      names.length > 1
        ? `The following routes have been generated:`
        : `The following route has been generated:`
    )
  );

  for (let i = 0; i < names.length; i++) {
    log(chalk.green("• " + names[i]));
    generateRouteSource(names[i]);
  }

  if (shouldCheckRoutes()) {
    log(chalk.greenBright("Now let's check that CMS pages are in sync..."));

    const remoteData = await getCmsRoutes();

    checkRoutesConsistency(remoteData);
  }

  callback();
};
