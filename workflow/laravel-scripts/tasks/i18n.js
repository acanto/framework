// @ts-check

/**
 * Simply copy over translations source files
 *
 * TODO: convert here in JSON? so that php would not need to parse the csv
 */
function i18nTranslations() {
  const path = require("path");
  const gulp = require("gulp");
  const paths = require("../paths");
  const { src, dest } = paths.frontend;

  return gulp
    .src(path.join(src.translations))
    .pipe(gulp.dest(dest.translations));
}

/**
 * We need to clear laravel view cache each time a csv with translated strings
 * is changed
 */
function i18nClear(callback) {
  const { execSync } = require("child_process");
  execSync("php artisan view:clear");
  callback();
}

/**
 * Collect subtasks in a single task that respect the gulp async lifecycle by
 * calling the callback on completion.
 */
function i18n(callback) {
  const gulp = require("gulp");
  const isProduction = require("../config");
  return isProduction
    ? gulp.series(i18nTranslations)(callback)
    : gulp.series(i18nTranslations, i18nClear)(callback);
}

module.exports = {
  i18nTranslations,
  i18nClear,
  i18n,
};
