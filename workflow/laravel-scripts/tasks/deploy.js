// @ts-check

const gulp = require("gulp");
const build = require("./build");
const { ciPre, ciPost } = require("./ci");

function deploy(callback) {
  return gulp.series(ciPre, build, ciPost)(callback);
}

module.exports = deploy;
