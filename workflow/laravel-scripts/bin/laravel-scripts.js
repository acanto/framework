#!/usr/bin/env node
"use strict";

const log = require("@acanto/workflow-utils/log");
const chalk = require("@acanto/workflow-utils/chalk");

// TODO: immediate check and prompt:
// const ci = require("ci-info");
// try {
//   require("dotenv").config();
//   log.yellow(chalk.redBright.bold("Missing '.env' file. One will be created."));
// } catch(e) {
//   if (!ci.isCI) {
//   }
// }

const spawn = require("@acanto/workflow-utils/crossSpawn");
const { cliPkg } = require("../config");
const { parseCliOptions } = require("../utils/cli");
const { isUsingLocalLinkedNodeModule } = require("../utils");
const {
  coreLibraryExists,
  getCoreLibraryPath,
  getCoreLibraryPkg,
} = require("../utils/coreLibrary");

// log info about the current cli pkg
const cliPath = require("../paths/self").nodeModule;
const cliOrigin = isUsingLocalLinkedNodeModule(cliPath) ? "local" : "remote";
log.info(
  chalk.yellow.bold(
    `Running ${chalk.white(cliOrigin)} ${cliPkg.name} v${cliPkg.version}`
  )
);

// log info about the current coreLibrary pkg
if (coreLibraryExists()) {
  const coreLibPkg = getCoreLibraryPkg();
  const coreLibPath = getCoreLibraryPath();
  const coreLibOrigin = isUsingLocalLinkedNodeModule(coreLibPath)
    ? "local"
    : "remote";
  log.info(
    chalk.yellow.bold(
      `Using ${chalk.white(coreLibOrigin)} ${coreLibPkg.name} v${
        coreLibPkg.version
      }`
    )
  );
}

// gulp required node arguments
const gulpfile = [
  "--gulpfile",
  "./node_modules/@acanto/laravel-scripts/tasks/index.js",
  "--cwd",
  ".",
];

// create env object
const env = Object.create(process.env);

const args = process.argv.slice(2);
// get the script options
const options = parseCliOptions();

// get the script name
let script = args[0];
// log(`Calling script ${script}`);

let nodeArgs;

switch (script) {
  case "preinstall":
    require("../scripts/preinstall")();
    break;
  case "help":
    require("../scripts/help")();
    break;
  case "prettify":
    require("../scripts/prettier")();
    break;
  case "visit":
    require("../scripts/visit")();
    break;
  case "extract-route":
  case "ex-r":
    require("../scripts/extract-classes/route");
    break;
  case "extract-component":
  case "ex-c":
    require("../scripts/extract-classes/component");
    break;
  case "link":
    require("../scripts/link");
    break;
  case "unlink":
    require("../scripts/unlink");
    break;
  case "use":
    require("../scripts/use").run();
    break;
  case "init":
  case "start":
  case "clean":
  case "clear":
  case "wipe":
  case "favicons":
  case "svgicons":
    env.NODE_ENV = "development";
    // test available gulp tasks with:
    // nodeArgs = [].concat(gulpfile).concat(["--tasks"]);
    nodeArgs = [script].concat(gulpfile);
    break;
  case "core":
  case "test":
  case "build":
  case "deploy":
    env.NODE_ENV = "production";
    nodeArgs = [script].concat(gulpfile).concat(options);
    break;
  case "component":
  case "route":
  case "components":
  case "routes":
    script = script.charAt(0).toUpperCase() + script.slice(1); // capitalise
    script = `generate${script.replace(/s$/, "")}`; // remove last char to support plural form
    nodeArgs = [script].concat(gulpfile).concat(options);
    break;
  default:
    log.error(chalk.redBright('Unknown script "' + script + '".'));
    log.info(chalk.bold("Perhaps you need to update @acanto/laravel-scripts?"));
    process.exit(1);
}

if (nodeArgs) {
  spawn("gulp", nodeArgs, {
    stdio: "inherit",
    env: env,
  });
}
