const path = require("path");
const execSync = require("child_process").execSync;
const fs = require("./fs");

function isInGitRepository() {
  try {
    execSync("git rev-parse --is-inside-work-tree", { stdio: "ignore" });
    return true;
  } catch (e) {
    return false;
  }
}

function isInMercurialRepository() {
  try {
    execSync("hg --cwd . root", { stdio: "ignore" });
    return true;
  } catch (e) {
    return false;
  }
}

function inGitRepo() {
  return isInGitRepository() || isInMercurialRepository();
}

function tryGitInit(cwd) {
  const options = { stdio: "ignore" };
  if (cwd) options.cwd = cwd;

  try {
    execSync("git --version", options);
    if (inGitRepo()) {
      return false;
    }

    execSync("git init", options);
    return true;
  } catch (e) {
    console.warn("Git repo not initialized", e);
    return false;
  }
}

function tryGitCommit(appPath, msg) {
  try {
    execSync("git add -A", { stdio: "ignore" });
    execSync(`git commit -m "${msg}"`, {
      stdio: "ignore",
    });
    return true;
  } catch (e) {
    // We couldn't commit in already initialized git repo,
    // maybe the commit author config is not set.
    // In the future, we might supply our own committer
    // like Ember CLI does, but for now, let's just
    // remove the Git files to avoid a half-done state.
    console.warn("Git commit not created", e);
    console.warn("Removing .git directory...");
    try {
      // unlinkSync() doesn't work on directories.
      fs.removeSync(path.join(appPath, ".git"));
    } catch (removeErr) {
      // Ignore.
    }
    return false;
  }
}

function getLatestNpmPkgVersion(pkgName) {
  const result = execSync(`npm view ${pkgName} --json`).toString();
  let data;

  try {
    data = JSON.parse(result);
  } catch (e) {
    throw new Error(`Unable to find latest version of ${pkgName}`);
  }

  return data["dist-tags"]["latest"];
}

module.exports = {
  inGitRepo,
  tryGitInit,
  tryGitCommit,
  getLatestNpmPkgVersion,
};
