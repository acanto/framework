<?php

namespace LaravelFrontend\Forms;

use Illuminate\Support\ServiceProvider;
use LaravelFrontend\Forms\Fillform;

class FillformServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('fillform', function ($app) {
            return new Fillform($app);
        });
    }
}
