<?php

namespace LaravelFrontend\Forms;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use LaravelFrontend\Cacher\CacherTags;
use LaravelFrontend\Helpers\Helpers;
use LaravelFrontend\Forms\Form;

class Fillform
{
    // protected $token;
    // protected $id;
    // protected $url;

    // /**
    //  * Create a new Fillform instance
    //  *
    //  * For now we just use this class as a static interface, we might turn it
    //  * into this later if we need it
    //  *
    //  * @param string|array $idOrIdsMap
    //  * @return LaravelFrontend\Forms\Fillform
    //  */
    // public function __construct($idOrIdsMap)
    // {
    //     $meta = self::getFormApiMeta($idOrIdsMap);
    //     $this->token = $meta['token'];
    //     $this->id = $meta['id'];
    //     $this->url = $meta['url'];
    //     return $this;
    // }

    /**
     * Get meta information about the desired Fillform's form by id/locale->id
     *
     * @param string|array $idOrIdsMap
     * @param bool $quickReturn
     * @return void
     */
    public static function getFormApiMeta(
        $idOrIdsMap,
        bool $quickReturn = false
    ) {
        $apiUrl = config('env.FILLFORM_API_URL');
        $token = config('env.FILLFORM_TOKEN');

        if (!$token || !$apiUrl) {
            if ($quickReturn) {
                return false;
            }
            exit(
                '[Fillform] You  must define a `FILLFORM_TOKEN` and a `FILLFORM_API_URL` in the .env file'
            );
        }

        if (!$idOrIdsMap) {
            if ($quickReturn) {
                return false;
            }
            exit(
                '[Fillform] You must pass a form id as a string or an array of locale->id forms'
            );
        }

        if (is_array($idOrIdsMap)) {
            // if it is an array get the id for the current locale
            $id = $idOrIdsMap[App::getLocale()] ?? '';

            if (!$id) {
                exit(
                    '[Fillform] You must supply a valid form id for the current locale'
                );
            }
        } else {
            // if a single id just use that
            $id = $idOrIdsMap;
        }

        // construct fillform form's urls
        $url = "$apiUrl$token/form/single/$id";
        $action = "$apiUrl$token/sendMailV2/$id";

        return [
            'token' => $token,
            'apiUrl' => $apiUrl,
            'id' => $id,
            'url' => $url,
            'action' => $action,
        ];
    }

    /**
     * Get fillform form data
     *
     * @param string|array| $idOrIdsMap An array like [ 'en' => '195', 'it' => '196' ] or just the id
     * @return void
     */
    public static function get($idOrIdsMap)
    {
        $meta = self::getFormApiMeta($idOrIdsMap);

        // retrieve form data from fillform API
        $remoteData = self::getData($meta['url']) ?? [];
        // merge formData with basic meta data
        $formData = array_merge($remoteData, [
            'fillform' => $meta,
            'action' => $meta['action'], // TODO: crate an internal endpoint
        ]);

        // process with our abstract form implementation
        $form = new Form($meta['id'], $formData);

        return $form->json();
    }

    /**
     * Prefill form data with given array
     *
     * @param array $form
     * @param array $values
     * @return array
     */
    public static function prefill(array $form = [], array $values = [])
    {
        $i = 0;
        foreach ($form['fields'] as $field) {
            $key = $field['name'];

            if (isset($values[$key])) {
                $form['fields'][$i]['value'] = $values[$key];
            }
            $i++;
        }

        return $form;
    }

    /**
     * Check that the given form id exists on fillform, we strictly check for
     * `false` as that is what is return by `file_get_contents` on failure,
     *
     * @param string|array $idOrIdsMap
     * @return boolean
     */
    public static function isFromFillform($idOrIdsMap)
    {
        $meta = self::getFormApiMeta($idOrIdsMap, true);
        if (!$meta) {
            return false;
        }
        $data = self::getData($meta['url']);

        return $data !== false;
    }

    /**
     * Get data remotely or from cache
     *
     * We check for `empty` as fillforms does not seem to return a 404
     * or an error status but an empty array, so we mimick what `file_get_contents`
     * would return in case of an actual failed request, it would return `false`
     *
     * FIXME: fillform side
     *
     * @param string $url
     * @return void
     */
    protected static function getData(string $url)
    {
        $cacheKey = Helpers::getCacheKey("fillform.$url", true);

        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        } else {
            $data = json_decode(file_get_contents($url), true);
            if (empty($data)) {
                $data = false;
            }
            $data = self::cleanResponse($data);
            Cache::tags([CacherTags::data, CacherTags::forms])->put(
                $cacheKey,
                $data
            );

            return $data;
        }
    }

    /**
     * Clean up fillform data returned by its API, we just get and treat field's
     * data
     *
     * @param array|false $data The fillform response data
     * @return array|false
     */
    public static function cleanResponse($data)
    {
        if (is_array($data)) {
            $fields = [];

            // clean up fields keys
            if (isset($data['fields'])) {
                $blacklist = array_flip([
                    'x',
                    'y',
                    'w',
                    'h',
                    'i',
                    'newsletter_field_name',
                    'formid',
                ]);
                $i = 0;
                foreach ($data['fields'] as $field) {
                    $fields[$i] = array_diff_key($field, $blacklist);

                    // FIXME: remove this when fillform fixes it
                    if (isset($field['placheholder'])) {
                        if (!isset($field['placeholder'])) {
                            $fields[$i]['placeholder'] = $field['placheholder'];
                        }
                        unset($fields[$i]['placheholder']);
                    }

                    $i++;
                }
            }

            return [
                'fields' => $fields,
            ];
        }

        return $data;
    }
}
