<?php

namespace LaravelFrontend\Forms;

use Illuminate\Support\Facades\Facade;

class FillformFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'fillform';
    }
}
