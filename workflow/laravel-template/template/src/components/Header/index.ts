import BaseHeader from "@acanto/components-header";
import I18nLinks from "@acanto/core-i18n/links";
import "components/Hamburger";
// import "utils/logo.scss";
import "./index.scss";

/**
 * Component: Header
 *
 */
export function Header() {
  const header = BaseHeader();
  I18nLinks();

  return {
    /**
     * Set variant
     *
     * @param {"light" | "dark"} variant
     */
    setVariant(variant) {
      const { className } = header.$root;
      header.$root.className = className.replace(
        /(is-).+(\s*)/,
        `$1${variant}$2`
      );
    },
  };
}
