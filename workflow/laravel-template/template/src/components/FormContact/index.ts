import "@acanto/core-forms/checkbox";
import FormsInputMaterial from "@acanto/core-forms/input/material";
// import FormsSelectMaterial from "@acanto/core-forms/select/material";
import "@acanto/core-forms/textarea/filled";
import Fillform from "@acanto/core-fillform/feedback";
import "./index.scss";

/**
 * Component: FormContact
 *
 */
export function FormContact() {
  const fillform = Fillform(".FormContact:");

  FormsInputMaterial();

  return {
    destroy() {
      fillform.destroy();
    },
  };
}
