import "@acanto/core-debug/api";
import "@acanto/core-reboot/index.scss";
import "@acanto/core-grid/index.scss";
import "@acanto/core-typography/reset.scss";
import "@acanto/core-icon/index.scss";
import LazyLoad from "@acanto/core-lazy";
import "@acanto/core-img";
import { Header } from "components/Header";
import "components/Footer";
import "./index.scss";
import "utils/btn.scss";
import "utils/fonts.scss";
import "utils/logo.scss";

Header();

new LazyLoad();
