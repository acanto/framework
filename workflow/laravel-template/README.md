# Laravel Template

![npm (scoped)](https://img.shields.io/npm/v/@acanto/laravel-template?style=flat-square&color=EA2C65)

> For all the documentation refer to the [Acanto Framework Docs](https://acanto.gitlab.io/framework/)

This is the official base template for [Create Laravel App](https://gitlab.com/acanto/framework/workflow/create-laravel-app).

If you don't specify a template (for example, `--template typescript`), this template will be used by default.

For more information, please refer to:

- [Getting Started](https://acanto.gitlab.io/framework/getting-started) – How to create a new app.
- [User Guide](https://acanto.gitlab.io/framework) – How to develop apps bootstrapped with Create Laravel App.

## Dev

### TODO

Maybe implement by default [quicklink](https://github.com/GoogleChromeLabs/quicklink), but check how it works alongside barba.js.
