import ajax from "@acanto/core-ajax";
import { getPostData } from "./helpers";

/**
 * Submit contact form
 */
export function submitContact(formData: object, ff: FillForm.data) {
  return ajax(ff.action, {
    method: "POST",
    data: getPostData(formData),
  });
}
