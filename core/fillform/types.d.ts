declare namespace FillForm {
  type data = {
    /** The fillform token of the current project */
    token: string;
    /** The URL endpoint for the fillform api of the current project */
    apiUrl: string;
    /** The fillform's form id of this form */
    id: string;
    /** The URL endpoint for this fillform's form */
    url: string;
    /** The URL endpoint that this fillform's form should submit to */
    action: string;
    /** any custom key/value pair put on the globally exposed data */
    [key: string]: any;
    /** subject or any other custom key/value pair */
    subject?: string;
  };

  /**
   * @param {string} rootSelector The DOM selector to run fillform upon
   */
  type initialiser = (
    rootSelector: string,
    hooks: hooks,
    adapters: adapters,
    checkers: checkers
  ) => instance;

  type hooks = {
    /** Called just before a valid submission attempt */
    before?: (instance: instance) => void;
    /** Called just after a succesfull server response */
    succeded?: (instance: instance) => void;
    /** Called just after a failed server response */
    failed?: (instance: instance) => void;
    /** Called on successful submitted data, it passes along the sent data, useful for analytics tracking */
    sent?: (formData: Object, data: FillForm.data) => void;
  };

  type adapters = {
    /** Data adapter for standard contact forms */
    contact?: (formData: Object, globalData: FillForm.data) => Object;
    /** Data adapter for mailchimp newsletter form data */
    mailchimp?: (formData: Object, globalData: FillForm.data) => Object;
    /** Data adapter for csv form data */
    csv?: (formData: Object, globalData: FillForm.data) => Object;
  };

  type checkers = {
    /** Checker to determine if the user wants to subscribe to the newsletter */
    newsletter?: (formData: Object) => boolean;
  };

  type instance = {
    $root: HTMLElement;
    $form: HTMLFormElement;
    $submit: HTMLButtonElement;
    data: data;
  };
}
