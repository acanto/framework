@props([
  'formId' => uniqid(), // just a fallback, you should pass it as prop
  'fields' => [],
  'textareaRows' => 8,
  'submitClassName' => '',
  'iconFile' => 'upload'
])
@foreach ($fields as $field)
  @php
    $fieldId = 'ff-' . $formId . '-' . $field['name'];
    $prevField = $fields[$loop->index - 1] ?? ['typology' => null];
    $nextField = $fields[$loop->index + 1] ?? ['typology' => null];
    $elAttrs = '';
    if (isset($field['hidden']) && $field['hidden']) $elAttrs .= ' style="display:none"';
    if (isset($field['trigger_val']) && $field['trigger_val']) $elAttrs .= ' data-trigger="'.$field['trigger_val'].'"';
    if (isset($field['action_val']) && $field['action_val']) $elAttrs .= ' data-action="'.$field['action_val'].'"';
    if (isset($field['value_val']) && $field['value_val']) $elAttrs .= ' data-value="'.$field['value_val'].'"';
    
    $_value = null;
    if (isset($field['value'])) {
      $_value = $field['value'];
    } else if(isset($field['default_value']) && $field['default_value'] !== ''){
      $_value = $field['default_value'];
    }

  @endphp 
    
  @if ($field['element'] == "Input")
    @if ($field['typology'] == "checkbox")
      @if ($nextField['typology'] == "checkbox")
      <div class="ff:group ff:{{ $field['name'] }}-{{ $nextField['name'] }}">
      @endif
      <div class="ff:el ff:{{ $field['name'] }}"{!! $elAttrs !!}>
        <x-forms-checkbox
          :id="$fieldId"
          :checked="isset($_value) && $_value"
          :value-true="$_value ?? 1"
          :label="$field['label']"
          :name="$field['name']"
          :type="$field['typology']"
          :required="$field['required']"
          :attrs="$field['attribute']"
          :class-element="$field['class']"
        />
      </div>
      @if ($prevField['typology'] == "checkbox")
      </div>
      @endif
    @elseif ($field['typology'] == "submit")
      <div class="ff:el ff:{{ $field['name'] }}"{!! $elAttrs !!}>
        <x-forms-submit
          :id="$fieldId"
          :value="$field['value'] ?? null"
          :label="$field['label']"
          :attrs="$field['attribute']"
          :class-root="$submitClassName"
          :class-element="$field['class']"
        />
      </div>
    @elseif ($field['typology'] == "file")
      <div class="ff:el ff:{{ $field['name'] }}"{!! $elAttrs !!}>
        <x-forms-file
          :id="$fieldId"
          :value="$field['value'] ?? null"
          :label="$field['label']"
          :placeholder="$field['placeholder'] ?? $field['placheholder']"
          :name="$field['name']"
          :type="$field['typology']"
          :required="$field['required']"
          :attrs="$field['attribute']"
          :class-element="$field['class']"
          :icon="$iconFile"
        />
      </div>
    @elseif ($field['typology'] == "hidden")
      <input type="hidden" name="{{ $field['name'] }}" value="{{ $_value ?? '' }}" />
    @else
    {{-- $field['typology'] == "text" | "email" | "url" --}}
      <div class="ff:el ff:{{ $field['name'] }}"{!! $elAttrs !!}>
        <x-forms-input
          :id="$fieldId"
          :value="$_value ?? null"
          :label="$field['label']"
          :placeholder="$field['placeholder'] ?? $field['placheholder']"
          :name="$field['name']"
          :type="$field['typology']"
          :required="$field['required']"
          :attrs="$field['attribute']"
          :class-element="$field['class']"
        />
      </div>
    @endif
  @elseif ($field['element'] == "InputList")
    @if ($field['typology'] == "radio")
      <div class="ff:el ff:{{ $field['name'] }}"{!! $elAttrs !!}>
        <x-forms-radio
          :id="$fieldId"
          :value="$_value ?? null"
          :label="$field['label']"
          :name="$field['name']"
          :type="'radio'"
          :options="$field['option']"
          :required="$field['required']"
          :attrs="$field['attribute']"
          :class-element="$field['class']"
        />
      </div>
    @else
      <div class="ff:el ff:{{ $field['name'] }}"{!! $elAttrs !!}>
        <x-forms-select
          :id="$fieldId"
          :value="$_value ?? null"
          :label="$field['label']"
          :placeholder="$field['placeholder'] ?? $field['placheholder']"
          :name="$field['name']"
          :type="'select'"
          :options="$field['option']"
          :required="$field['required']"
          :attrs="$field['attribute']"
          :class-element="$field['class']"
        />
      </div>
    @endif
  @elseif ($field['element'] == "Textarea")
    <div class="ff:el ff:{{ $field['name'] }}"{!! $elAttrs !!}>
      <x-forms-textarea
        :id="$fieldId"
        :value="$_value ?? null"
        :label="$field['label']"
        :placeholder="$field['placeholder'] ?? $field['placheholder']"
        :name="$field['name']"
        :required="$field['required']"
        :attrs="$field['attribute']"
        :rows="$textareaRows"
        :class-element="$field['class']"
      />
    </div>
  @elseif ($field['element'] == "Text")
    @if ($field['typology'] == "longtext")
      <div class="ff:el ff:{{ $field['name'] }} {{$field['class']}}" {!! $elAttrs !!}>
          <p class="textLabel">
            {!! $field['label'] !!}
          </p>
      </div>
    @elseif ($field['typology'] == "sorttext")
      <div class="ff:el ff:{{ $field['name'] }} {{$field['class']}}" {!! $elAttrs !!}>
        <p class="textLabel">
          {!! $field['label'] !!}
        </p>
      </div>
    @else
      <div class="ff:el ff:{{ $field['name'] }} {{$field['class']}}" {!! $elAttrs !!}>
        <p class="textLabel">
          {!! $field['label'] !!}
        </p>
      </div>      
    @endif  
  @endif
@endforeach