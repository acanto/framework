<form 
  id="{{ $domId ? $domId : $form['id'] }}" 
  class="ff:" 
  @isset($form['fillform']) data-ff="{{ json_encode($form['fillform']) }}"@endisset
>
  <x-fillform-fields
    :form-id="$form['id']"
    :fields="$form['fields']"
    :submit-class-name="$submitClassName"
    :icon-file="$iconFile"
    :textarea-rows="$textareaRows"
  />
  <x-debug-forms-btn :form-id="$form['id']" />
</form>
