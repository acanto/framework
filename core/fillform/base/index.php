<?php

namespace resources\components;

use Illuminate\View\Component;
use LaravelFrontend\Forms\Fillform;

class FillformBase extends Component
{
    public $form;
    public $extra;
    public $textareaRows;
    public $domId;
    public $submitClassName;
    public $iconFile;

    /**
     * Create a new component instance.
     *
     * FillForm.
     * Forms URL should be like:
     * https://api.fillform.io/533664cd32eea629d9692c3231225188/form/single/114
     * https://api.fillform.io/{token}/form/single/{formId}
     *
     * Usage:
     * ```
     * <x-fillform-base
     *   :forms="[ 'it' => '134', 'en' => '134' ]"
     *   :extra="[ 'subject' => 'contactform.email.subject' ]"
     *   dom-id="myForm"
     *   submit-class-name="btnCustom"
     *   textarea-rows="4"
     *   icon-file="upload"
     * />
     * ```
     *
     * @param string $token
     * @param array $forms Optional, an object like `forms: { it: '114', en: '114' }`
     * @param array $extra Optional, key/value pairs to add to the output object, it allows for instance to add a custom email subject
     * @return void
     */
    public function __construct(
        $form = [],
        $forms = [],
        $extra = [],
        $textareaRows = 8,
        $domId = 'myForm',
        $submitClassName = '',
        $iconFile = 'upload'
    ) {
        $this->form = array_merge(
            empty($form) ? Fillform::get($forms) : $form,
            $extra
        );
        $this->textareaRows = $textareaRows;
        $this->domId = $domId;
        $this->submitClassName = $submitClassName;
        $this->iconFile = $iconFile;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.fillform-base');
    }
}
