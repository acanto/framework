import { $, addClass, removeClass, getDataAttr } from "@acanto/core-dom";
import Validation from "@acanto/core-forms/validation";
import scrollTo from "@acanto/core-scroll/scrollTo";
import { getFormData } from "../helpers";
import { adaptDataForContact } from "../adapters";
import { submitContact } from "../submitters";
import "./index.scss";

/**
 * Fillform basic initialiser, it provides the standard FillForm behaviour and
 * comunication with the server
 *
 * @type {FillForm.initialiser}
 */
export default function Fillform(
  rootSelector: string = ".ffForm:",
  hooks: FillForm.hooks = {},
  adapters: FillForm.adapters = {},
  checkers: FillForm.checkers = {}
): FillForm.instance {
  const $root = $(rootSelector);
  const $form = $(".ff:", $root) as HTMLFormElement;
  const $submit = $("[type='submit']", $form) as HTMLButtonElement;
  const data = JSON.parse(getDataAttr($form, "ff"));
  const instance = {
    $root,
    $form,
    $submit,
    data,
    destroy,
  };

  // init form validation
  const validation = Validation($form, {
    onerror: handleInvalidSubmit,
    onsuccess: handleValidSubmit,
  });

  /**
   * Handle invalid submission attempt (before sending to server)
   *
   * Default behaviour is to scroll to first element with error
   * @param {import("../../forms/validation").Error[]} errors
   */
  function handleInvalidSubmit(errors) {
    const firstEl = errors[0].element;

    scrollTo(firstEl, {
      offset: 100,
      onstop: () => firstEl.focus(),
    });
  }

  /**
   * Handle valid submission attempt (before sending to server)
   */
  function handleValidSubmit() {
    callHookSafely("before", instance);

    onSubmitStart();

    const formDataRaw = getFormData($form);

    return new Promise<void>((resolve, reject) => {
      let dataContact = { ...formDataRaw };
      if (adapters.contact) {
        dataContact = adapters.contact(dataContact, data);
      } else {
        dataContact = adaptDataForContact(dataContact);
      }

      // first submit contact form which also operates server validation
      submitContact(dataContact, data)
        .then(() => {
          callHookSafely("sent", dataContact);
          handleSucceded();
          resolve();
        })
        .catch(() => {
          handleFailed();
          reject();
        });
    });
  }

  /**
   * Call hook safely (if defined)
   */
  function callHookSafely(hookName: keyof FillForm.hooks, specificData: any) {
    if (hooks[hookName]) hooks[hookName](specificData, data);
  }

  /**
   * Handle succeded ajax response
   */
  function handleSucceded() {
    onSubmitEnd();
    callHookSafely("succeded", instance);
  }

  /**
   * Handle failed ajax response
   */
  function handleFailed() {
    onSubmitEnd();
    callHookSafely("failed", instance);
  }

  /**
   * On submit start default behaviour
   */
  function onSubmitStart() {
    $submit.disabled = true;
    addClass($root, "is-loading");
  }

  /**
   * On submit end default behaviour
   */
  function onSubmitEnd() {
    $submit.disabled = false;
    removeClass($root, "is-loading");
  }

  /**
   * Destroy fillform instance
   *
   */
  function destroy() {
    validation.destroy();
  }

  return instance;
}
