import { $ } from "@acanto/core-dom";
import Collapsable from "@acanto/core-collapsable";
import Fillform from "../base";
import "@acanto/core-progress/loading.scss";
import "./index.scss";

/**
 * Fillform initialiser variant: withFeedback, it adds automatic feedback area
 * collapsing/uncollapsing
 *
 * @type {FillForm.initialiser}
 */
export default function FillformWithFeedback(
  rootSelector,
  hooks = {},
  adapters,
  checkers
) {
  const instance = Fillform(
    rootSelector,
    {
      ...hooks,
      before: handleBefore,
      succeded: handleSucceded,
      failed: handleFailed,
    },
    adapters,
    checkers
  );

  const $feedback = $(".ff:feedback", instance.$root);
  const $success = $(".ff:feedback__success", $feedback);
  const $failure = $(".ff:feedback__failure", $feedback);
  const areaForm = Collapsable(instance.$form);
  const areaFeedback = Collapsable($feedback);
  const { before, succeded, failed } = hooks;

  toggle($failure, false);
  toggle($success, false);

  areaFeedback.collapse(true);

  function handleBefore() {
    areaFeedback.collapse();
    if (before) before(instance);
  }

  function handleSucceded() {
    toggle($success, true);
    toggle($failure, false);
    areaForm.collapse();
    areaFeedback.expand();
    if (succeded) succeded(instance);
  }

  function handleFailed() {
    toggle($success, false);
    toggle($failure, true);
    areaFeedback.expand();
    if (failed) failed(instance);
  }

  function toggle(el, show) {
    el.style.display = show ? "block" : "none";
  }

  return instance;
}
