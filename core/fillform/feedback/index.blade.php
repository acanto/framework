@props([
  'success' => 'Done',
  'failure' => 'Failed',
])
<div class="ff:feedback">
  <div class="ff:feedback__msg ff:feedback__success">
    {!! $success !!}
  </div>
  <div class="ff:feedback__msg ff:feedback__failure">
    {!! $failure !!}
  </div>
</div>
