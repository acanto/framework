import FillForm from "@acanto/core-forms/ff/withFeedback";
import { submitContact } from "@acanto/core-forms/ff/submit";
import "@acanto/core-forms/input/filled";
import "@acanto/core-forms/textarea/filled";
import "@acanto/core-forms/checkbox";
import "@acanto/core-progress/loading.scss";
import "./index.scss";

function tplBodyRow(label, value) {
  return label && value
    ? `<tr><td><i style="color: red; font-size: 13px;">${label}:</i> ${value}</td></tr>`
    : "";
}

function tplBody(form, filled) {
  const { name, surname, email, nation, company, job, message } = filled;
  let output =
    "" +
    '<div class="table" style="padding-top:20px; padding-left: 15px; padding-right:15px;">' +
    '<table style="color: red; font-size: 17px;">' +
    "<tbody>" +
    "<tr>" +
    '<td style="color: red; text-decoration: none;"><strong>' +
    name +
    " " +
    surname +
    "</strong></td>" +
    "</tr>" +
    "<tr>" +
    '<td style="color: red; text-decoration: none;"><i style="color: red; font-size: 13px;">Email:</i><strong style="color: red;"> ' +
    email +
    " </strong></td>" +
    "</tr>" +
    "<tr>" +
    "<td></td>" +
    "</tr>" +
    "</tbody>" +
    "</table>" +
    '<div style="padding-top:10px; padding-bottom:10px">' +
    '<table style="color: red; font-size: 17px;">' +
    "<tbody>";
  output += tplBodyRow("Nazione", nation);
  output += tplBodyRow("Azienda", company);
  output += tplBodyRow("Lavoro", job);
  output +=
    "" +
    "<tr>" +
    "<td></td>" +
    "</tr>" +
    "</tbody>" +
    "</table>" +
    "</div>" +
    '<table style="color: red; font-size: 17px;">' +
    "<tbody>" +
    "<tr>" +
    '<td><i style="color: red; font-size: 13px;">Richiesta:</i></td>' +
    "</tr>" +
    '<tr style="padding-top: 10px">' +
    "<td>" +
    message +
    "</td>" +
    "</tr>" +
    "</tbody>" +
    "</table>" +
    "</div>";

  return output;
}

class FormContact extends FillForm {
  static rootSelector = ".FormContact:";

  submit() {
    submitContact.call(this);
  }

  transformData(formData) {
    const body = tplBody(this.data, formData);
    const { subject } = this.data;

    return { body, subject };
  }
}

new FormContact();
