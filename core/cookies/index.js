/**
 * Get cookie
 *
 * @param {string} name
 * @returns {string | null}
 */
export function getCookie(name) {
  var v = document.cookie.match("(^|;) ?" + name + "=([^;]*)(;|$)");
  return v ? v[2] : null;
}

/**
 * Set cookie
 *
 * @param {string} name
 * @param {string | number | boolean} value
 * @param {number} days
 */
export function setCookie(name, value, days) {
  var d = new Date();
  d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
  document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}

/**
 * Delete cookie
 *
 * @param {string} name
 */
export function deleteCookie(name) {
  setCookie(name, "", -1);
}
