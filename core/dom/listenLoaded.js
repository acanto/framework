import { on } from "./index";

/**
 * Fires a callback when the DOM content is loaded
 *
 * @see https://mathiasbynens.be/notes/settimeout-onload
 * @param {EventHandlerNonNull} handler
 */
export default function listenLoaded(handler) {
  on(document, "DOMContentLoaded", handler);
  // document.addEventListener("DOMContentLoaded", setTimeout(handler, 4));
}
