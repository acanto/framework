/**
 * Throttle for resize / scroll
 *
 * @see https://github.com/Mobius1/Rangeable/
 * @param {Function} fn
 * @param {Number} limit
 * @param {Object} context
 * @return {Function}
 */
export default function throttle(fn, limit, context) {
  let wait;
  return function () {
    context = context || this;
    if (!wait) {
      fn.apply(context, arguments);
      wait = true;
      return setTimeout(function () {
        wait = false;
      }, limit);
    }
  };
}
