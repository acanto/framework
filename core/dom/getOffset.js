/**
 * Get an element's distance from the top and left of the Document.
 *
 * @param  {HTMLElement} elem The HTML node element
 * @return {{ top: number, left: number }} Distance from the top and left in pixels
 */
export function getOffset(elem) {
  let left = 0;
  let top = 0;

  while (elem && !isNaN(elem.offsetLeft) && !isNaN(elem.offsetTop)) {
    left += elem.offsetLeft - elem.scrollLeft;
    top += elem.offsetTop - elem.scrollTop;
    // @ts-ignore
    elem = elem.offsetParent;
  }
  return { top, left };
}

/**
 * Get an element's distance from the top of the Document.
 *
 * @see https://vanillajstoolkit.com/helpers/getoffsettop/
 *
 * @param  {HTMLElement} elem The HTML node element
 * @return {number}    Distance from the top in pixels
 */
export function getOffsetTop(elem) {
  let location = 0;
  if (elem.offsetParent) {
    while (elem) {
      location += elem.offsetTop;
      // @ts-ignore
      elem = elem.offsetParent;
    }
  }
  return location >= 0 ? location : 0;
}
