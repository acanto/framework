import debounce from "lodash.debounce";
import { on, off } from "./index";

/**
 * Listen window scroll event using lodash's debounce
 *
 * @param {EventHandlerNonNull} handler
 * @param {import("lodash").DebounceSettings} [debounceOptions]
 * @returns {() => any} An automatic unbinding function to run to deregister the listener upon call
 */
export default function listenScroll(handler, debounceOptions) {
  const scrollHandler = debounce(handler, debounceOptions);

  on(window, "scroll", scrollHandler, {
    capture: true,
    passive: true,
  });

  /**
   * Unbind the previously attached scroll handler
   */
  function unbinder() {
    scrollHandler.cancel();
    off(window, "scroll", scrollHandler);
  }

  return unbinder;
}
