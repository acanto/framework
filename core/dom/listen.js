import { on, off, escape } from "./index";
import { isString } from "@acanto/core-helpers/type";

/**
 * Listen: events delegation system
 *
 * From:
 * https://github.com/cferdinandi/events
 * https://github.com/cferdinandi/events/blob/master/src/js/events/events.js
 *
 * @fileoverview
 */

/**
 * @typedef {Object} ListenEvent
 * @property {string} selector
 * @property {(event: Event, desiredTarget: window | Document | Element) => any} callback
 */

/**
 * Active events
 * @type {{ [key: string]: ListenEvent[] }}
 */
const activeEvents = {};

/**
 * Get the index for the listener
 *
 * @param  {Array} arr
 * @param  {string} selector
 * @param  {ListenEvent["callback"]} callback
 * @return {number} The index of the listener
 */
function getIndex(arr, selector, callback) {
  for (var i = 0; i < arr.length; i++) {
    if (
      arr[i].selector === selector &&
      arr[i].callback.toString() === callback.toString()
    )
      return i;
  }
  return -1;
}

/**
 * Check if the listener callback should run or not
 *
 * @param {Element} target The event.target
 * @param {string | window | Document | Element} selector The selector/element to check the target against
 * @return {window | Document | Element | false} If not false, run listener and pass the targeted element to use in the callback
 */
function getRunTarget(target, selector) {
  if (["*", "window", window].indexOf(selector) > -1) {
    return window;
  }
  if (
    [
      "document",
      "document.documentElement",
      document,
      document.documentElement,
    ].indexOf(selector) > -1
  )
    return document;

  if (isString(selector)) {
    return target.closest(escape(selector));
  }

  if (typeof selector !== "string" && selector.contains) {
    if (selector === target) {
      return target;
    }
    if (selector.contains(target)) {
      return selector;
    }
    return false;
  }

  return false;
}

/**
 * Handle listeners after event fires
 *
 * @param {Event} event The event
 */
function eventHandler(event) {
  if (!activeEvents[event.type]) return;
  activeEvents[event.type].forEach(function (listener) {
    const target = getRunTarget(
      /** @type {Element} */ (event.target),
      listener.selector
    );
    if (!target) {
      return;
    }
    listener.callback(event, target);
  });
}

/**
 * Listen an event
 *
 * @param {string} types The event type or types (comma separated)
 * @param {string} selector The selector to run the event on
 * @param {ListenEvent["callback"]} callback The function to run when the event fires
 */
export function listen(types, selector, callback) {
  // Make sure there's a selector and callback
  if (!selector || !callback) return;

  // Loop through each event type
  types.split(",").forEach(function (type) {
    // Remove whitespace
    type = type.trim();

    // If no event of this type yet, setup
    if (!activeEvents[type]) {
      activeEvents[type] = [];
      on(window, type, eventHandler, true);
    }

    // Push to active events
    activeEvents[type].push({
      selector: selector,
      callback: callback,
    });
  });
}

/**
 * Stop listening for an event
 *
 * @param {string} types The event type or types (comma separated)
 * @param {string} selector The selector to remove the event from
 * @param {Function} callback The function to remove
 */
export function unlisten(types, selector, callback) {
  // Loop through each event type
  types.split(",").forEach(function (type) {
    // Remove whitespace
    type = type.trim();

    // if event type doesn't exist, bail
    if (!activeEvents[type]) return;

    // If it's the last event of it's type, remove entirely
    if (activeEvents[type].length < 2 || !selector) {
      delete activeEvents[type];
      off(window, type, eventHandler, true);
      return;
    }

    // Otherwise, remove event
    var index = getIndex(activeEvents[type], selector, callback);
    if (index < 0) return;
    activeEvents[type].splice(index, 1);
  });
}

/**
 * Listen an event, and automatically unlisten it after it's first run
 *
 * @param {string} types The event type or types (comma separated)
 * @param {string} selector The selector to run the event on
 * @param {Function} callback The function to run when the event fires
 */
export function listenOnce(types, selector, callback) {
  listen(types, selector, function temp(event) {
    callback(event);
    unlisten(types, selector, temp);
  });
}

/**
 * Get an immutable copy of all active event listeners
 *
 * @return {Object} Active event listeners
 */
export function getListeners() {
  var obj = {};
  for (var type in activeEvents) {
    if (activeEvents.hasOwnProperty(type)) {
      obj[type] = activeEvents[type];
    }
  }
  return obj;
}
