/**
 * Determine if an element is in the viewport
 *
 * (c) 2017 Chris Ferdinandi, MIT License, https://gomakethings.com
 *
 * @param {Element} elem The element
 * @return {boolean} Returns true if element is in the viewport
 */
export default function isInViewport(elem) {
  const distance = elem.getBoundingClientRect();
  return (
    distance.top >= 0 &&
    distance.left >= 0 &&
    distance.bottom <=
      (window.innerHeight || document.documentElement.clientHeight) &&
    distance.right <=
      (window.innerWidth || document.documentElement.clientWidth)
  );
}
