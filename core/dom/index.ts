/**
 * @module Description
 * @overview A set of utilities to interact with the DOM, event listeners and
 * similar low level tasks. For a more structured API (jQuery like) consider
 * [DOMtastic](https://github.com/webpro/DOMtastic).
 *
 * ```js
 * import { ... } from "@acanto/core-dom";
 * ```
 */

/**
 * Escape colons to allow use class names as `.module:block__element`
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelector#Escaping_special_characters
 *
 * @param {string} selector
 * @returns {string}
 */
export function escape(selector: string) {
  return selector.replace(/:/g, "\\:");
}

/**
 * Sizzle/jQuery like DOM nodes shortcut for `document.querySelector`
 * To avoid an extra function call we inline the above `escape`
 *
 * @param {string} selector DOM selector
 * @param {HTMLElement | Document} [parent] It falls back to `window.document`
 * @param {boolean} [avoidEscape] Whether to avoid escaping `:` in the selector string
 * @example <caption>Basic DOM selection</caption>
 * const $container = $(".my-section:");
 * const $el = $("[data-some-attr]", $container);
 */
export function $<T extends Element = HTMLElement>(
  selector: string,
  parent?: HTMLElement | Document,
  avoidEscape?: boolean
) {
  return ((parent ? parent : document).querySelector(
    avoidEscape ? selector : selector.replace(/:/g, "\\:")
  ) as unknown) as T;
}

/**
 * Sizzle/jQuery like DOM nodes shortcut for `document.querySelectorAll`
 * To avoid an extra function call we inline the above `escape`
 *
 * @param {string} selector DOM selector
 * @param {HTMLElement | Document} [parent] It falls back to `window.document`
 * @param {boolean} [avoidEscape] Whether to avoid escaping `:` in the selector string
 */
export function $$<T extends Element = HTMLElement>(
  selector: string,
  parent?: HTMLElement | Document,
  avoidEscape?: boolean
) {
  return ((parent ? parent : document).querySelectorAll(
    avoidEscape ? selector : selector.replace(/:/g, "\\:")
  ) as unknown) as NodeListOf<T>;
}

/**
 * For each, iterate through a NodeList of HTMLElements
 *
 * @param {NodeList} nodes DOM nodes collection
 * @param {function(HTMLElement, number):any} callback
 * @param {object} [scope]
 */
export function forEach<T extends Element = HTMLElement>(
  nodes: NodeList,
  callback: ($element: T) => any,
  scope?: object
) {
  for (var i = 0; i < nodes.length; i++) {
    callback.call(scope, nodes[i], i);
  }
}

/**
 * Each shortcut, iterate through a NodeList of HTMLElements retrieved with the
 * given selector (and optionally a parent container passed as thrid arguement)
 *
 * @param {string} selector DOM selector
 * @param {function(HTMLElement, number):any} callback
 * @param {HTMLElement} [parent] It falls back to `window.document`
 * @param {object} [scope]
 */
export function $each<T extends Element = HTMLElement>(
  selector: string,
  callback: ($element: T, index: number) => any,
  parent?: HTMLElement,
  scope?: object
) {
  const nodes = $$(selector, parent);
  for (var i = 0; i < nodes.length; i++) {
    callback.call(scope, nodes[i], i);
  }
}

/**
 * Node list to array
 *
 * @param {NodeList | HTMLFormControlsCollection} nodeList
 */
export function toArray<T extends Element = HTMLElement>(
  nodeList: NodeListOf<T> | HTMLFormControlsCollection
) {
  return Array.prototype.slice.call(nodeList) as T[] | HTMLFormElement[];
}

/**
 * Add class shortcut
 */
export function addClass<T extends Element = HTMLElement>(
  el?: T,
  className?: string
) {
  if (__DEV__) {
    if (!el) {
      console.warn("Used `addClass` with an unexisting DOM element");
      return;
    }
  }
  if (el) el.classList.add(className);
}

/**
 * Remove class shortcut
 */
export function removeClass<T extends Element = HTMLElement>(
  el?: T,
  className?: string
) {
  if (__DEV__) {
    if (!el) {
      console.warn("Used `removeClass` with an unexisting DOM element");
      return;
    }
  }
  if (el) el.classList.remove(className);
}

/**
 * Set vendor CSS rule
 *
 * @param {HTMLElement} element A single HTMLElement
 * @param {string} prop CSS rule proerty
 * @param {string | number | boolean} value The CSS value to set, it will be automatically vendor prefixed
 */
export function setVendorCSS(
  element: HTMLElement,
  prop: string,
  value: string | number | boolean
) {
  const propUpper = prop.charAt(0).toUpperCase() + prop.slice(1);
  element.style["webkit" + propUpper] = value;
  element.style["moz" + propUpper] = value;
  element.style["ms" + propUpper] = value;
  element.style["o" + propUpper] = value;
  element.style[prop] = value;
}

/**
 * Is node list
 *
 * @param {any} nodes The object to check
 */
export function isNodeList<T>(nodes: any): nodes is NodeList {
  const stringRepr = Object.prototype.toString.call(nodes);

  return (
    typeof nodes === "object" &&
    /^\[object (HTMLCollection|NodeList|Object)\]$/.test(stringRepr) &&
    typeof nodes.length === "number" &&
    (nodes.length === 0 ||
      (typeof nodes[0] === "object" && nodes[0].nodeType > 0))
  );
}

/**
 * Shortcut for `addEventListener`
 *
 * @param {Window | Document | HTMLElement | Element} el
 * @param {string} type
 * @param {EventListener} handler
 * @param {AddEventListenerOptions | boolean} [options=false]
 */
export function on(
  el: Window | Document | HTMLElement | Element,
  type: string,
  handler: EventListener,
  options: AddEventListenerOptions | boolean = false
) {
  if (__DEV__) {
    if (!el) {
      console.warn("Used `on` with an unexisting DOM element");
      return;
    }
  }
  if (el) el.addEventListener(type, handler, options);
}

/**
 * Shortcut for `removeEventListener`
 *
 * @param {Window | Document | HTMLElement | Element} el
 * @param {string} type
 * @param {EventListener} handler
 * @param {EventListenerOptions | boolean} [options=false]
 */
export function off(
  el: Window | Document | HTMLElement | Element,
  type: string,
  handler: EventListener,
  options: EventListenerOptions | boolean = false
) {
  if (__DEV__) {
    if (!el) {
      console.warn("Used `off` with an unexisting DOM element");
      return;
    }
  }
  if (el) el.removeEventListener(type, handler, options);
}

/**
 * One shot listener, it `addEventListener` and removes it first time is called
 * with `removeEventListener`
 *
 * @param {Window | Document | HTMLElement} el
 * @param {string} type
 * @param {EventListener} handler
 * @param {EventListenerOptions | boolean} [options=false]
 */
export function once(
  el: Window | Document | HTMLElement,
  type: string,
  handler: EventListener,
  options: EventListenerOptions | boolean = false
) {
  const handlerWrapper = (event) => {
    handler(event);
    off(el, type, handlerWrapper);
  };

  on(el, type, handlerWrapper, options);
}

/**
 * Shortcut for `document.createElement`, allowing to to create an HTML element
 * with a given className directly (a very common use case)
 *
 * @param {string} type
 * @param {string} [className]
 * @return {HTMLElement}
 */
export function createElement(type: string, className?: string) {
  const el = document.createElement(type);
  if (className) {
    el.classList.add(className);
  }
  return el;
}

/**
 * Is element hidden?
 *
 * @param {HTMLElement} [el]
 * @return {boolean}
 */
export function isHidden(el?: HTMLElement) {
  return !el || el.offsetParent === null;
}

/**
 * Finds siblings nodes of the passed node.
 *
 * @see @glidejs/glide/src/utils/dom (source)
 * @param {Element} node
 * @return {Array}
 */
export function siblings(node: Element) {
  if (node && node.parentNode) {
    let n = node.parentNode.firstChild;
    let matched = [] as Element[];

    for (; n; n = n.nextSibling) {
      if (n.nodeType === 1 && n !== node) {
        matched.push(n as Element);
      }
    }

    return matched;
  }

  return [];
}

/**
 * Checks if passed node exist and is a valid element.
 *
 * @see @glidejs/glide/src/utils/dom (source)
 * @param {Element} [node]
 * @return {boolean}
 */
export function exists(node?: Element) {
  if (node && node instanceof window.HTMLElement) {
    return true;
  }

  return false;
}

export { getDataAttr, setDataAttr } from "./data";
export { listen, unlisten, listenOnce } from "./listen";
export { default as listenLoaded } from "./listenLoaded";
export { default as listenResize } from "./listenResize";
export { default as listenScroll } from "./listenScroll";
export { onClickOutside } from "./onClickOutside";
export { default as throttle } from "./throttle";
export { default as emitEvent } from "./emitEvent";
export { default as getHeight } from "./getHeight";
export { getOffset, getOffsetTop } from "./getOffset";
export { default as getDocumentHeight } from "./getDocumentHeight";
export { default as getScrollbarWidth } from "./getScrollbarWidth";
export { default as isTotallyScrolled } from "./isTotallyScrolled";
export { default as isInViewport } from "./isInViewport";
