/**
 * Get scrollbar's current width
 *
 * @param {HTMLElement} [element]
 * @returns {number}
 */
export default function getScrollbarWidth(element) {
  return window.innerWidth - (element || document.documentElement).clientWidth;
}
