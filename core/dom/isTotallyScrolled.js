/**
 * Is element totally scrolled
 *
 * @see https://github.com/willmcpo/body-scroll-lock/blob/master/src/bodyScrollLock.js#L116
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
 *
 * @param {HTMLElement} [el]
 * @returns {boolean}
 */
export default function isTotallyScrolled(el) {
  return el ? el.scrollHeight - el.scrollTop <= el.clientHeight : false;
}
