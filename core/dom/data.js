// TODO: maybe move to `dataset` API but consider the comment about Safari here
// https://stackoverflow.com/a/9201264/1938970

/**
 * Get data attribute
 *
 * @param {HTMLElement} element
 * @param {string} attribute The name of the `data-{attr}`
 * @return {string | null}
 */
export function getDataAttr(element, attribute) {
  // return element.dataset[attribute];
  return element.getAttribute("data-" + attribute);
}

/**
 * Set data attribute
 *
 * @param {Element} element
 * @param {string} attribute The name of the `data-{attr}`
 * @param {string | number | null} [value] The value to set, `null` or
 *                                         `undefined` will remove the attribute
 */
export function setDataAttr(element, attribute, value) {
  if (value === null || typeof value === "undefined") {
    // delete element.dataset[attribute];
    // return;
    element.removeAttribute("data-" + attribute);
    return;
  }
  // element.dataset[attribute] = value.toString();
  element.setAttribute("data-" + attribute, value.toString());
}
