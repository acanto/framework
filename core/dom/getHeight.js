/**
 * Get element height
 *
 * @param {HTMLElement} element
 * @returns {number}
 */
export default function getHeight(element) {
  return parseInt(window.getComputedStyle(element).height, 10);
}
