import debounce from "lodash.debounce";
import { on, off } from "./index";

/**
 * Listen window resize event using lodash's debounce
 *
 * @param {EventHandlerNonNull} handler
 * @param {import("lodash").DebounceSettings} [debounceOptions]
 * @returns {Function} An automatic unbinding function to run to deregister the listener upon call
 */
export default function listenResize(handler, debounceOptions) {
  const resizeHandler = debounce(handler, debounceOptions);

  on(window, "resize", resizeHandler);

  /**
   * Unbind the previously attached scroll handler
   */
  function unbinder() {
    resizeHandler.cancel();
    off(window, "resize", resizeHandler);
  }

  return unbinder;
}
