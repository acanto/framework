/**
 * Emit event (use only if the targeted browser supports `CustomEvent`s)
 *
 * @param {string} [type="customEvent"]
 * @param {Object} [detail={}]
 * @returns
 */
export default function emitEvent(type = "customEvent", detail = {}) {
  if (typeof window.CustomEvent !== "function") return;

  document.dispatchEvent(
    new CustomEvent(type, {
      bubbles: true,
      detail,
    })
  );
}
