import { on, off } from "./index";

export function onClickOutside(element, callback, autoUnbind = false) {
  const bind = (event) => {
    // if (event.target.closest(element) === null) {
    if (!element.contains(event.target)) {
      callback();
      if (autoUnbind) unbind();
    }
  };

  const unbind = () => {
    off(document, "click", bind);
  };

  on(document, "click", bind);

  return unbind;
}
