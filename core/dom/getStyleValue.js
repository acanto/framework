/**
 * Return the current style value for an element CSS property
 *
 * @param {HTMLElement} el The element to compute
 * @param {string} prop The style property
 * @returns {string}
 */
export default function getStyleValue(el, prop) {
  return getComputedStyle(el, null).getPropertyValue(prop);
}
