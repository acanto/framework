import videojs from "video.js/dist/alt/video.core.novtt.js";
import "./index.scss";
// export * from "video.js";

const defaultOptions = {
  // controls: true, // read from DOM
  // preload: "auto", // read from DOM
  liveui: false,
};

/**
 * Player, just a wrapper around `videojs`
 *
 * It is always better to import this module with async imports as such:
 * ```
 * let playerInstance;
 * import("@acanto/core-player").then(({ default: player }) => {
 *   playerInstance = player;
 * });
 * ```
 * or with async/await:
 * ```
 * const { player } = await import("@acanto/core-player");
 * ```
 * For videojs API @see https://docs.videojs.com/
 *
 * Regarding the bundle size @see https://github.com/videojs/video.js/issues/6166
 *
 * @param {string | HTMLElement | import("video.js").VideoJsPlayer} [rooter]
 * @param {import("./types").Player.options} [options]
 * @param {() => void} [ready]
 */
export const player = (rooter, options = {}, ready) => {
  return videojs(rooter, { ...defaultOptions, ...options }, ready);
};

export default player;
