// import { post } from "@acanto/core-ajax/laravel";
import { off } from "@acanto/core-dom";
import { $, $$, forEach, on } from "@acanto/core-dom";

/**
 * I18n switch select, a normal form that automatically submits on change
 *
 * @param {string} [rootSelector=''] Selector to scope the intialisation
 */
export default function I18nSelect(rootSelector = "") {
  const $forms = $$(`${rootSelector ? rootSelector + " " : ""}.i18nSelect`);
  const unbinders = [];

  forEach($forms, ($form) => {
    const $select = /** @type {HTMLSelectElement} */ ($(".formControl", $form));
    const current = $select.value;
    // const $options = $$(".selectOption", $select);

    unbinders.push({ el: $select, fn: handleChange });

    on($select, "change", handleChange);

    function handleChange(event) {
      const url = event.target.value;
      // const $option = $options[$select.selectedIndex];
      // if ($option) {
      //   const url = option.value;
      //   const locale = getDataAttr($option, "locale");
      //   document.location = url;
      // }

      // handleSubmit()

      if (url && current !== url) {
        // document.location.href = url;
        $form.submit();
      }
    }
  });

  return {
    destroy() {
      unbinders.forEach(({ el, fn }) => off(el, "change", fn));
    },
  };

  // function handleSubmit(url) {
  //   post("/_/i18n/switch", {
  //     data: { url }
  //   })
  // }
}
