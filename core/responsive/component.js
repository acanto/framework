import { listenResize } from "@acanto/core-dom";

/**
 * @typedef {"" | "mobile" | "desktop"} responsiveComponentMode
 */

/**
 * @typedef {Object} ResponsiveReadyComponent
 * @property {Function} init
 * @property {Function} destroy
 * @property {Function} [inMode] Dynamically added by the responsiveComponent
 */

/**
 * Responsive component
 *
 * @param {string} [breakpoint="lg"]
 * @param {Object} options
 * @param {Object} options.breakpoints
 * @param {ResponsiveReadyComponent} options.mobile
 * @param {ResponsiveReadyComponent} options.desktop
 * @returns
 */
export default function responsiveComponent(
  breakpoint = "lg",
  { breakpoints, mobile, desktop }
) {
  const DESKTOP = "desktop";
  const MOBILE = "mobile";
  let current = mobile;
  /** @type {responsiveComponentMode} */
  let mode = "";
  const threshold = breakpoints[breakpoint];

  listenResize(check);

  /**
   * In move?
   *
   * Attach a method to the given components that returns the information on
   * which mode is currently active
   *
   * @returns {"" | "mobile" | "desktop"}
   */
  function inMode() {
    return mode;
  }

  // dynamically add components methods
  mobile.inMode = inMode;
  desktop.inMode = inMode;

  /**
   * Check enabling the right mode based on screen width (desktop/mobile).
   * First we destroy the current one, then assign the new current component,
   * then initialise it. NB: all components used here need to have a `destroy`
   * and `init` method.
   */
  function check() {
    if (window.innerWidth >= threshold) {
      if (mode === MOBILE) {
        mobile.destroy();
      }
      if (mode !== DESKTOP) {
        desktop.init();
        mode = DESKTOP;
        current = desktop;
      }
    } else {
      if (mode === DESKTOP) {
        desktop.destroy();
      }
      if (mode !== MOBILE) {
        mobile.init();
        mode = MOBILE;
        current = mobile;
      }
    }
  }

  // check immediately
  check();

  return current;
}
