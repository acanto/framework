import { setVendorCSS, listenResize } from "@acanto/core-dom";

function adjust($origin, $target) {
  $target.style.marginTop = `-${
    $origin.parentNode.offsetHeight - $origin.offsetHeight
  }px`;
}

/**
 * The parent is the column content. So we measure the empty space between
 * the content and the bottom edge of the column. The difference in px is then
 * applied to the target column as a negative top margin, shifting up the column
 *
 * @param {HTMLElement} $origin
 * @param {HTMLElement} $target
 */
export default function compensateGridVerticalOffset($origin, $target) {
  setVendorCSS($target, "transition", "margin-top .2s ease-in");

  adjust($origin, $target);

  listenResize(() => adjust($origin, $target));
}
