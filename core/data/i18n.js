/**
 * Localise string
 *
 * Like in PHP it replaces interpolated variables with an object data, e.g.:
 * ```
 * __("Choose :name!", { name: "nobody" }) === "Choose nobody!";
 * ```
 *
 * @param {string} key
 * @param {Object} data
 * @returns string
 */
export function __(key, data, dictionary) {
  let value = dictionary ? dictionary[key] : window["__i18n"][key];

  if (value && data) {
    const matches = value.match(/(\:.*?)[\s|.|?|!|:|,|;]/g);
    for (let i = 0; i < matches.length; i++) {
      const dynamicStringPortion = matches[i];
      const dataKey = dynamicStringPortion.replace(":", "");
      if (data[dataKey]) {
        value = value.replace(
          new RegExp(dynamicStringPortion, "g"),
          data[dataKey]
        );
      }
    }
  }

  return value;
}
