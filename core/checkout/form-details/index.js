import FormsBase from "@acanto/core-forms/base";

/**
 * Core: Checkout Form Details
 */
/**
 * This is a change
 */ 
export default function CheckoutFormDetails($root, options = {}) {
  return FormsBase($root, ".checkoutFormDetails", options);
}