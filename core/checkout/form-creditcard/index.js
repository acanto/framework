import FormsBase from "@acanto/core-forms/base";

/**
 * Core: Checkout Form Details
 */
/**
 * This is a change
 */ 
export default function CheckoutFormCreditcard($root, options = {}) {
  return FormsBase($root, ".checkoutFormCreditcard", options);
}