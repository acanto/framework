import { escape, listenResize } from "@acanto/core-dom";
import Glide from "./glide.modular.esm";
import "./index.scss";

/**
 * @typedef {Object} glideOptions
 * @property {{ above?: number, below?: number }} [killWhen] Kill/destroy the slider above or below a certain `window.innerWidth`. When using this option components must be passed as third argument
 * @property {boolean} [sizeByTrack=false] Calculates the slider width based on the `data-glide-el="track"` element, useful when paried up with `ContrainToImgHeight` custom component
 * @property {glideInstance} [slaveOf] Turns a glide instance into a non-interactive slave of another, synced with it
 * @property {glideInstance} [sync] Allows to sync multiple glide instances
 * @property {number|boolean} [autoHeight] This just sets the animation timing of the autoHeight in `ms`. N.B.: it's necessary to also import and add the component `AutoHeight`
 * @property {{ enter: number, exit: number }} [delay]
 * @property {boolean} [loop=true] When true slides clones will be created
 * @property {number} [startAt=0] Start at specific slide number
 * @property {number} [perMove=1] Number of slides to slide at once
 * @property {number} [perView=1] Number of visible slides at once
 * @property {number} [focusAt=0] Focus currently active slide at a specified position
 * @property {number} [gap=0] A size of the space between slides
 * @property {number} [autoplay=false] Change slides after a specified interval
 * @property {number} [cloneRatio=1] Definest how many clones will be created in looped mode.
 * @property {boolean} [hoverpause=true] Stop autoplay on mouseover
 * @property {boolean} [keyboar=true] Change slides with keyboard arrows
 * @property {boolean} [bound=false] Stop running perView number of slides from the end
 * @property {boolean | number} [swipeThreshold=80] Minimal swipe distance needed to change the slide
 * @property {number} [dragThreshold=120] Minimal mousedrag distance needed to change the slide
 * @property {"perView" | "perMove"} [perSwipe="perView"] A maximum number of slides moved per single swipe or drag
 * @property {number | boolean} [touchRatio=0.5] Alternate moving distance ratio of swiping and dragging. Use `false` for unlimited
 * @property {number} [touchAngle=45] Angle required to activate slides moving
 * @property {number} [animationDuration=400] Duration of the animation
 * @property {boolean} [rewind] Allow looping the slider type
 * @property {number} [rewindDuration=800] Duration of the rewinding animation
 * @property {string} [animationTimingFunc='cubic-bezier(0.165, 0.840, 0.440, 1.000)'] Easing function for the animation
 * @property {"ltr" | "rtl"} [direction] Moving direction mode
 * @property {number | { before: number, after: number }} [peek] The value of the future viewports which have to be visible in the current view
 * @property {Object<number, glideOptions>} [breakpoints] Collection of options applied at specified media breakpoints
 * @property {number} [throttle=25] Throttle costly events
 * @property {{
    swipeable: string;
    direction: {
      ltr: string;
      rtl: string;
    },
    dragging: string;
    slide: {
      clone: string;
      active: string;
    },
    arrow: {
      disabled: string;
    },
    nav: {
      active: string;
    }
  }} [classes] Better not override these
 *
 * Deprecated property {Object} [classes] Collection of used HTML classes
 */

/**
 * @typedef {Object} glideComponents
 * @property {Function} [AutoHeight] Custom component
 * @property {Function} [CrossFade] Custom component
 * @property {Function} [Delay] Custom component
 * @property {Function} [Fade] Custom component
 * @property {{ load: (container: HTMLElement, callback?: Function) => void }} [LazyLoad] Custom component
 * @property {Function} [NextPrev] Custom component
 * @property {Function} [SlaveOf] Custom component
 * @property {any} [Anchors]
 * @property {any} [Breakpoints]
 * @property {any} [Controls]
 * @property {any} [Hover]
 * @property {any} [Images]
 * @property {any} [Keyboard]
 * @property {any} [Swipe]
 * @property {{ root: HTMLElement, track: HTMLElement, slides: HTMLElement[], wrapper: HTMLElement }} Html
 * @property {{ items: HTMLElement[] }} Clones
 */

/**
 * @typedef {"build" | "build.after" | "build.before" | "mount" | "mount.after" | "mount.before" | "run" | "run.after" | "run.before" | "swipe" | "destroy" | "update" | "resize" | "clones.after" | "enabled" | "disabled" | "autoHeight.after" | "lazyLoad.after"} glideEvent And others...
 * @typedef {Object} glideEvents
 * @property {(eventName: glideEvent | glideEvent[], handler: Function) => any} [on]
 * @property {(eventName: glideEvent | glideEvent[], handler: Function) => any} [off]
 * @property {(eventName: string, eventData?: any) => any} [emit]
 */

/**
 * @typedef {">" | "<" | "={i}" | ">>" | "<<"} glidePattern One of:
 * - `>`: Move one forward
 * - `<`: Move one backward
 * - `={i}`: Go to {i} zero-based slide (eq. '=1', will go to second slide)
 * - `>>`: Rewinds to end (last slide)
 * - `<<`: Rewinds to start (first slide)
 */

/**
 * @typedef {Object} glideInstance
 * @property {number} index
 * @property {glideOptions} settings
 * @property {boolean} disabled
 * @property {(components?: glideComponents) => glideInstance} mount
 * @property {(options: glideOptions) => glideOptions} update
 * @property {Function} destroy
 * @property {(event: string, handler: Function) => glideInstance} on
 * @property {(pattern: glidePattern) => glideInstance} go
 * @property {Function} pause
 * @property {(isPlaying: boolean) => glideInstance} play
 * @property {Function} disable
 * @property {Function} enable
 */

/**
 * Init glidejs slider (custom fork)
 *
 * @param {string | HTMLElement} selector
 * @param {glideOptions} options
 * @param {glideComponents} [components={}]
 * @returns {glideInstance}
 */
export function init(selector, options, components = {}) {
  // allow to pass a DOM element or a selector. Escape it to allow the use of
  // colons in HTML classNames
  // @ts-ignore
  let glide = /** @type {glideInstance} */ (new Glide(
    typeof selector === "string" ? escape(selector) : selector,
    options
  ));

  // FIXME: @styleloader
  if (__DEV__) {
    const _mount = glide.mount;
    // const _mount = glide.mount.bind(glide);
    // @ts-ignore
    glide.mount = function (components) {
      setTimeout(() => {
        _mount.call(glide, components);
      }, 50);
    };
  }

  // @see https://github.com/glidejs/glide/issues/208
  if (options.killWhen) {
    const { above, below } = options.killWhen;
    let hasInit = false;

    const toggle = () => {
      if (
        (above && window.innerWidth >= above) ||
        (below && window.innerWidth < below)
      ) {
        if (hasInit) {
          hasInit = false;
          try {
            glide.destroy();
          } catch (e) {
            // FIXME: sometimes the glide.Events class gives error, if resizing
            // too fast maybe, not sure if this is a problem...
          }
          // glide.disable();
        }
      } else {
        if (!hasInit) {
          glide.mount(components);

          // if (!glide) {
          //   glide = /** @type {glideInstance} */ (new Glide(
          //     typeof selector === "string" ? escape(selector) : selector,
          //     options
          //   )).mount(components);;
          // } else {
          //   glide.mount(components);
          // }
          hasInit = true;
        }
      }
    };

    const listenerResize = listenResize(toggle);

    glide.on("destroy", listenerResize);

    toggle();
  }

  // return the glide instance, with its default API,
  // @see https://glidejs.com/docs/api/
  return glide;
}
