import { $, on } from "@acanto/core-dom";
import onScroll from "@acanto/core-scroll/onScroll";
import scrollTo from "@acanto/core-scroll/scrollTo";

/**
 * Link to inline video
 *
 * Video link, import video player when it comes into the viewport or when the
 * trigger link is clicked, on this latter event wait for scroll end and then
 * play the video.
 *
 * @param {string} triggerSelector
 * @param {string} targetSelector
 */
export default function linkToInlineVideo(triggerSelector, targetSelector) {
  const $trigger = $(triggerSelector);
  if (!$trigger) return;

  const $target = $(targetSelector);
  const getPlayer = () => import("@acanto/core-player");
  const handleVideoReached = () => {
    getPlayer().then(({ default: player }) => {
      player($(".video-js", $target)).play();
    });
  };

  on($trigger, "click", (event) => {
    event.preventDefault();

    scrollTo($target, {
      onstop: handleVideoReached,
    });
  });

  onScroll($target, {
    onin: getPlayer,
  });
}
