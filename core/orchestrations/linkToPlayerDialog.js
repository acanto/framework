import { listen, getDataAttr } from "@acanto/core-dom";
import openDialogPlayer from "@acanto/core-dialog/player";

/**
 * Link to player dialog
 *
 * Matches all elements with the given selector and bind them to open a player
 * dialog, the targeted elements must have a `data-src="{videoUrl}"` attribute
 * and optionally a `data-poster="{posterUrl}"` attribute
 *
 * @param {string} selector
 * @param {(src: string, poster?: string) => any} [onclick]
 */
export default function linkToPlayerDialog(selector, onclick) {
  listen("click", selector, function (event, el) {
    event.preventDefault();
    const src = getDataAttr(el, "src");
    const poster = getDataAttr(el, "poster");

    if (onclick) onclick(src, poster);

    openDialogPlayer({
      w: 1920,
      h: 1080,
      src,
      poster,
    });
  });
}
