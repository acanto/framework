import { $, setVendorCSS } from "@acanto/core-dom";

/**
 * Collapsable
 *
 * Simple utility to make an element collapsable. It requires the element
 * not to have vertical margins, as we simply read the "inner" height of the
 * given element.
 *
 * @param {string|HTMLElement} selector
 * @param {HTMLElement} [$root]
 * @param {Object} [options]
 * @param {number} [options.duration=300]
 * @param {function(HTMLElement):any} [options.didExpand]
 * @param {function(HTMLElement):any} [options.didCollapse]
 */
export default function collapsable(selector, $root, options) {
  const { duration = 300, didExpand, didCollapse } = options || {};
  /** @type HTMLElement */
  // @ts-ignore
  const $el = typeof selector === "string" ? $(selector, $root) : selector;

  if (!$el) {
    return;
  }

  let lastHeight = $el.scrollHeight;
  let isExpanded = lastHeight > 0;

  $el.style.overflow = "hidden";

  setVendorCSS($el, "transition", `height ${duration}ms ease`);

  setAriaExpanded(isExpanded);

  /**
   * Set ARIA attribute on the element to flag the expanded status
   */
  function setAriaExpanded(expanded) {
    $el.setAttribute("aria-expanded", expanded);
  }

  /**
   * Toggle element
   */
  function toggle() {
    // const collapsed = $el.getAttribute("aria-expanded");
    // if (collapsed === "true") {
    if (isExpanded) {
      collapse();
    } else {
      expand();
    }
  }

  /**
   * Expand element
   *
   * @param {boolean} [immediate] Run without animation
   */
  function expand(immediate) {
    if (immediate) {
      _doExpand();
    } else {
      $el.style.height = `${$el.scrollHeight}px`;

      setTimeout(_doExpand, duration + 1);
    }
  }

  /**
   * Actually expand
   *
   */
  function _doExpand() {
    $el.style.height = "auto";
    lastHeight = $el.scrollHeight;

    setAriaExpanded(true);
    isExpanded = true;

    if (didExpand) didExpand($el);
  }

  /**
   * Collapse element
   *
   * @param {boolean} [immediate] Run without animation
   */
  function collapse(immediate) {
    if (immediate) {
      _doCollapse();
    } else {
      $el.style.height = `${lastHeight}px`;

      setTimeout(_doCollapse, 3);
    }
  }

  /**
   * Actually collapse
   *
   */
  function _doCollapse() {
    $el.style.height = "0";
    setAriaExpanded(false);
    isExpanded = false;

    setTimeout(() => {
      if (didCollapse) didCollapse($el);
    }, duration + 3);
  }

  return {
    toggle,
    expand,
    collapse,
  };
}
