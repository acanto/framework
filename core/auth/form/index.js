import { setTimezone } from "../timezone";
import { updateFormsRedirectFromUrl } from "../redirect";

export default function AuthForm() {
  setTimezone();
  updateFormsRedirectFromUrl();
}
