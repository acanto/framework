import FormsBase from "@acanto/core-forms/base";
import AuthForm from "../form";
import "./index.scss";

/**
 * Core: AuthFormRegister
 */
export default function AuthFormRegister($root, options = {}) {
  AuthForm();
  return FormsBase($root, ".authFormRegister", options);
}
