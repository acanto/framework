import FormsBase from "@acanto/core-forms/base";
import AuthForm from "../form";
import "./index.scss";

/**
 * Core: AuthFormPasswordRecovery
 */
export default function AuthFormPasswordRecovery($root, options = {}) {
  AuthForm();
  return FormsBase($root, ".authFormPasswordRecovery", options);
}
