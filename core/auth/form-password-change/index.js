import FormsBase from "@acanto/core-forms/base";
import AuthForm from "../form";
import "./index.scss";

/**
 * Core: Auth Form PasswordChange
 */
export default function AuthFormPasswordChange($root, options = {}) {
  AuthForm();
  return FormsBase($root, ".authFormPasswordChange", options);
}
