import FormsBase from "@acanto/core-forms/base";
import AuthForm from "../form";
import "./index.scss";

/**
 * Core: AuthFormProfile
 */
export default function AuthFormProfile($root, options = {}) {
  AuthForm();
  return FormsBase($root, ".authFormProfile", options);
}
