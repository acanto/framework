@props([
  'form' => AuthApi::getForm('profile'),
  'id' => 'profile',
  'ajax' => false,
  'timezone' => true,
  'redirect' => '',
])
@php
  $userData = AuthApi::user() ?? [];
  $form = Fillform::prefill($form, $userData);
@endphp
<form {{ $attributes->merge(['class' => 'authForm authFormProfile']) }}
  action="{{ $form['action'] }}"
  method="post"
  data-auth="profile"
  data-ajax-submit="{{ $ajax }}"
>
  {{ $pre ?? '' }}
  @csrf
  @if($timezone)<x-auth-timezone />@endif
  <x-auth-redirect url="{{ $redirect }}" />
  @isset($form['fields'])
    <x-fillform-fields
      :form-id="$id"
      :fields="$form['fields']"
    />
  @endisset
  {{ $slot ?? '' }}
  {{ $success ?? '' }}
  {{ $error ?? '' }}
  <x-debug-forms-btn :form-id="$form['id']" />
</form>
