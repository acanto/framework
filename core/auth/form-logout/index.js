import FormsBase from "@acanto/core-forms/base";
import AuthForm from "../form";
import "./index.scss";

/**
 * Core: Auth Form Logout
 */
export default function AuthFormLogout($root, options = {}) {
  AuthForm();
  return FormsBase($root, ".authFormLogout", options);
}
