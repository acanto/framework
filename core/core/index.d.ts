// Type definitions for @acanto/core
// Project: @acanto/core
// Definitions by: Acanto <https://acanto.agency/>

declare interface Window {
  /** Data exposed from the server through `<x-data />` */
  __DATA: { string: any };

  /** Google tag manager `dataLayer` is nearly always injected in production websites. */
  dataLayer: any[];
  /** Google tag manager `object`, . */
  google_tag_manager?: any;
}
