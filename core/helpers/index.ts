export { mergeObjects } from "./data";
export { callHookSafely, getScopedRoot } from "./interface";
export {
  getBaseUrl,
  getEndpoint,
  getPathnameParts,
  getQueryParams,
  buildQueryString,
  changeUrlPath,
  changeUrlParams,
  removeUrlParam,
  redirectTo,
} from "./location";
export {
  trueTypeOf,
  isArray,
  isObject,
  isDate,
  isRegexp,
  isNull,
  isFunction,
  isString,
  isNumber,
  isBoolean,
  isUndefined,
  isFormData,
} from "./type";
export { default as debounce } from "lodash.debounce";
export { default as throttle } from "lodash.throttle";

/**
 * Uuid, tiny custom helper instead of node's uuid/v4
 *
 * @see https://stackoverflow.com/a/2117523/1938970
 * @returns {string}
 */
export function uuid() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

/**
 * Get random int
 *
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
export function randomInt(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * Get random key from given object
 *
 * @see https://stackoverflow.com/a/15106541/1938970
 * @param {object} obj
 * @returns {any}
 */
export function randomKey(obj) {
  const keys = Object.keys(obj);
  return keys[(keys.length * Math.random()) << 0];
}

/**
 * Maps an array of objects into a map keyed with the given key
 *
 * @param {Object[]} [array=[]]
 * @param {string} [key=""]
 * @returns {Object}
 */
export function mapListBy(array = [], key = "") {
  return array.reduce((obj, item) => {
    obj[item[key]] = item;
    return obj;
  }, {});
}

/**
 * Remove duplicated array objects, equality is determined by a strict (`===`)
 * comparison of each object's given key
 *
 * @param {object[]} [array=[]]
 * @param {string} [key=""]
 * @returns {object[]}
 */
export function removeDuplicatesByKey(array = [], key = "") {
  const keysMap = {};
  const output = [];

  for (let i = 0; i < array.length; i++) {
    const item = array[i];
    if (!keysMap[item[key]]) {
      output.push(item);
      keysMap[item[key]] = true;
    }
  }

  return output;
}

/**
 * Swap object map key/value
 *
 * @param {object} [map=[]]
 * @returns {object}
 */
export function swapMap(map = {}) {
  const output = {};
  for (const key in map) {
    output[map[key]] = key;
  }
  return output;
}

/**
 * Download file programmatically, it fakes a click action, should only work
 * from same origin domain
 *
 * @param {string} url
 * @param {string} name
 */
export function downloadFile(url, name) {
  const a = document.createElement("a");
  a.download = name;
  a.href = url;
  a.click();
}
