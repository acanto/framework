import { listenResize } from "@acanto/core-dom";

/**
 * Size canvas (and return its new width and height)
 *
 * @param {HTMLCanvasElement} element
 * @returns {{ w: number, h: number }} The current canvas height (same as window by default)
 */
function sizeFullscreenCanvas(element) {
  const w = window.innerWidth;
  const h = window.innerHeight;
  element.width = w;
  element.height = h;

  return { w, h };
}

/**
 * Create fullscreen canvas
 *
 * @param {string} id
 * @param {(canvas: HTMLCanvasElement, w: number, h: number) => any} [onResize]
 * @returns {HTMLCanvasElement}
 */
export default function createFullscreenCanvas(id, onResize) {
  const canvas = document.createElement("canvas");
  canvas.id = id;
  document.body.appendChild(canvas);

  sizeFullscreenCanvas(canvas);

  listenResize(() => {
    const size = sizeFullscreenCanvas(canvas);
    if (onResize) {
      onResize(canvas, size.w, size.h);
    }
  });

  return canvas;
}
