import { gsap } from "gsap";
import listenResize from "@acanto/core-dom/listenResize";

/**
 * @typedef {Object} GsapTilt.Options
 * @property {boolean} hasPower - Indicates whether the Power component is present.
 * @property {HTMLElement} area - The mousemove sensible area
 * @property {HTMLElement} target - The target element to tilt
 * @property {number} [max=8]
 * @property {boolean} [full=false]
 * @property {boolean} [reverse=true]
 */

export default class GsapTilt {
  /**
   * Creates an instance of GsapTilt.
   * @param {GsapTilt.Options} { gsap, area, target } Options
   * @memberof GsapTilt
   */
  constructor({ area, target, max = 8, full = false, reverse = true }) {
    this.area = area;
    this.target = target;
    this.max = max;
    this.full = full;
    this.reverse = reverse ? -1 : 1;

    gsap.set(target, { transformStyle: "preserve-3d" });
    this.area.addEventListener("mousemove", this.handleMouseMove.bind(this));
    // this.area.addEventListener("mouseenter", this.handleMouseEnter.bind(this));
    // this.area.addEventListener("mouseleave", this.handleMouseLeave.bind(this));

    this.updateTargetPos();
    listenResize(this.updateTargetPos.bind(this));

    return this;
  }

  updateTargetPos() {
    if (this.full) {
      this.clientWidth =
        window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth;
      this.clientHeight =
        window.innerHeight ||
        document.documentElement.clientHeight ||
        document.body.clientHeight;
    } else {
      const rect = this.area.getBoundingClientRect();
      this.width = this.area.offsetWidth;
      this.height = this.area.offsetHeight;
      this.left = rect.left;
      this.top = rect.top;
    }
  }

  handleMouseMove(event) {
    let x, y;
    const { clientX, clientY } = event;
    if (this.full) {
      x = clientX / this.clientWidth;
      y = clientY / this.clientHeight;
    } else {
      x = (clientX - this.left) / this.width;
      y = (clientY - this.top) / this.height;
    }

    const tiltX = parseFloat(
      (this.reverse * (this.max - x * this.max * 2)).toFixed(2)
    );
    const tiltY = parseFloat(
      (this.reverse * (y * this.max * 2 - this.max)).toFixed(2)
    );

    // const tiltX =  e.pageX / $body.width()  * 100 - 50;
    // const tiltY =  e.pageY / $body.height() * 100 - 50;

    gsap.to(this.target, {
      rotationY: 0.2 * tiltX, // 0.05 * tiltX,
      rotationX: 0.4 * tiltY, // 0.20 * tiltY,
      rotationZ: "-0.1",
      transformPerspective: 500,
      transformOrigin: "center center",
    });
  }

  destroy() {
    this.area.removeEventListener("mousemove", this.handleMouseMove);
  }
}
