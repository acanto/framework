// import _isEmail from "validator/lib/isEmail";
import { $ } from "@acanto/core-dom";

/**
 * Validator: is email check
 *
 * @param {HTMLInputElement} element
 * @returns {Boolean}
 */
export default function isFilesize(element) {
    const file = element.files[0];
    if(!file){
        return true
    }

    const size = file.size;
    const $file = $('.file');
    const sizeLimit = $('.fileSize', $file).value;
    if(size > parseInt(sizeLimit)){
        return false;
    } 

    return true;
}
