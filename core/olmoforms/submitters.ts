import ajax from "@acanto/core-ajax";
// import { getPostData } from "./helpers";
// import { $ } from "@acanto/core-dom";

/**
 * Submit contact form
 */
export function submitContact(formData: object, action: Olmoforms.data) {
  
  function getMeta(metaName) {
    const metas = document.getElementsByTagName('meta');

    for (let i = 0; i < metas.length; i++) {
      if (metas[i].getAttribute('name') === metaName) {
        return metas[i].getAttribute('content');
      }
    }

    return '';
  }

  return ajax(action, {
    headers: {
      'X-CSRF-TOKEN': getMeta('csrf-token')
    },
    method: "POST",
    data: formData,
  });
}
