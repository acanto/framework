import { $, $$, forEach, on, addClass, removeClass, getDataAttr } from "@acanto/core-dom";

/**
 * Olmoforms basic initialiser, it provides the standard Olmoforms behaviour and
 * comunication with the server
 *
 * @type {Olmoforms.initialiser}
 */
export default function Dependency(
    rootSelector: string = ".ffForm:"
  ): Olmoforms.instance {
    const $root = $(rootSelector);
    const $form = $(".of:", $root) as HTMLFormElement;    
    const $els = $$(".of:el", $form) as HTMLFormElement; 
    const $formid = getDataAttr($form, "id");
    const instance = {
      $root,
      $form,
    };   

    forEach($els, (el) => {        

        if(el.hasAttribute('data-value')){

            const value = el.getAttribute('data-value');
            const $dep = $('#of-'+$formid+'-'+value);
            
            const name = el.getAttribute('data-name');
            const $source = $('#of-'+$formid+'-'+name);

            on($dep, 'keyup', (e) => {
                const $trigger = el.getAttribute('data-trigger');
                if(e.target.value == $trigger){
                    setElement(el, $source);
                } else {
                    disableElement(el, $source);
                }
            });

            on($dep, 'change', (e) => {
                const $trigger = el.getAttribute('data-trigger');
                if(e.target.value == $trigger){
                    setElement(el, $source);
                } else {
                    disableElement(el, $source);
                }
            });

            /** Loading page */
            const $trigger = el.getAttribute('data-trigger');
            if($dep.value == $trigger){
                setElement(el, $source);
            } else {
                disableElement(el, $source);
            }

        }

    });

    function setElement(el, dep){
        el.style.display = "block";
        dep.setAttribute('required', 'required');
        // dep.value = "";
    }
    function disableElement(el, dep){
        el.style.display = "none";
        dep.removeAttribute('required');
        dep.value = "";
    }

}