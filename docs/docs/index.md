---
id: index
title: Acanto Framework
sidebar_label: Introduction
slug: /
hide_table_of_contents: true
---

A set of multi-language packages (js, scss, php) to develop, deploy and mantain Acanto Frontend applications.

![acanto-framework](../static/img/screenshots/banner.png)

## Latest versions

- `"@acanto/components"`: ![npm (scoped)](https://img.shields.io/npm/v/@acanto/components?style=flat-square&color=EA2C65)
- `"@acanto/core"`: ![npm (scoped)](https://img.shields.io/npm/v/@acanto/core?style=flat-square&color=EA2C65)
- `"@acanto/laravel-scripts"`: ![npm (scoped)](https://img.shields.io/npm/v/@acanto/laravel-scripts?style=flat-square&color=539DCC)
- `"@acanto/use"`: ![npm (scoped)](https://img.shields.io/npm/v/@acanto/use?style=flat-square&color=EA2C65)

- `"acanto/laravel-frontend"`: ![composer (scoped)](https://img.shields.io/packagist/v/acanto/laravel-frontend?style=flat-square&color=53CCB6)

## Features

- Hot Module Replacement
- JS/SCSS modules bundling with `webpack`
- SCSS compilation with `sass` and `postcss`
- SCSS optimization combining media queries with `postcss-sort-media-queries`
- ES6 syntax support with `babel`
- Routes specific JS/CSS
- CSS and JS files automatic splitting and cache busting (content hashed)
- Assets minification
- Images dynamic resizing, processing, minification and compression
- SVG inline icons automatic generation from `svg` images
- Favicons auto-generation from single image
- License banner for compiled `css` and `js` files
- Route and Component generator with `.js`, `.scss` and `.php` templates
