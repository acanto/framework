---
id: index
title: Laravel Frontend
sidebar_label: Overview
slug: /laravel-frontend
---

![composer (scoped)](https://img.shields.io/packagist/v/acanto/laravel-frontend?style=flat-square&color=53CCB6)

`"acanto/laravel-frontend"`

Opinionated Laravel wrapper to build frontend applications in tandem with [@acanto/laravel-script](https://www.npmjs.com/package/@acanto/laravel-scripts), it provides customizations, conventions and helpers integrated in the laravel framework.
