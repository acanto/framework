<?php

use Cms\Classes\Theme;
use System\Classes\MediaLibrary;

function onStart()
{
    $rows = $this['data'];
    $gutter = $this['gutter'];
    $maxWidth = 0;
    $idxRow = 0;

    foreach ($rows as $row) {
        $row['width'] = 0;
        $row['maxWidthCol'] = 0;
        $idxCol = 0;

        foreach ($row['cols'] as $col) {
            $col['width'] = 0;
            $col['gutter'] = 0;
            $idxBrick = 0;

            foreach ($col['bricks'] as $brick) {
                // support both realtive paths manually set from the template
                if (isset($brick['localImg'])) {
                    $brick['filepath'] =
                        themes_path(
                            Theme::getActiveThemeCode() . '/assets/images'
                        ) . $brick['localImg'];
                    $brick['local_src'] = $brick['localImg'];
                    $brick['src'] = '';
                }
                if (isset($brick['mediaImg'])) {
                    $brick['filepath'] = storage_path(
                        'app/media' . $brick['mediaImg']
                    );
                    $brick['local_src'] = '';
                    $brick['src'] = $brick['mediaImg'];
                }

                $size = file_exists($brick['filepath'])
                    ? getimagesize($brick['filepath'])
                    : [1, 1];
                $brick['width'] = $size[0];
                $brick['height'] = $size[1];

                if ($col['direction'] == 'horizontal') {
                    $col['width'] += $brick['width'];
                    $col['gutter'] += $gutter;
                } else {
                    $col['width'] =
                        $brick['width'] > $col['width']
                            ? $brick['width']
                            : $col['width'];
                }

                $col['bricks'][$idxBrick] = $brick;
                $idxBrick++;
            }

            $col['flexDirection'] =
                $col['direction'] == 'horizontal' ? 'row' : 'column';
            $col['width'] = $col['width'] + $col['gutter'];
            $row['width'] += $col['width'];
            $row['maxWidthCol'] =
                $col['width'] > $row['maxWidthCol']
                    ? $col['width']
                    : $row['maxWidthCol'];
            $row['cols'][$idxCol] = $col;
            $idxCol++;
        }

        $maxWidth = $row['width'] > $maxWidth ? $row['width'] : $maxWidth;
        $rows[$idxRow] = $row;
        $idxRow++;
    }

    // $row['widestWidth'] = $colWidestWidth . "px";

    $this['wall'] = [
        'maxWidth' => $maxWidth,
        'rows' => $rows,
    ];
}
?>
