import {
  $,
  addClass,
  removeClass,
  on,
  off,
  setVendorCSS,
} from "@acanto/core-dom";
import { parseSize } from "@acanto/core-helpers/jss";

/** @typedef {"collapsed" | "expanding" | "expanded"} HeaderStatus */

export default function header(rootSelector = ".Header:", customOptions = {}) {
  const $root = $(rootSelector);
  if (!$root) {
    return;
  }

  const options = {
    styles: {
      expandTime: 300,
    },
    ...customOptions,
  };
  const expandTime = parseSize(options.styles.expandTime);
  const $toggle = $(".Header:toggle", $root);
  const $collapsable = $(".Header:collapse", $root);
  const $bg = $(".Header:bgExpanded", $root);

  /** @type {HeaderStatus} */
  let status = "collapsed";

  // localeSwitch();

  // default status as collapsed
  _setStatus("collapsed");

  // immediately clamp the collpsable header part, it won't never be open
  // at this point.
  if ($collapsable) $collapsable.style.maxHeight = "0";
  if ($bg) $bg.style.bottom = "100%";

  if ($collapsable) {
    setVendorCSS($collapsable, "transitionDuration", expandTime);
  }

  if ($toggle) {
    on($toggle, "click", handleToggle);
    on(document, "click", handleClickOutside);
  }

  /**
   * Handle click outside
   *
   * @param {MouseEvent} event
   */
  function handleClickOutside(event) {
    if (status === "expanded") {
      // @ts-ignore
      if (!$root.contains(event.target)) {
        collapse();
      }
    }
  }

  /**
   * Handle toggle
   */
  function handleToggle() {
    if (status !== "expanded") {
      expand();
    } else {
      collapse();
    }
  }

  /**
   * Expand
   *
   */
  function expand() {
    // first expand the background
    $collapsable.style.maxHeight = `${window.innerHeight * 0.9}px`;
    $bg.style.bottom = "0";
    _setStatus("expanding");

    // then slide in the collapsable
    setTimeout(() => {
      addClass($collapsable, "is-in");
      _setStatus("expanded");
    }, expandTime);

    // then we are expanded
    setTimeout(() => {
      _setStatus("expanded");
    }, expandTime * 1.5);

    addClass($toggle, "is-active");
  }

  /**
   * Collapse
   *
   */
  function collapse() {
    // first slide out the collapsable
    removeClass($collapsable, "is-in");
    _setStatus("expanding");

    // then contract the background
    setTimeout(() => {
      $bg.style.bottom = "100%";
    }, expandTime);

    // then we are collapsed
    setTimeout(() => {
      $collapsable.style.maxHeight = "0";
      _setStatus("collapsed");
    }, expandTime * 1.5);

    removeClass($toggle, "is-active");
  }

  /**
   * Set status
   *
   * @param {HeaderStatus} newValue
   */
  function _setStatus(newValue) {
    status = newValue;
    $root.setAttribute("data-status", newValue);
  }

  /**
   * Destroy
   *
   */
  function destroy() {
    off($toggle, "click", handleToggle);
    off(document, "click", handleClickOutside);
  }

  return {
    $root,
    expand,
    collapse,
    destroy,
  };
}
