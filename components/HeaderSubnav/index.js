import { $, $$, forEach } from "@acanto/core-dom";
import Dropdown from "@acanto/core-dropdown";
import "./index.scss";

/**
 * Component: Header subnav
 */
export default function HeaderSubnav(headerSelector = ".Header:") {
  const $header = $(headerSelector);

  forEach($$(".dropdown", $header), ($dropdown) => {
    Dropdown($dropdown, {
      namespace: "HS",
      fillGaps: [$header],
      hoverable: true,
    });
  });
}
