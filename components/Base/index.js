import { $$, forEach, isNodeList } from "@acanto/core-dom";

/**
 * Base component class to extend, it makes sure that the root selector
 * is in the page before initializing the component.
 *
 * @class Base
 */
export default class Base {
  /**
   * Default static root selector that determines the root element where the
   * component is mounted
   *
   * @static
   * @memberof Base
   * @type {string}
   */
  static rootSelector = "";

  /**
   * Default static options for the component.
   * It always includes a styles object where we can make use of sass variables.
   * Pass them during instantiation like so:
   * ```
   * import styles from "./index.scss"
   *
   * new MyComponent(".myselector", { styles })
   * ```
   * @static
   * @memberof Base
   * @type {Object} defaults
   * @property {Object} styles
   */
  static defaults = {
    styles: {},
  };

  /**
   * @type {HTMLElement}
   * @memberof Base
   */
  root = null;

  /**
   * Base component constructor
   *
   * @param {string | HTMLElement} selectorOrElement
   * @param {object} [customOptions={}]
   */
  constructor(selectorOrElement, customOptions = {}) {
    if (!selectorOrElement && !this.constructor.rootSelector) {
      return;
    }

    this.options = {
      ...this.constructor.defaults,
      ...customOptions,
    };

    // the order of the following conditions matters because we can both have
    // a string selector that when recursively instantiate calls this class
    // both with the rootSelector and with a HTMLelement/NodeList. We give
    // priority to the latter

    // initialised with HTMLElement
    if (selectorOrElement instanceof HTMLElement) {
      this.root = selectorOrElement;
      if (!!selectorOrElement) {
        this.created();
        this.mounted();
      }
    }
    // initialised with NodeList
    else if (isNodeList(selectorOrElement)) {
      if (selectorOrElement.length) {
        forEach(selectorOrElement, (element, idx) => {
          new this.constructor(element);
        });
      }
    }
    // initialised with string selector
    else if (
      typeof selectorOrElement === "string" ||
      this.constructor.rootSelector
    ) {
      const elements = $$(selectorOrElement || this.constructor.rootSelector);

      if (isNodeList(elements) && elements.length) {
        forEach(elements, (element) => {
          new this.constructor(element);
        });
      }
    }
  }

  /**
   * Method to override in subclasses
   * @abstract
   * @memberof Base
   */
  created() {}

  /**
   * Method to override in subclasses
   * @abstract
   * @memberof Base
   */
  mounted() {}

  /**
   * Method to override in subclasses
   * @abstract
   * @memberof Base
   */
  destroyed() {}
}
