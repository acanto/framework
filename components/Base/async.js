import { listenLoaded } from "@acanto/core-dom";
import loadResource from "@acanto/core-ajax/loadResource";

/**
 * Base async component class to extend, it loads its external dependencies
 * lazyly, for stuff like galleries not needed immediately.
 *
 * It uses `loadResource` tiny and efficient plugin
 * @see https://github.com/filamentgroup/loadCSS
 */
export default class ComponentAsync {
  static dependencies = [];

  constructor(name) {
    name = name || this.constructor.name;

    this.created();

    listenLoaded(() => {
      if (loadResource.isDefined(name)) {
        loadResource.ready(name, this.ready.bind(this));
      } else {
        if (this.shouldLoad()) {
          loadResource(this.constructor.dependencies, name, {
            success: this.ready.bind(this),
            error: this.failed.bind(this),
            numRetries: 3,
          });
        } else {
          this.refused();
        }
      }
    });
  }

  /**
   * Should load? Method to override in subclasses
   *
   * Starts the dependencies loading based on the returning value of this
   * method, false by default.
   *
   * @abstract
   * @memberof Base
   */
  shouldLoad() {
    return false;
  }

  /**
   * Created
   *
   * Executed immediately regardless of the dependencies loading
   *
   * @abstract
   * @memberof Base
   */
  created() {}

  /**
   * Ready
   *
   * All dependencies have been loaded.
   *
   * @abstract
   * @memberof Base
   */
  ready() {}

  /**
   * Failed
   *
   * Dependencies loading has failed.
   *
   * @abstract
   * @memberof Base
   */
  failed() {}

  /**
   * Refused
   *
   * Called after `shouldLoad` check, useful to revert stuff done on `created`
   * @abstract
   * @memberof Base
   */
  refused() {}
}
