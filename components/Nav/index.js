import between from "@acanto/core-scroll/between";
import scrollSpy from "@acanto/core-scroll/scrollSpy";

/**
 * Nav in header component
 *
 * @param {Object} options
 * @param {number | HTMLElement | function(): number} options.showFrom
 * @param {Function} [options.onchange]
 */
export default function Nav({ showFrom, onchange }) {
  scrollSpy(".Nav: .Header:nav__link", {
    header: ".Header:",
    onSetup: (areas) => {
      const lastArea = areas[areas.length - 1];

      between(showFrom, lastArea.content, {
        className: "Nav:",
        onchange: onchange,
      });
    },
  });
}
