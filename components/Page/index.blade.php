<section class="Page:">
  <article class="Page:wrap">
    <div class="Page:wrap__inner">
      @if (isset($title) && $title)
        <h1 class="Page:title">
          {!! $title !!}
        </h1>
      @endif
      @if (isset($subtitle) && $subtitle)
        <h2 class="Page:subtitle">
          {!! $subtitle !!}
        </h2>
      @endif
      @if (isset($content) && $content)
        <div class="Page:content">
          {!! $content !!}
        </div>
      @endif
    </div>
  </article>
</section>
