<?php

use Cms\Classes\Theme;
use System\Classes\MediaLibrary;

function onStart()
{
    $images = $this['images'];
    $basePath = '/assets/images';
    $idx = 0;

    foreach ($images as $img) {
        $filepath = themes_path(Theme::getActiveThemeCode() . $basePath) . $img;
        $url = asset(
            'themes/' . Theme::getActiveThemeCode() . $basePath . $img
        );
        $size = getimagesize($filepath);
        $images[$idx] = [
            'src' => $url,
            'alt' => isset($img['alt']) ? $img['alt'] : '',
            'w' => $size[0],
            'h' => $size[1],
        ];
        $idx++;
    }

    $this['gallery'] = $images;
}
?>
