import ComponentAsync from "@acanto/components-base/async";
import {
  forEach,
  $,
  $$,
  addClass,
  removeClass,
  getDataAttr,
  setDataAttr,
} from "@acanto/core-dom";
import template from "./tpl";
import "./index.scss";

const baseURL = "https://cdnjs.cloudflare.com/ajax/libs/photoswipe";

export default class Gallery extends ComponentAsync {
  static dependencies = [
    `${baseURL}/4.1.3/photoswipe.min.css`,
    `${baseURL}/4.1.3/default-skin/default-skin.min.css`,
    `${baseURL}/4.1.3/default-skin/default-skin.svg`,
    `${baseURL}/4.1.3/photoswipe.min.js`,
    `${baseURL}/4.1.3/photoswipe-ui-default.min.js`,
  ];

  shouldLoad() {
    return !!window["Gallery"];
  }

  created() {
    this.triggers = $$("[data-gallery]");
    forEach(this.triggers, (el) => {
      setDataAttr(el, "disabled", "true");
      addClass(el, "loading");
    });
  }

  ready() {
    this.galleries = window["Gallery"];

    forEach(this.triggers, (el) => {
      const id = getDataAttr(el, "gallery");
      setDataAttr(el, "disabled");
      removeClass(el, "loading");
      el.style.cursor = "pointer";

      el.addEventListener("click", () => {
        if (this.galleries[id]) {
          this.open(id);
        }
      });
    });

    this.insertTpl();
  }

  insertTpl() {
    if ($(".Gallery")) {
      return;
    }
    const elem = document.createElement("div");
    elem.innerHTML = template;
    document.body.appendChild(elem);
  }

  open(id) {
    const gallery = this.galleries[id];
    const root = $(".Gallery");
    const options = {
      history: false,
      closeOnScroll: false,
      mainClass: "Gallery",
      bgOpacity: 0.9,
      // focus: false,
      showAnimationDuration: 500,
      // hideAnimationDuration: 0,
      // timeToIdle: 4000,
      // timeToIdleOutside: 1000,
    };
    const instance = new window["PhotoSwipe"](
      root,
      // @ts-ignore
      PhotoSwipeUI_Default,
      gallery,
      options
    );
    instance.init();

    return instance;
  }
}
