# Changelog

## laravel-frontend

### 0.1.52

- In your project's `composer.json` change the `autoload.ps4` part as such:

```json
"psr-4": {
  "resources\\": "resources/"
}
```

<!--
- In your project's gitignore add next to `/resources/`:

```text
/app/
``` -->
